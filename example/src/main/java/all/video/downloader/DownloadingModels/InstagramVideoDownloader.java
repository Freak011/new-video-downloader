package all.video.downloader.DownloadingModels;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Looper;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;

import com.tonyodev.fetch2core.DownloadBlock;
import com.tonyodev.fetch2core.Downloader;
import com.tonyodev.fetch2core.Func;
import com.tonyodev.fetch2okhttp.OkHttpDownloader;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;

import all.video.downloader.Helping.NotificationHelper;
import all.video.downloader.Helping.PathAddress;
import all.video.downloader.MainActivity.MainActivity;
import all.video.downloader.MainActivity.ui.DownloadingFragment;
import all.video.downloader.R;
import all.video.downloader.fetchmainpac.DefaultFetchNotificationManager;
import all.video.downloader.fetchmainpac.Download;
import all.video.downloader.fetchmainpac.Error;
import all.video.downloader.fetchmainpac.Fetch;
import all.video.downloader.fetchmainpac.FetchConfiguration;
import all.video.downloader.fetchmainpac.FetchListener;
import all.video.downloader.fetchmainpac.NetworkType;
import all.video.downloader.fetchmainpac.Priority;
import all.video.downloader.fetchmainpac.Request;
import all.video.downloader.fetchmainpac.Status;

public class InstagramVideoDownloader implements VideoDownloader {

    private Context context;
    private String VideoURL;
    private String VideoTitle;

    public InstagramVideoDownloader(Context context, String videoURL) {
        this.context = context;
        VideoURL = videoURL;
    }

    @Override
    public String createDirectory() {
        File folder = new File(Environment.getExternalStorageDirectory()
                + "/FreeVideoDownloader");

        if (!folder.exists()) {
         folder.mkdirs();
        }
        return folder.getPath();
    }

    @Override
    public String getVideoId(String link) {
        return link;
    }

    @Override
    public void DownloadVideo() {
        new Data().execute(getVideoId(VideoURL));
    }
    @SuppressLint("StaticFieldLeak")
    private class Data extends AsyncTask<String, String,String>{

        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection connection;
            BufferedReader reader;
            try {
                URL url = new URL(strings[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                String buffer = "No URL";
                String Line;
                while ((Line = reader.readLine()) != null)
                {
                    if(Line.contains("og:title")) {
                        VideoTitle = Line.substring(Line.indexOf("og:title"));
                        VideoTitle = Line.substring(Line.indexOf("content"));
                        VideoTitle = VideoTitle.substring(ordinalIndexOf(VideoTitle,"\"",0)+1,ordinalIndexOf(VideoTitle,"\"",1));
                        Log.e("Hello", VideoTitle);
                    }
                    if(Line.contains("og:video"))
                    {
                        Line = Line.substring(Line.indexOf("og:video"));
                        Line = Line.substring(ordinalIndexOf(Line,"\"",1)+1,ordinalIndexOf(Line,"\"",2));
                        if(Line.contains("amp;")) {
                            Line = Line.replace("amp;", "");
                        }
                        if(!Line.contains("https"))
                        {
                            Line = Line.replace("http","https");
                        }
                        Log.e("Hello1",Line);
                        buffer=Line;
                        break;
                    }
                    else {
                        buffer = "No URL";
                    }
                }
                return buffer;
            } catch (IOException e) {
                return "No URL";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(!s.contains("No URL")) {
                if(VideoTitle == null || VideoTitle.equals(""))
                {
                    VideoTitle = "InstaVideo" + new Date().toString()+".mp4";
                }
                else {
                    VideoTitle = VideoTitle + ".mp4";
                }
                StartDownloading((MainActivity)context,s,VideoTitle);

            }
            else {
                if (Looper.myLooper()==null)
                Looper.prepare();
                Toast.makeText(context, "Wrong Video URL or Private Video Can't be downloaded", Toast.LENGTH_SHORT).show();
                Looper.loop();
            }
        }
    }
    private static int ordinalIndexOf(String str, String substr, int n) {
        int pos = -1;
        do {
            pos = str.indexOf(substr, pos + 1);
        } while (n-- > 0 && pos != -1);
        return pos;
    }
    Fetch fetch;
    ProgressDialog dialog;
    Request request;
    String filename;
    NotificationHelper notificationHelper;
    private void StartDownloading(Activity act, String URL, String Title) {
        dialog=new ProgressDialog(act);
        dialog.setMessage("Fetching Video Please Wait...");
        dialog.show();
        FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(act)
                .setDownloadConcurrentLimit(10)
                .enableRetryOnNetworkGain(true)
                .setHttpDownloader(new OkHttpDownloader(Downloader.FileDownloaderType.SEQUENTIAL))
                .setNotificationManager(new DefaultFetchNotificationManager(act))
                .build();
        fetch = Fetch.Impl.getInstance(fetchConfiguration);

        downloadmyvideos(act,URL,Title,".mp4",act);

        fetch.getDownloadsWithStatus(Status.DOWNLOADING, new Func<List<Download>>() {
            @Override
            public void call(@NotNull List<Download> result) {

            }
        });
    }
    public void downloadmyvideos(Activity act,String url, String fileName, String ext, ContextThemeWrapper contextThemeWrapper){

        File yourAppDir = PathAddress.pathofdownload(contextThemeWrapper);
        downloadwithfetch(act,url,fileName,yourAppDir.getPath()+File.separator+fileName+"."+ext);
    }
    public void downloadwithfetch(Activity act,String url,String filename,String file){
        request = new Request(url, file);
        request.setPriority(Priority.HIGH);
        request.setNetworkType(NetworkType.ALL);
        request.addHeader("clientKey", "SD78DF93_3947&MVNGHE1WONG");
        notificationHelper = new NotificationHelper(act);
        this.filename=filename;
        fetch.enqueue(request, updatedRequest -> {
        }, error -> {
            Toast.makeText(act, "Ar error" +error, Toast.LENGTH_SHORT).show();
            //An error occurred enqueuing the request.
        });
        fetch.addListener(new FetchListener() {
            @Override
            public void onError(@NotNull Download download, @NotNull Error error, @Nullable Throwable throwable) {
                dialog.dismiss();
            }
            @Override
            public void onAdded(@NotNull Download download) {

            }

            @Override
            public void onQueued(@NotNull Download download, boolean waitingOnNetwork) {

            }

            @Override
            public void onWaitingNetwork(@NotNull Download download) {

            }

            @Override
            public void onCompleted(@NotNull Download download) {
                if (request.getId() == download.getId()) {
                    try {
                        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                        Uri contentUri=download.getFileUri();
                        mediaScanIntent.setData(contentUri);
                        act.sendBroadcast(mediaScanIntent);

                        File file= new File(download.getFile());
                        act.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    fetch.getDownloadsWithStatus(Status.DOWNLOADING, new Func<List<Download>>() {
                        @Override
                        public void call(@NotNull List<Download> result) {

                            try {
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                    fetch.remove(download.getId());
                }
            }



            @Override
            public void onDownloadBlockUpdated(@NotNull Download download, @NotNull DownloadBlock downloadBlock, int totalBlocks) {

            }

            @Override
            public void onStarted(@NotNull Download download, @NotNull List<? extends DownloadBlock> downloadBlocks, int totalBlocks) {
                dialog.dismiss();
                if (((MainActivity) act).BrowserLayout.getVisibility()== View.VISIBLE){
                    ((MainActivity) act).webview.onPause();
                }
                ((MainActivity) act).HomeLayout.setVisibility(View.GONE);
                ((MainActivity) act).llContainer.setVisibility(View.VISIBLE);

                FragmentManager fm = ((MainActivity) act).getSupportFragmentManager();
                DownloadingFragment fragment = new DownloadingFragment();
                fm.beginTransaction().replace(R.id.FragmentContainer,fragment).commit();
            }
            @Override
            public void onProgress(@NotNull Download download, long etaInMilliSeconds, long downloadedBytesPerSecond) {

            }

            @Override
            public void onPaused(@NotNull Download download) {

            }

            @Override
            public void onResumed(@NotNull Download download) {

            }

            @Override
            public void onCancelled(@NotNull Download download) {

            }

            @Override
            public void onRemoved(@NotNull Download download) {

            }

            @Override
            public void onDeleted(@NotNull Download download) {

            }
        });
    }
}
