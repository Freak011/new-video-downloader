package all.video.downloader.Splash;

import static all.video.downloader.Helping.Utils.getUIDs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;

//import com.facebook.ads.Ad;
//import com.facebook.ads.AdError;
//import com.facebook.ads.AudienceNetworkAds;
//import com.facebook.ads.InterstitialAd;
//import com.facebook.ads.InterstitialAdListener;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

//import com.google.android.ads.mediationtestsuite.MediationTestSuite;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.OnPaidEventListener;
import com.google.android.gms.ads.ResponseInfo;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;

import all.video.downloader.AdmobHelper.AdmobHelper;
import all.video.downloader.Helping.MyExceptionHandler;
import all.video.downloader.MainActivity.MainActivity;
import all.video.downloader.R;

public class Splash extends Activity {
//    private InterstitialAd interstitialAd;
    private com.google.android.gms.ads.interstitial.InterstitialAd mInterstitialAdMob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        AdmobHelper.getInstance().loadInterstitial(Splash.this,"");
        getUIDs(this);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        loadGoogleAds();
//        MediationTestSuite.launch(Splash.this);

    }


    private void loadGoogleAds(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AdmobHelper.getInstance().mInterstitial != null){
                    AdmobHelper.getInstance().showAdAfterDialogFor2Seconds(Splash.this);
                    AdmobHelper.getInstance().mInterstitial.setFullScreenContentCallback(new FullScreenContentCallback() {
                        @Override
                        public void onAdDismissedFullScreenContent() {
                            startActivity(new Intent(Splash.this,MainActivity.class));
                            finish();
                        }
                    });
                }else{
                    startActivity(new Intent(Splash.this,MainActivity.class));
                    finish();
                }
            }
        },7000);
    }

}
