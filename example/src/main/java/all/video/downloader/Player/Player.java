package all.video.downloader.Player;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import all.video.downloader.FullScreen.MakeFullScreen;
import all.video.downloader.Helping.Constants;
import all.video.downloader.Helping.MimeTypes;
import all.video.downloader.R;

public class Player extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        MakeFullScreen fullScreen=new MakeFullScreen(Player.this);
        fullScreen.makeFull();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String str1= Constants.urlPlay.getAbsolutePath();
        Uri uri=  Uri.parse(str1);
        String mimeType = MimeTypes.getMimeType(Constants.urlPlay.getName());
        intent.setDataAndType(uri, mimeType);
        try {
//            GiraffePlayer.play(Player.this, new VideoInfo(uri));
        }catch (Exception e) {

            try{
                Uri uri1=  Uri.parse(str1);
                intent.setDataAndType(uri1, mimeType);
                startActivity(Intent.createChooser(intent, "Open file"));
            }catch (Exception ea){
                ea.printStackTrace();
                Toast.makeText(Player.this, "No application to open file",
                        Toast.LENGTH_SHORT).show();
            }

        }
    }

}
