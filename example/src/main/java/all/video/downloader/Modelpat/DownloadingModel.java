package all.video.downloader.Modelpat;

public class DownloadingModel {
    String id,title,thumb;
    long progress;

    public DownloadingModel(String id, String title, String thumb, long progress) {
        this.id = id;
        this.title = title;
        this.thumb = thumb;
        this.progress=progress;
    }

    public DownloadingModel() {
    }

    public long getProgress() {
        return progress;
    }

    public void setProgress(long progress) {
        this.progress = progress;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getThumb() {
        return thumb;
    }
}
