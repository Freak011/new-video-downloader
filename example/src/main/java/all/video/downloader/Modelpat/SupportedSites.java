package all.video.downloader.Modelpat;

import android.graphics.drawable.Drawable;

public class SupportedSites {

    Drawable drawable;
    String url;

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
