package all.video.downloader.fetchmainpac.database.migration

import androidx.sqlite.db.SupportSQLiteDatabase
import all.video.downloader.fetchmainpac.database.DownloadDatabase
import all.video.downloader.fetchmainpac.util.EMPTY_JSON_OBJECT_STRING

class MigrationFiveToSix : Migration(5, 6) {

    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("ALTER TABLE '${DownloadDatabase.TABLE_NAME}' "
                + "ADD COLUMN '${DownloadDatabase.COLUMN_EXTRAS}' TEXT NOT NULL DEFAULT '$EMPTY_JSON_OBJECT_STRING'")
    }

}