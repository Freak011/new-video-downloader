package all.video.downloader.fetchmainpac.exception

open class FetchException constructor(message: String) : RuntimeException(message)