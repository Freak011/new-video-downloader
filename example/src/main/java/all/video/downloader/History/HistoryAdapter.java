package all.video.downloader.History;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import all.video.downloader.R;
import all.video.downloader.database.DbItem;


public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.RecyclerViewHolder> {


    List<DbItem> list;
    Context context;


    public HistoryAdapter(Context con, List<DbItem> list1){
        this.context = con;
        this.list = list1;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_list, null, false);

        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {
        DbItem item=list.get(position);
        holder.textView.setText(item.getTitle());
        holder.textViewLink.setText(item.getUrl());
    }
    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
    @Override
    public int getItemCount() {
        return list.size();
    }
    public class RecyclerViewHolder extends RecyclerView.ViewHolder  {
        TextView textView,textViewLink;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.textView);
            textViewLink=itemView.findViewById(R.id.textViewLink);
        }
    }
}
