package all.video.downloader.History;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import all.video.downloader.MainActivity.MainActivity;
import all.video.downloader.R;
import all.video.downloader.database.DbItem;
import all.video.downloader.database.HistoryDatabase;


public class DialogHistory extends Dialog{
    public Activity activity;
    RecyclerView HistoryRecycler;
    Button clear;
    HistoryDatabase historyDatabase;
    ArrayList<DbItem> list;
    public DialogHistory(Activity activity) {
        super(activity);
        this.activity = activity;
        historyDatabase=new HistoryDatabase(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.history_layout);
        HistoryRecycler=findViewById(R.id.HistoryRecycler);
        HistoryRecycler.setLayoutManager(new LinearLayoutManager(activity));
        HistoryRecycler.setHasFixedSize(true);
        list=historyDatabase.getHistory();
        HistoryAdapter adapter=new HistoryAdapter(activity,list);
        HistoryRecycler.setAdapter(adapter);
        HistoryRecycler.addOnItemTouchListener(
                new RecyclerItemClickListener(activity, HistoryRecycler ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        DbItem item=list.get(position);
                        ((MainActivity) activity).webview.loadUrl(item.getUrl());
                        ((MainActivity) activity).BrowserLayout.setVisibility(View.VISIBLE);
                        ((MainActivity) activity).HomeLayout.setVisibility(View.GONE);
                        ((MainActivity) activity).etBrowserURL.setText(item.getUrl());
                        ((MainActivity) activity).customDialog.hide();
                    }
                    @Override public void onLongItemClick(View view, int position) {
                    }
                })
        );
        clear=findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                historyDatabase.clearAllItems();
                list.clear();
                list=historyDatabase.getHistory();
                HistoryAdapter adapter=new HistoryAdapter(activity,list);
                HistoryRecycler.setAdapter(adapter);
            }
        });
    }
}