package all.video.downloader.PopupPack;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import androidx.fragment.app.FragmentManager;

import com.tonyodev.fetch2core.DownloadBlock;
import com.tonyodev.fetch2core.Func;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.List;

import all.video.downloader.DownloadingModels.ModelBase;
import all.video.downloader.Helping.Utils;
import all.video.downloader.Helping.NotificationHelper;
import all.video.downloader.MainActivity.MainActivity;
import all.video.downloader.MainActivity.ui.DownloadingFragment;
import all.video.downloader.R;
import all.video.downloader.fetchmainpac.DefaultFetchNotificationManager;
import all.video.downloader.fetchmainpac.Download;
import all.video.downloader.fetchmainpac.Error;
import all.video.downloader.fetchmainpac.Fetch;
import all.video.downloader.fetchmainpac.FetchConfiguration;
import all.video.downloader.fetchmainpac.FetchListener;
import all.video.downloader.fetchmainpac.NetworkType;
import all.video.downloader.fetchmainpac.Priority;
import all.video.downloader.fetchmainpac.Request;
import all.video.downloader.fetchmainpac.Status;


public class DialogCenter extends Dialog implements
        View.OnClickListener {
    public Activity activity;
    public ImageView img_fast, img_normal;
    int downloadId;
    private all.video.downloader.fetchmainpac.Fetch fetch;
    ProgressDialog dialog;
    all.video.downloader.fetchmainpac.Request request;
    ModelBase modelBase;
    String filename;
    NotificationHelper notificationHelper;
    public DialogCenter(Activity activity, ModelBase modelBase) {
        super(activity);
        this.activity = activity;

        dialog=new ProgressDialog(activity);
        dialog.setMessage("Please wait a moment...");
        dialog.setCanceledOnTouchOutside(false);
        this.modelBase=modelBase;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.center_pop_up_lay);

        img_fast = (ImageView) findViewById(R.id.img_fast);
        img_normal = (ImageView) findViewById(R.id.img_normal);
        img_fast.setOnClickListener(this);
        img_normal.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_fast:
                downloadVid();
                break;
            case R.id.img_normal:
                downloadVid();
                break;
            default:
                break;
        }
        dismiss();
    }

    private void downloadVid() {
        dialog.show();
        String dirPath = Utils.getRootDirPath(activity);
        all.video.downloader.fetchmainpac.FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(activity)
                .setDownloadConcurrentLimit(10)
                .enableRetryOnNetworkGain(true)
                .enableAutoStart(false)
                .enableLogging(true)
                .setNotificationManager(new DefaultFetchNotificationManager(activity))
                .build();
        fetch = Fetch.Impl.getInstance(fetchConfiguration);
        downloadwithfetch(modelBase.getDownurl(),modelBase.getTitle(),dirPath+File.separator+modelBase.getTitle()+"."+modelBase.getExt());
    }

    public void downloadwithfetch(String url,String filename,String file){
        request = new Request(url, file);
        request.setPriority(Priority.HIGH);
        request.setNetworkType(NetworkType.ALL);
        request.addHeader("clientKey", "SD78DF93_3947&MVNGHE1WONG");
        notificationHelper = new NotificationHelper(activity);
        this.filename=filename;
        fetch.enqueue(request, updatedRequest -> {
        }, error -> {

        });
        fetch.addListener(new FetchListener() {
            @Override
            public void onError(@NotNull all.video.downloader.fetchmainpac.Download download, @NotNull Error error, @Nullable Throwable throwable) {
                dialog.dismiss();
            }

            @Override
            public void onWaitingNetwork(@NotNull all.video.downloader.fetchmainpac.Download download) {

            }

            @Override
            public void onStarted(@NotNull all.video.downloader.fetchmainpac.Download download, @NotNull List<? extends DownloadBlock> list, int i) {
                if (request.getId() == download.getId()) {
                    dialog.dismiss();
                    if (((MainActivity) activity).BrowserLayout.getVisibility()==View.VISIBLE){
                        ((MainActivity) activity).webview.onPause();
                    }
                    ((MainActivity) activity).mHome.setImageResource(R.drawable.home_normal);
                    ((MainActivity) activity).mDownloading.setImageResource(R.drawable.inprogress_selector);
                    ((MainActivity) activity).mFiles.setImageResource(R.drawable.downloaded_normal);
                    ((MainActivity) activity).HomeLayout.setVisibility(View.GONE);
                    ((MainActivity) activity).llContainer.setVisibility(View.VISIBLE);
                    FragmentManager fm = ((MainActivity) activity).getSupportFragmentManager();
                    DownloadingFragment fragment = new DownloadingFragment();
                    fm.beginTransaction().replace(R.id.FragmentContainer,fragment).commit();
                    fetch.getDownloadsWithStatus(all.video.downloader.fetchmainpac.Status.DOWNLOADING, new Func<List<all.video.downloader.fetchmainpac.Download>>() {
                        @Override
                        public void call(@NotNull List<all.video.downloader.fetchmainpac.Download> result) {

                            try {
                                if (result.size() < 1) {
                                } else {
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }

            @Override
            public void onResumed(@NotNull all.video.downloader.fetchmainpac.Download download) {

            }

            @Override
            public void onRemoved(@NotNull all.video.downloader.fetchmainpac.Download download) {

            }

            @Override
            public void onQueued(@NotNull all.video.downloader.fetchmainpac.Download download, boolean b) {

            }

            @Override
            public void onProgress(@NotNull all.video.downloader.fetchmainpac.Download download, long l, long l1) {
            }

            @Override
            public void onPaused(@NotNull all.video.downloader.fetchmainpac.Download download) {

            }

            @Override
            public void onDownloadBlockUpdated(@NotNull all.video.downloader.fetchmainpac.Download download, @NotNull DownloadBlock downloadBlock, int i) {

            }

            @Override
            public void onDeleted(@NotNull all.video.downloader.fetchmainpac.Download download) {

            }

            @Override
            public void onCompleted(@NotNull all.video.downloader.fetchmainpac.Download download) {
                if (request.getId() == download.getId()) {
                    try {
                        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                        Uri contentUri=download.getFileUri();
                        mediaScanIntent.setData(contentUri);
                        activity.sendBroadcast(mediaScanIntent);
                        File file= new File(download.getFile());
                        activity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    fetch.getDownloadsWithStatus(Status.DOWNLOADING, new Func<List<all.video.downloader.fetchmainpac.Download>>() {
                        @Override
                        public void call(@NotNull List<all.video.downloader.fetchmainpac.Download> result) {

                            try {
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                    fetch.remove(download.getId());
                }
            }

            @Override
            public void onCancelled(@NotNull all.video.downloader.fetchmainpac.Download download) {
                dialog.dismiss();
            }

            @Override
            public void onAdded(@NotNull Download download) {

            }
        });
    }
}