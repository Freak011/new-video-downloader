package all.video.downloader.PopupPack;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;

import all.video.downloader.R;
import im.delight.android.webview.AdvancedWebView;


public class DialogLinks extends Dialog{
    public Activity activity;
    AdvancedWebView WebView;
    String link;
    public DialogLinks(Activity activity, String link) {
        super(activity);
        this.activity = activity;
        this.link=link;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.link_pop_up_lay);
        WebView=(AdvancedWebView)findViewById(R.id.WebView);
        WebView.loadUrl(link);
    }

}