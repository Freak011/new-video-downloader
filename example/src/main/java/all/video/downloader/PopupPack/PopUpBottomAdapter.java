package all.video.downloader.PopupPack;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tonyodev.fetch2core.DownloadBlock;
import com.tonyodev.fetch2core.Downloader;
import com.tonyodev.fetch2core.Func;
import com.tonyodev.fetch2okhttp.OkHttpDownloader;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;

import all.video.downloader.DownloadingModels.ModelBase;
import all.video.downloader.Helping.FacebookBuilder;
import all.video.downloader.Helping.FileBuilder;
import all.video.downloader.Helping.FileDirector;
import all.video.downloader.Helping.GeneralFileBuilder;
import all.video.downloader.Helping.NotificationHelper;
import all.video.downloader.Helping.PathAddress;
import all.video.downloader.MainActivity.MainActivity;
import all.video.downloader.MainActivity.ui.DownloadingFragment;
import all.video.downloader.R;
import all.video.downloader.fetchmainpac.DefaultFetchNotificationManager;
import all.video.downloader.fetchmainpac.Download;
import all.video.downloader.fetchmainpac.Error;
import all.video.downloader.fetchmainpac.Fetch;
import all.video.downloader.fetchmainpac.FetchConfiguration;
import all.video.downloader.fetchmainpac.FetchListener;
import all.video.downloader.fetchmainpac.NetworkType;
import all.video.downloader.fetchmainpac.Priority;
import all.video.downloader.fetchmainpac.Request;
import all.video.downloader.fetchmainpac.Status;


public class PopUpBottomAdapter extends RecyclerView.Adapter<PopUpBottomAdapter.RecyclerViewHolder> {

    private static int viewtypeinit = 0;

    List<ModelBase> list;
    Context context;


    public PopUpBottomAdapter(Context con, List<ModelBase> list1) {
        this.context = con;
        this.list = list1;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = null;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.topsheet_down_popup_adapter_lay, null, false);
        //  }
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {
        holder.bindView(position);


        holder.txt_format.setText(list.get(position).getFormat());

        holder.progressBar.setVisibility(View.VISIBLE);


        holder.img_pop_up_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Activity a = scanForActivity(context);
                    StartDownloading(a, list.get(position));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


    }

    int downloadId;
    Fetch fetch;
    ProgressDialog dialog;
    Request request;
    String filename;
    NotificationHelper notificationHelper;

    private void StartDownloading(Activity act, ModelBase modelBase) {
        dialog = new ProgressDialog(act);
        dialog.setMessage("Fetching Video Please Wait...");
        dialog.show();
        FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(act)
                .setDownloadConcurrentLimit(10)
                .enableRetryOnNetworkGain(true)
                .setHttpDownloader(new OkHttpDownloader(Downloader.FileDownloaderType.SEQUENTIAL))
                .setNotificationManager(new DefaultFetchNotificationManager(act))
                .build();
        fetch = Fetch.Impl.getInstance(fetchConfiguration);

        downloadmyvideos(act, modelBase.getDownurl(), modelBase.getTitle(), modelBase.getExt(), act);

        fetch.getDownloadsWithStatus(Status.DOWNLOADING, new Func<List<Download>>() {
            @Override
            public void call(@NotNull List<Download> result) {

            }
        });
    }

    public void downloadmyvideos(Activity act, String url, String fileName, String ext, ContextThemeWrapper contextThemeWrapper) {

        File yourAppDir = PathAddress.pathofdownload(contextThemeWrapper);
        FileBuilder builder;
        if (url.contains("facebook")) {
            Log.d("URL", "facebook");
            builder = new FacebookBuilder(url);
        } else {
            Log.d("URL", "General FIle");
            builder = new GeneralFileBuilder(url);
        }
        FileDirector fd = new FileDirector(builder);
        String urlnew = builder.getFile().getUrl();
        downloadwithfetch(act, url, fileName, yourAppDir.getPath() + File.separator + fileName + "." + ext);
    }

    public void downloadwithfetch(Activity act, String url, String filename, String file) {
        request = new all.video.downloader.fetchmainpac.Request(url, file);
        request.setPriority(Priority.HIGH);
        request.setNetworkType(NetworkType.ALL);
        request.addHeader("clientKey", "SD78DF93_3947&MVNGHE1WONG");
        notificationHelper = new NotificationHelper(act);
        this.filename = filename;
        fetch.enqueue(request, updatedRequest -> {
        }, error -> {
            Toast.makeText(act, "Ar error" + error, Toast.LENGTH_SHORT).show();
            //An error occurred enqueuing the request.
        });
        fetch.addListener(new FetchListener() {
            @Override
            public void onAdded(@NotNull Download download) {

            }

            @Override
            public void onQueued(@NotNull Download download, boolean waitingOnNetwork) {

            }

            @Override
            public void onWaitingNetwork(@NotNull Download download) {

            }

            @Override
            public void onCompleted(@NotNull Download download) {
                if (request.getId() == download.getId()) {
                    try {
                        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                        Uri contentUri = download.getFileUri();
                        mediaScanIntent.setData(contentUri);
                        act.sendBroadcast(mediaScanIntent);

                        File file = new File(download.getFile());
                        act.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    fetch.getDownloadsWithStatus(Status.DOWNLOADING, new Func<List<Download>>() {
                        @Override
                        public void call(@NotNull List<Download> result) {
                        }
                    });
                    fetch.remove(download.getId());
                }
            }

            @Override
            public void onError(@NotNull Download download, @NotNull Error error, @Nullable Throwable throwable) {
                dialog.dismiss();
            }

            @Override
            public void onDownloadBlockUpdated(@NotNull Download download, @NotNull DownloadBlock downloadBlock, int totalBlocks) {

            }

            @Override
            public void onStarted(@NotNull Download download, @NotNull List<? extends DownloadBlock> downloadBlocks, int totalBlocks) {
                dialog.dismiss();
                ((MainActivity) act).bottomsheetdialog.dismiss();
                if (((MainActivity) act).BrowserLayout.getVisibility() == View.VISIBLE) {
                    ((MainActivity) act).webview.onPause();
                }
                ((MainActivity) act).mHome.setImageResource(R.drawable.home_normal);
                ((MainActivity) act).mDownloading.setImageResource(R.drawable.inprogress_selector);
                ((MainActivity) act).mFiles.setImageResource(R.drawable.downloaded_normal);
                ((MainActivity) act).HomeLayout.setVisibility(View.GONE);
                ((MainActivity) act).llContainer.setVisibility(View.VISIBLE);
                FragmentManager fm = ((MainActivity) act).getSupportFragmentManager();
                DownloadingFragment fragment = new DownloadingFragment();
                fm.beginTransaction().replace(R.id.FragmentContainer, fragment).commit();

//                Activity a = scanForActivity(context);
//                if (((MainActivity) a).mInterstitialAdMob.isLoaded()) {
//                    ((MainActivity) a).mInterstitialAdMob.show();
//                } else if (((MainActivity) a).fbinterstitialAd.isAdLoaded()) {
//                    ((MainActivity) a).fbinterstitialAd.show();
//                }
            }

            @Override
            public void onProgress(@NotNull Download download, long etaInMilliSeconds, long downloadedBytesPerSecond) {

            }

            @Override
            public void onPaused(@NotNull Download download) {

            }

            @Override
            public void onResumed(@NotNull Download download) {

            }

            @Override
            public void onCancelled(@NotNull Download download) {

            }

            @Override
            public void onRemoved(@NotNull Download download) {

            }

            @Override
            public void onDeleted(@NotNull Download download) {

            }
        });
    }

    //    public void downloadwithfetch(Activity activity,String url,String filename,String file){
//        request = new Request(url, file);
//        request.setPriority(Priority.HIGH);
//        request.setNetworkType(NetworkType.ALL);
//        request.addHeader("clientKey", "SD78DF93_3947&MVNGHE1WONG");
//        notificationHelper = new NotificationHelper(activity);
//        this.filename=filename;
//        fetch.enqueue(request, updatedRequest -> {
//        }, error -> {
//
//        });
//        fetch.addListener(new FetchListener() {
//            @Override
//            public void onWaitingNetwork(@NotNull Download download) {
//
//            }
//
//            @Override
//            public void onStarted(@NotNull Download download, @NotNull List<? extends DownloadBlock> list, int i) {
//                if (request.getId() == download.getId()) {
//                    dialog.dismiss();
//                    if (((MainActivity) activity).BrowserLayout.getVisibility()==View.VISIBLE){
//                        ((MainActivity) activity).webview.onPause();
//                    }
//                    ((MainActivity) activity).mHome.setImageResource(R.drawable.home_normal);
//                    ((MainActivity) activity).mDownloading.setImageResource(R.drawable.inprogress_selector);
//                    ((MainActivity) activity).mFiles.setImageResource(R.drawable.downloaded_normal);
//                    ((MainActivity) activity).HomeLayout.setVisibility(View.GONE);
//                    ((MainActivity) activity).llContainer.setVisibility(View.VISIBLE);
//                    FragmentManager fm = ((MainActivity) activity).getSupportFragmentManager();
//                    DownloadingFragment fragment = new DownloadingFragment();
//                    fm.beginTransaction().replace(R.id.FragmentContainer,fragment).commit();
//                    fetch.getDownloadsWithStatus(Status.DOWNLOADING, new Func<List<Download>>() {
//                        @Override
//                        public void call(@NotNull List<Download> result) {
//
//                            try {
//                                if (result.size() < 1) {
//                                } else {
//                                }
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//
//                }
//            }
//
//            @Override
//            public void onResumed(@NotNull Download download) {
//
//            }
//
//            @Override
//            public void onRemoved(@NotNull Download download) {
//
//            }
//
//            @Override
//            public void onQueued(@NotNull Download download, boolean b) {
//
//            }
//
//            @Override
//            public void onProgress(@NotNull Download download, long l, long l1) {
//            }
//
//            @Override
//            public void onPaused(@NotNull Download download) {
//
//            }
//
//            @Override
//            public void onError(@NotNull Download download, @NotNull Error error, @Nullable Throwable throwable) {
//                dialog.dismiss();
//            }
//
//            @Override
//            public void onDownloadBlockUpdated(@NotNull Download download, @NotNull DownloadBlock downloadBlock, int i) {
//
//            }
//
//            @Override
//            public void onDeleted(@NotNull Download download) {
//
//            }
//
//            @Override
//            public void onCompleted(@NotNull Download download) {
//                if (request.getId() == download.getId()) {
//                    try {
//                        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//                        Uri contentUri=download.getFileUri();
//                        mediaScanIntent.setData(contentUri);
//                        activity.sendBroadcast(mediaScanIntent);
//                        File file= new File(download.getFile());
//                        activity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                    fetch.getDownloadsWithStatus(Status.DOWNLOADING, new Func<List<Download>>() {
//                        @Override
//                        public void call(@NotNull List<Download> result) {
//
//                            try {
//                            }catch (Exception e){
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//                    fetch.remove(download.getId());
//                }
//            }
//
//            @Override
//            public void onCancelled(@NotNull Download download) {
//                dialog.dismiss();
//            }
//
//            @Override
//            public void onAdded(@NotNull Download download) {
//
//            }
//        });
//    }
    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }


    //////////////

    @Override
    public int getItemCount() {
        return list.size();
    }


    private static Activity scanForActivity(Context cont) {
        if (cont == null)
            return null;
        else if (cont instanceof Activity)
            return (Activity) cont;
        else if (cont instanceof ContextWrapper)
            return scanForActivity(((ContextWrapper) cont).getBaseContext());

        return null;
    }

    public static String getStringSizeLengthFile(long size) {

        DecimalFormat df = new DecimalFormat("0.00");

        float sizeKb = 1024.0f;
        float sizeMb = sizeKb * sizeKb;
        float sizeGb = sizeMb * sizeKb;
        float sizeTerra = sizeGb * sizeKb;


        if (size < sizeMb)
            return df.format(size / sizeKb) + " Kb";
        else if (size < sizeGb)
            return df.format(size / sizeMb) + " Mb";
        else if (size < sizeTerra)
            return df.format(size / sizeGb) + " Gb";

        return "";
    }


    ////////////////////


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView img_pop_up_download;
        TextView txt_format;
        TextView txt_detail;
        ProgressBar progressBar;

        public RecyclerViewHolder(View itemView) {

            super(itemView);
            try {

                init(itemView);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void init(View itemView) {


            img_pop_up_download = (ImageView) itemView.findViewById(R.id.img_pop_up_download);
            txt_format = (TextView) itemView.findViewById(R.id.txt_format);
            txt_detail = (TextView) itemView.findViewById(R.id.txt_detail);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);


        }

        public void bindView(final int position) {

            new AsyncTask<String, String, String>() {


                @Override
                protected String doInBackground(String... strings) {
                    try {

                        URL url = new URL(list.get(position).getDownurl());
                        int fileLength = url.openConnection().getContentLength();
                        String str = "" + getStringSizeLengthFile(fileLength);
                        return str;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(String id) {
                    super.onPostExecute(id);
                    if (id != null) {
                        //Load your bitmap here
                        try {
                            progressBar.setVisibility(View.GONE);
                            txt_detail.setText(id);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.execute(list.get(position).getDownurl());
        }


    }

    //////
    public static long getFolderSize(File f) {
        long size = 0;
        if (f.isDirectory()) {
            for (File file : f.listFiles()) {
                size += getFolderSize(file);
            }
        } else {
            size = f.length();
        }
        return size;
    }


    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);

        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable) drawable).getBitmap() != null;
        }

        return hasImage;
    }
}
