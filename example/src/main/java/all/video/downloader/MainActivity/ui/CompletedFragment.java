package all.video.downloader.MainActivity.ui;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.AudienceNetworkAds;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import all.video.downloader.Helping.Constants;
import all.video.downloader.Helping.FileUtils;
import all.video.downloader.MainActivity.Adapter.AdapterMainActivitySavedRecyclerView;
import all.video.downloader.Helping.PathAddress;
import all.video.downloader.MainActivity.MainActivity;
import all.video.downloader.MainActivity.WalletActivity;
import all.video.downloader.R;
public class CompletedFragment extends Fragment {
    String saveFile = "";
    private RecyclerView recyclerView;
    private AdapterMainActivitySavedRecyclerView adapter;
    ImageView Back;
    Button openWallet;
    GridLayoutManager gridLayoutManager;
    private List<File> files;
    File newf;
    private AdView mAdView;

    Dialog dialog;
    AlertDialog.Builder builder;
    public CompletedFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_completed, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Constants.isURL=3;
//        AudienceNetworkAds.initialize(getActivity());
        builder= new AlertDialog.Builder(getActivity());
        builder.setView(R.layout.progress_dialog);
        dialog = builder.create();
        openWallet=view.findViewById(R.id.openWallet);
        openWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), WalletActivity.class),3);
            }
        });

        // Find the Ad Container
//        LinearLayout adContainer = (LinearLayout) view.findViewById(R.id.banner_container);
//
//        // Add the ad view to your activity layout
//        if(((MainActivity) getActivity()).adViewBanner.getParent() != null) {
//            ((ViewGroup)((MainActivity) getActivity()).adViewBanner.getParent()).removeView(((MainActivity) getActivity()).adViewBanner); // <- fix
//        }
//        adContainer.addView(((MainActivity) getActivity()).adViewBanner);
//        MobileAds.initialize(getActivity(), new OnInitializationCompleteListener() {
//            @Override
//            public void onInitializationComplete(InitializationStatus initializationStatus) {
//            }
//        });
//        mAdView = view.findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);
//        mAdView.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                mAdView.setVisibility(View.VISIBLE);
//                // Code to be executed when an ad finishes loading.
//            }
//
//            @Override
//            public void onAdFailedToLoad(int errorCode) {
////                adContainer.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onAdOpened() {
//                // Code to be executed when an ad opens an overlay that
//                // covers the screen.
//            }
//
//            @Override
//            public void onAdClicked() {
//                // Code to be executed when the user clicks on an ad.
//            }
//
//            @Override
//            public void onAdLeftApplication() {
//                // Code to be executed when the user has left the app.
//            }
//
//            @Override
//            public void onAdClosed() {
//                // Code to be executed when the user is about to return
//                // to the app after tapping on an ad.
//            }
//        });

        saveFile= PathAddress.pathofdownload(getActivity()).getPath();
        newf = new File(Environment.getExternalStorageDirectory()
                + "/FreeVideoDownloader");
        recyclerView=(RecyclerView) view.findViewById(R.id.recyclerViewMedia);
        gridLayoutManager = new GridLayoutManager(getActivity(),3);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        files = getListFilesmp4(newf);
        Collections.sort(files, new Comparator<File>() {

            @Override
            public int compare(File file1, File file2) {
                long k = file1.lastModified() - file2.lastModified();
                if(k > 0){
                    return 1;
                }else if(k == 0){
                    return 0;
                }else{
                    return -1;
                }
            }
        });
        adapter = new AdapterMainActivitySavedRecyclerView(getActivity(), files,CompletedFragment.this);
        recyclerView.setAdapter(adapter);

/*        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),recyclerView , new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }

            @Override
            public void onLongItemClick(View view, int position) {
                //   getOperations(files.get(position).getAbsolutePath());

                galMyDialog(files.get(position).getAbsolutePath());

            }
        }));*/

    }
    public void sharedialog(File file) {
        Uri imageUri = FileProvider.getUriForFile(getActivity(), "all.video.downloader.provider", file);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("video/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        intent.putExtra(Intent.EXTRA_STREAM, imageUri);
        startActivity(Intent.createChooser(intent, "Share via :"));
    }
    public void hidedialog(File file) {
        Uri imageUri = FileProvider.getUriForFile(
                getActivity(),
                "all.video.downloader.provider",
                file);
        dialog.show();
        final File des= new File(Environment.getExternalStorageDirectory()+"/.DownloaderVault");
        if (!des.exists()) {
            des.mkdir();
        }

        File Destination= new File(Environment.getExternalStorageDirectory()+"/.DownloaderVault/"+file.getName());
        try {
            transferImage(file,Destination);
            dialog.dismiss();
        } catch (IOException e) {
            dialog.dismiss();
            Toast.makeText(getActivity(), "First" + e, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
    public void transferImage(File source, File distination) throws IOException {
        FileInputStream inStream = new FileInputStream(source);
        FileOutputStream outStream = new FileOutputStream(distination);
        FileChannel inChannel = inStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outStream.getChannel());
        inStream.close();
        outStream.close();
        try {
            final String where = MediaStore.MediaColumns.DATA + "=?";
            final String[] selectionArgs = new String[]{
                    source.getAbsolutePath()
            };
            final ContentResolver contentResolver = getActivity().getContentResolver();
            final Uri filesUri = MediaStore.Files.getContentUri("external");
            contentResolver.delete(filesUri, where, selectionArgs);
            source.delete();
            refresh();
        }catch (Throwable e)
        {
            Toast.makeText(getActivity(), "Second"+e, Toast.LENGTH_SHORT).show();
        }
    }
    public void renamedialog(File file, final String filepath){

        final Dialog dialog = new Dialog(
                getActivity());
        dialog.setContentView(R.layout.rename_dialog);
        final TextView rename = (TextView) dialog
                .findViewById(R.id.rename_file_text);
        rename.setText(file.getName());
        TextView saveButton = (TextView) dialog
                .findViewById(R.id.rename_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final TextView rename = (TextView) dialog
                        .findViewById(R.id.rename_file_text);
                final CharSequence name = rename.getText();
                if (name.length() == 0) {
                    rename.setError("enter valid name");

                    return;
                }
                FileUtils.renameTarget(filepath, rename.getText()
                        .toString());
                refresh();
                dialog.dismiss();

            }
        });
        TextView cancelButton = (TextView) dialog
                .findViewById(R.id.cancel_dialog);
        cancelButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }
    private void refresh() {
        newf = new File(Environment.getExternalStorageDirectory()
                + "/FreeVideoDownloader");
        if (getListFilesmp4(newf) != null) {
            files = getListFilesmp4(newf);
            Collections.sort(files, new Comparator<File>() {

                @Override
                public int compare(File file1, File file2) {
                    long k = file1.lastModified() - file2.lastModified();
                    if(k > 0){
                        return 1;
                    }else if(k == 0){
                        return 0;
                    }else{
                        return -1;
                    }
                }
            });
            adapter = new AdapterMainActivitySavedRecyclerView(getActivity(), files,CompletedFragment.this);
            recyclerView.setAdapter(adapter);

            adapter.notifyDataSetChanged();
        }
    }

    public void deletemydialog(File file, final String filepath){
        final Dialog dialog = new Dialog(
                getActivity());
        dialog.setContentView(R.layout.delete_dialog);

        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        // define the contents of edit dialog
        final TextView txt_file_text = (TextView) dialog
                .findViewById(R.id.txt_file_text);

        txt_file_text.setText(file.getName());
        // dialog save button to save the edited item
        TextView del_button = (TextView) dialog
                .findViewById(R.id.del_button);
        // for updating the list item
        del_button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                FileUtils.deleteTarget(filepath);
                refresh();
                dialog.dismiss();

            }
        });

        // cancel button declaration
        TextView cancelButton = (TextView) dialog
                .findViewById(R.id.cancel_dialog);
        cancelButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });

        dialog.show();


    }
    private List<File> getListFilesmp4(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files = parentDir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    inFiles.addAll(getListFilesmp4(file));
                } else {
                    if (file.getName().toLowerCase().contains("mkv") ||file.getName().toLowerCase().contains("MKV") ||file.getName().toLowerCase().contains("webm") ||file.getName().toLowerCase().contains("mp4") || file.getName().toLowerCase().contains("avi")||file.getName().toLowerCase().contains("3gp")||file.getName().toLowerCase().contains("mov")||file.getName().toLowerCase().contains("flv")||file.getName().toLowerCase().contains("MP4")){
                        inFiles.add(file);
                    }
                }
            }
        } else {
        }
        return inFiles;
    }
    public static String getExtension(String uri) {
        if (uri == null) {
            return null;
        }

        int dot = uri.lastIndexOf(".");
        if (dot >= 0) {
            return uri.substring(dot);
        } else {
            // No extension.
            return "";
        }
    }
}
