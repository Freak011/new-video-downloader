package all.video.downloader.MainActivity.Adapter;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import all.video.downloader.Helping.Utils;
import all.video.downloader.MainActivity.ui.DownloadingFragment;
import all.video.downloader.R;
import all.video.downloader.fetchmainpac.Download;
import all.video.downloader.fetchmainpac.Status;

public final class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder> {
    @NonNull
    private final List<DownloadData> downloads = new ArrayList<>();
    @NonNull
    private final ActionListener actionListener;
    public FileAdapter(@NonNull final DownloadingFragment actionListener) {
        this.actionListener = actionListener;
    }
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.down_list, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.actionButton.setOnClickListener(null);
        holder.actionButton.setEnabled(true);
        final DownloadData downloadData = downloads.get(position);
        String url = "";
        if (downloadData.download != null) {
            url = downloadData.download.getUrl();
        }else {
        }
        final Uri uri = Uri.parse(url);
        final all.video.downloader.fetchmainpac.Status status = downloadData.download.getStatus();
        final Context context = holder.itemView.getContext();
        holder.titleTextView.setText(downloadData.download.getFile().substring(downloadData.download.getFile().lastIndexOf("/") + 1));
        holder.statusTextView.setText(getStatusString(status));
        holder.img_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Uri uri12 = Uri.parse(downloadData.download.getUrl());
                new AlertDialog.Builder(context)
                        .setMessage(context.getString(R.string.delete_title, uri12.getLastPathSegment()))
                        .setPositiveButton(R.string.deletea, (dialog, which) -> actionListener.onRemoveDownload(downloadData.download.getId()))
                        .setNegativeButton(R.string.cancela, null)
                        .show();
            }
        });
        int progress = downloadData.download.getProgress();
        if (progress == -1) {
            progress = 0;
        }
        holder.progressBar.setProgress(progress);
        holder.progressTextView.setText(context.getString(R.string.percent_progress, progress));
        if (downloadData.eta == -1) {
            holder.timeRemainingTextView.setText("");
        } else {
            holder.timeRemainingTextView.setText(Utils.getETAString(context, downloadData.eta));
        }
        if (downloadData.downloadedBytesPerSecond == 0) {
            holder.downloadedBytesPerSecondTextView.setText("");
        } else {
            holder.downloadedBytesPerSecondTextView.setText(Utils.getDownloadSpeedString(context, downloadData.downloadedBytesPerSecond));
        }
        Glide.with(context)
                .load(downloadData.download.getFile())
                .into(holder.img_thumb);
        switch (status) {
            case COMPLETED: {
                holder.actionButton.setImageResource(R.drawable.play_btn);
                holder.actionButton.setOnClickListener(view -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        Toast.makeText(context, "Downloaded Path:" + downloadData.download.getFile(), Toast.LENGTH_LONG).show();
//                        GiraffePlayer.play(context, new VideoInfo(Uri.parse(downloadData.download.getFile())).setBgColor(context.getResources().getColor(R.color.black)));
                        return;
                    }
//                    GiraffePlayer.play(context, new VideoInfo(Uri.parse(downloadData.download.getFile())).setBgColor(context.getResources().getColor(R.color.black)));
                });
                break;
            }
            case FAILED: {
                holder.actionButton.setImageResource(R.drawable.play_btn);
                holder.actionButton.setOnClickListener(view -> {
                    holder.actionButton.setEnabled(false);
                    actionListener.onRetryDownload(downloadData.download.getId());
                });
                break;
            }
            case PAUSED: {
                holder.actionButton.setImageResource(R.drawable.play_btn);
                holder.actionButton.setOnClickListener(view -> {
                    holder.actionButton.setEnabled(false);
                    actionListener.onResumeDownload(downloadData.download.getId());
                });
                break;
            }
            case DOWNLOADING:{
                holder.actionButton.setImageResource(R.drawable.pause_icon);
                holder.actionButton.setOnClickListener(view -> {
                    holder.actionButton.setEnabled(false);
                    actionListener.onPauseDownload(downloadData.download.getId());
                });
                break;
            }
            case QUEUED: {
                holder.actionButton.setOnClickListener(view -> {
                    holder.actionButton.setEnabled(false);
                    actionListener.onPauseDownload(downloadData.download.getId());
                });
                break;
            }
            case ADDED: {
                holder.actionButton.setOnClickListener(view -> {
                    holder.actionButton.setEnabled(false);
                    actionListener.onResumeDownload(downloadData.download.getId());
                });
                break;
            }
            default: {
                break;
            }
        }
        holder.itemView.setOnLongClickListener(v -> {
            final Uri uri12 = Uri.parse(downloadData.download.getUrl());
            new AlertDialog.Builder(context)
                    .setMessage(context.getString(R.string.delete_title, uri12.getLastPathSegment()))
                    .setPositiveButton(R.string.deletea, (dialog, which) -> actionListener.onRemoveDownload(downloadData.download.getId()))
                    .setNegativeButton(R.string.cancela, null)
                    .show();
            return true;
        });
    }
    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
    public void addDownload(@NonNull final all.video.downloader.fetchmainpac.Download download) {
        boolean found = false;
        DownloadData data = null;
        int dataPosition = -1;
        for (int i = 0; i < downloads.size(); i++) {
            final DownloadData downloadData = downloads.get(i);
            if (downloadData.id == download.getId()) {
                data = downloadData;
                dataPosition = i;
                found = true;
                break;
            }
        }
        if (!found) {
            final DownloadData downloadData = new DownloadData();
            downloadData.id = download.getId();
            downloadData.download = download;
            downloads.add(downloadData);
            notifyItemInserted(downloads.size() - 1);
        } else {
            data.download = download;
            notifyItemChanged(dataPosition);
        }
    }
    @Override
    public int getItemCount() {
        return downloads.size();
    }
    public void update(@NonNull final all.video.downloader.fetchmainpac.Download download, long eta, long downloadedBytesPerSecond) {
        for (int position = 0; position < downloads.size(); position++) {
            final DownloadData downloadData = downloads.get(position);
            if (downloadData.id == download.getId()) {
                switch (download.getStatus()) {
                    case REMOVED:
                    case DELETED: {
                        downloads.remove(position);
                        notifyItemRemoved(position);
                        break;
                    }
                    default: {
                        downloadData.download = download;
                        downloadData.eta = eta;
                        downloadData.downloadedBytesPerSecond = downloadedBytesPerSecond;
                        notifyItemChanged(position);
                    }
                }
                return;
            }
        }
    }
    private String getStatusString(Status status) {
        switch (status) {
            case COMPLETED:
                return "Done";
            case DOWNLOADING:
                return "Downloading";
            case FAILED:
                return "Error";
            case PAUSED:
                return "Paused";
            case QUEUED:
                return "Waiting in Queue";
            case REMOVED:
                return "Removed";
            case NONE:
                return "Not Queued";
            default:
                return "Unknown";
        }
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView titleTextView;
        final TextView statusTextView;
        public final ProgressBar progressBar;
        public final TextView progressTextView;
        public final ImageView actionButton;
        final TextView timeRemainingTextView;
        final TextView downloadedBytesPerSecondTextView;
        ImageView img_thumb;
        ImageView img_web;
        ViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            statusTextView = itemView.findViewById(R.id.status_TextView);
            progressBar = itemView.findViewById(R.id.progressBar);
            actionButton = itemView.findViewById(R.id.actionButton);
            progressTextView = itemView.findViewById(R.id.progress_TextView);
            timeRemainingTextView = itemView.findViewById(R.id.remaining_TextView);
            downloadedBytesPerSecondTextView = itemView.findViewById(R.id.downloadSpeedTextView);
            img_thumb=itemView.findViewById(R.id.img_thumb);
            img_web=itemView.findViewById(R.id.img_web);
        }
    }
    public static class DownloadData {
        public int id;
        @Nullable
        public Download download;
        long eta = -1;
        long downloadedBytesPerSecond = 0;
        @Override
        public int hashCode() {
            return id;
        }
        @Override
        public String toString() {
            if (download == null) {
                return "";
            }
            return download.toString();
        }
        @Override
        public boolean equals(Object obj) {
            return obj == this || obj instanceof DownloadData && ((DownloadData) obj).id == id;
        }
    }
    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}