package all.video.downloader.MainActivity;

import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import all.video.downloader.Helping.FileUtils;
import all.video.downloader.Helping.PathAddress;
import all.video.downloader.Helping.TinyDB;
import all.video.downloader.MainActivity.Adapter.AdapterWallet;
import all.video.downloader.R;

public class WalletActivity extends Activity {
    LinearLayout EmailLayout;
    RelativeLayout PINLayout;
    TextView PINText;
    EditText PIN,Email;
    ImageView BackBtn,Next;
    TinyDB tinyDB;
    String saveFile = "";
    private RecyclerView recyclerView;
    private AdapterWallet adapter;
    RelativeLayout NightMoodLayout;
    ImageView Back;
    AlertDialog.Builder builder;
    Dialog dialog;
    TextView openWallet;
    TextView ForgotText;
    GridLayoutManager gridLayoutManager;
    private List<File> files;
    public LinearLayout emptyView;
    File newf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_wallet);
        ForgotText=findViewById(R.id.ForgotText);
        NightMoodLayout=findViewById(R.id.NightMoodLayout);
        tinyDB=new TinyDB(this);
        ForgotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgotSialog();
            }
        });
        PINLayout=findViewById(R.id.PINLayout);
        PINLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        EmailLayout=findViewById(R.id.EmailLayout);
        PINText=findViewById(R.id.PINText);
        PIN=findViewById(R.id.PIN);
        Email=findViewById(R.id.Email);
        BackBtn=findViewById(R.id.BackBtn);
        Next=findViewById(R.id.Next);
        if (TextUtils.equals(tinyDB.getString("myPIN"),"")){
            PINText.setText("Please Select Your PIN");
        }
        if (TextUtils.equals(tinyDB.getString("FName"),"")){
            EmailLayout.setVisibility(View.VISIBLE);
        }
        BackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pin=PIN.getText().toString().trim();
                String name=Email.getText().toString().trim();
                if (TextUtils.equals(tinyDB.getString("myPIN"),"")){
                    if (TextUtils.equals(pin,"")||TextUtils.equals(name,"")){
                        Toast.makeText(WalletActivity.this, "Required all fields", Toast.LENGTH_SHORT).show();
                    }else if (pin.length()<5){
                        Toast.makeText(WalletActivity.this, "PIN should be 5 digits", Toast.LENGTH_SHORT).show();
                    }else{
                        tinyDB.putString("myPIN",pin);
                        tinyDB.putString("FName",name);
                        PINLayout.setVisibility(View.GONE);
                    }
                }else{
                    if (TextUtils.equals(pin,"")){
                        Toast.makeText(WalletActivity.this, "PIN is empty", Toast.LENGTH_SHORT).show();
                    }else{
                        String myPin=tinyDB.getString("myPIN");
                        if (TextUtils.equals(myPin,pin)){
                            PINLayout.setVisibility(View.GONE);
                        }else{
                            ForgotText.setVisibility(View.VISIBLE);
                            Toast.makeText(WalletActivity.this, "Invalid PIN", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });



        builder= new AlertDialog.Builder(WalletActivity.this);
        builder.setView(R.layout.progress_dialog);
        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        tinyDB=new TinyDB(WalletActivity.this);
        saveFile= PathAddress.pathofdownload(WalletActivity.this).getPath();
        newf = new File(Environment.getExternalStorageDirectory()
                + "/.DownloaderVault");
        recyclerView=(RecyclerView) findViewById(R.id.recyclerViewMedia);
        emptyView=findViewById(R.id.emptyView);
        gridLayoutManager = new GridLayoutManager(WalletActivity.this,3);
        recyclerView.setLayoutManager(new LinearLayoutManager(WalletActivity.this));

        files = getListFilesmp4(newf);
        Collections.sort(files, new Comparator<File>() {

            @Override
            public int compare(File file1, File file2) {
                long k = file1.lastModified() - file2.lastModified();
                if(k > 0){
                    return 1;
                }else if(k == 0){
                    return 0;
                }else{
                    return -1;
                }
            }
        });
        if (files.size()>0){
            emptyView.setVisibility(View.GONE);
        }else{
            emptyView.setVisibility(View.VISIBLE);
        }
        adapter = new AdapterWallet(WalletActivity.this, files, WalletActivity.this);
        recyclerView.setAdapter(adapter);
    }

    private void showForgotSialog() {
        final AlertDialog dialog = new AlertDialog.Builder(WalletActivity.this).create();
        View view= LayoutInflater.from(WalletActivity.this).inflate(R.layout.forgot_dialog,null);
        dialog.setView(view);
        EditText rename = (EditText) view
                .findViewById(R.id.rename_file_text);
        TextView PINText2=view.findViewById(R.id.PINText2);
        RelativeLayout NightMoodLayout=view.findViewById(R.id.NightMoodLayout);
        TextView saveButton = (TextView) view
                .findViewById(R.id.rename_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String answer=rename.getText().toString();
                if (TextUtils.equals(tinyDB.getString("FName"),answer)){
                    PINText2.setText("PIN: "+tinyDB.getString("myPIN"));
                }else{
                    Toast.makeText(WalletActivity.this, "Incorrect Name", Toast.LENGTH_SHORT).show();
                    PINText2.setText("Please Type Correct name.");
                }
            }
        });
        dialog.show();
    }

    public void sharedialog(File file) {
        Uri imageUri = FileProvider.getUriForFile(
                WalletActivity.this,
                "all.video.downloader.provider",
                file);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("video/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        intent.putExtra(Intent.EXTRA_STREAM, imageUri);
        startActivity(Intent.createChooser(intent, "Share via :"));
    }

    public void hidedialog(File file) {
        Uri imageUri = FileProvider.getUriForFile(
                WalletActivity.this,
                "all.video.downloader.provider",
                file);
        dialog.show();
        final File des= new File(Environment.getExternalStorageDirectory()+"/FreeVideoDownloader");
        if (!des.exists()) {
            des.mkdir();
        }

        File Destination= new File(Environment.getExternalStorageDirectory()+"/FreeVideoDownloader/"+file.getName());
        try {
            transferImage(file,Destination);
            dialog.dismiss();
        } catch (IOException e) {
            dialog.dismiss();
            Toast.makeText(WalletActivity.this, "First" + e, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
    public void transferImage(File source, File distination) throws IOException {
        FileInputStream inStream = new FileInputStream(source);
        FileOutputStream outStream = new FileOutputStream(distination);
        FileChannel inChannel = inStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outStream.getChannel());
        inStream.close();
        outStream.close();
        try {
            final String where = MediaStore.MediaColumns.DATA + "=?";
            final String[] selectionArgs = new String[]{
                    source.getAbsolutePath()
            };
            final ContentResolver contentResolver = WalletActivity.this.getContentResolver();
            final Uri filesUri = MediaStore.Files.getContentUri("external");
            contentResolver.delete(filesUri, where, selectionArgs);
            source.delete();
            refresh();
        }catch (Throwable e)
        {
            Toast.makeText(WalletActivity.this, "Second"+e, Toast.LENGTH_SHORT).show();
        }
    }

    public void renamedialog(File file, final String filepath){

        final AlertDialog dialog = new AlertDialog.Builder(WalletActivity.this).create();
        View view= LayoutInflater.from(WalletActivity.this).inflate(R.layout.rename_dialog,null);
        dialog.setView(view);
        final TextView rename = (TextView) view
                .findViewById(R.id.rename_file_text);
        rename.setText(file.getName());

        TextView saveButton = (TextView) view
                .findViewById(R.id.rename_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final TextView rename = (TextView) view
                        .findViewById(R.id.rename_file_text);
                final CharSequence name = rename.getText();
                if (name.length() == 0) {
                    rename.setError("enter valid name");

                    return;
                }
                FileUtils.renameTarget(filepath, rename.getText()
                        .toString());
                refresh();
                dialog.dismiss();

            }
        });
        TextView cancelButton = (TextView) view
                .findViewById(R.id.cancel_dialog);
        cancelButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }
    private void refresh() {
        newf = new File(Environment.getExternalStorageDirectory()
                + "/.PlayerVault");
        if (getListFilesmp4(newf) != null) {
            files = getListFilesmp4(newf);
            Collections.sort(files, new Comparator<File>() {

                @Override
                public int compare(File file1, File file2) {
                    long k = file1.lastModified() - file2.lastModified();
                    if(k > 0){
                        return 1;
                    }else if(k == 0){
                        return 0;
                    }else{
                        return -1;
                    }
                }
            });
            adapter = new AdapterWallet(WalletActivity.this, files,WalletActivity.this);
            recyclerView.setAdapter(adapter);

            adapter.notifyDataSetChanged();
        }
    }

    public void deletemydialog(File file, final String filepath){
        final AlertDialog dialog = new AlertDialog.Builder(WalletActivity.this).create();
        View view=LayoutInflater.from(WalletActivity.this).inflate(R.layout.delete_dialog,null);
        dialog.setView(view);
        dialog.setCancelable(false);
        // define the contents of edit dialog
        final TextView txt_file_text = (TextView) view
                .findViewById(R.id.txt_file_text);
        txt_file_text.setText(file.getName());
        // dialog save button to save the edited item
        TextView del_button = (TextView) view
                .findViewById(R.id.del_button);
        // for updating the list item
        del_button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                FileUtils.deleteTarget(filepath);
                refresh();
                dialog.dismiss();

            }
        });

        // cancel button declaration
        TextView cancelButton = (TextView) view
                .findViewById(R.id.cancel_dialog);
        cancelButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });

        dialog.show();


    }
    private List<File> getListFilesmp4(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files = parentDir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    inFiles.addAll(getListFilesmp4(file));
                } else {
                    if (file.getName().toLowerCase().contains("mkv") ||file.getName().toLowerCase().contains("MKV") ||file.getName().toLowerCase().contains("webm") ||file.getName().toLowerCase().contains("mp4") || file.getName().toLowerCase().contains("avi")||file.getName().toLowerCase().contains("3gp")||file.getName().toLowerCase().contains("mov")||file.getName().toLowerCase().contains("flv")||file.getName().toLowerCase().contains("MP4")){
                        inFiles.add(file);
                    }
                }
            }
        } else {
        }
        return inFiles;
    }
    public static String getExtension(String uri) {
        if (uri == null) {
            return null;
        }

        int dot = uri.lastIndexOf(".");
        if (dot >= 0) {
            return uri.substring(dot);
        } else {
            // No extension.
            return "";
        }
    }
    public void nightOn(){
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        float brightness=0.05f;
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
        NightMoodLayout.setVisibility(View.VISIBLE);

    }
    public void nightOff(){
        NightMoodLayout.setVisibility(View.GONE);
    }
}
