package all.video.downloader.MainActivity;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.Toolbar.OnMenuItemClickListener;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import all.video.downloader.AdmobHelper.AdmobHelper;
import all.video.downloader.DownloadingModels.InstagramVideoDownloader;
import all.video.downloader.DownloadingModels.ModelBase;
import all.video.downloader.DownloadingModels.TikTokVideoDownloader;
import all.video.downloader.Helping.Constants;
import all.video.downloader.Helping.OnSwipeTouchListener;
import all.video.downloader.Helping.TinyDB;
import all.video.downloader.History.DialogHistory;
import all.video.downloader.MainActivity.ui.CompletedFragment;
import all.video.downloader.MainActivity.ui.DownloadingFragment;
import all.video.downloader.Modelpat.MainPattModel;
import all.video.downloader.PopupPack.PopUpBottomAdapter;
import all.video.downloader.R;
import all.video.downloader.View.VideoEnabledWebChromeClient;
import all.video.downloader.View.VideoEnabledWebView;
import all.video.downloader.View.WebViewClient;
import all.video.downloader.WAStatus.MainActivityWA;
import de.mrapp.android.tabswitcher.AbstractState;
import de.mrapp.android.tabswitcher.AddTabButtonListener;
import de.mrapp.android.tabswitcher.Animation;
import de.mrapp.android.tabswitcher.Layout;
import de.mrapp.android.tabswitcher.PeekAnimation;
import de.mrapp.android.tabswitcher.PullDownGesture;
import de.mrapp.android.tabswitcher.RevealAnimation;
import de.mrapp.android.tabswitcher.StatefulTabSwitcherDecorator;
import de.mrapp.android.tabswitcher.SwipeGesture;
import de.mrapp.android.tabswitcher.Tab;
import de.mrapp.android.tabswitcher.TabPreviewListener;
import de.mrapp.android.tabswitcher.TabSwitcher;
import de.mrapp.android.tabswitcher.TabSwitcherListener;
import de.mrapp.android.util.ThemeUtil;
import de.mrapp.android.util.multithreading.AbstractDataBinder;
import de.mrapp.util.Condition;

import static all.video.downloader.Helping.Constants.isadOne;
import static de.mrapp.android.util.DisplayUtil.getDisplayWidth;

public class MainActivity extends AppCompatActivity implements TabSwitcherListener {
    public static VideoEnabledWebView webview;
    public LinearLayout HomeLayout;
    public EditText etBrowserURL;
    public static LinearLayout llContainer;
    public static ImageView TuNext, HowTo, moreTab, moreTab2, SideMenu, gotoHome, mDownloading, mFiles, mmHome, mmDownloading, mmFiles,
            DownloadBtn;
    TextView txt_pop_up_title;
    int tu = 1;
    LinearLayout AdPage;
    RelativeLayout AdPage2;
    String type = "main";
    public DialogHistory customDialog;
    ImageView TuImage;
    RelativeLayout TutorialPage;
    TinyDB tinyDB;
    DrawerLayout drawer;
    Context mmContext;
    public static MainActivity getMain;
    ProgressDialog dialog;
    boolean is_float_condition = true;
    public static TextView tabCount, tabCount2;
    public ArrayList<ModelBase> modelBaseArrayList = new ArrayList<ModelBase>();
    public Dialog bottomsheetdialog;
    android.view.animation.Animation animRightToLeft, animLeftToTight;
    String DownloadURL, TypeURL = "";
    public static ArrayList<MainPattModel> mainpattarray = new ArrayList<>();
    public static String pattern_mat_string = "1111111";
    public static String tiktokURL = "";
    public static String site_mat_string = "111111122222";
    private VideoEnabledWebChromeClient webChromeClient;
    boolean isClose = false;
    public static boolean is_insta_on = false;
    public static RelativeLayout BrowserLayout;
    public static ImageView mHome;
    private NativeAd nativeAd;
    public InterstitialAd mInterstitialMain;

    private class State extends AbstractState
            implements AbstractDataBinder.Listener<ArrayAdapter<String>, Tab, ListView, Void>,
            TabPreviewListener {

        private ArrayAdapter<String> adapter;

        State(@NonNull final Tab tab) {
            super(tab);
        }

        public void loadItems(@NonNull final ListView listView) {
            Condition.INSTANCE.ensureNotNull(listView, "The list view may not be null");

            if (adapter == null) {
                dataBinder.addListener(this);
                dataBinder.load(getTab(), listView);
            } else if (listView.getAdapter() != adapter) {
                listView.setAdapter(adapter);
            }
        }

        @Override
        public boolean onLoadData(
                @NonNull final AbstractDataBinder<ArrayAdapter<String>, Tab, ListView, Void> dataBinder,
                @NonNull final Tab key, @NonNull final Void... params) {
            return true;
        }

        @Override
        public void onCanceled(
                @NonNull final AbstractDataBinder<ArrayAdapter<String>, Tab, ListView, Void> dataBinder) {

        }

        @Override
        public void onFinished(
                @NonNull final AbstractDataBinder<ArrayAdapter<String>, Tab, ListView, Void> dataBinder,
                @NonNull final Tab key, @Nullable final ArrayAdapter<String> data,
                @NonNull final ListView view, @NonNull final Void... params) {
            if (getTab().equals(key)) {
                view.setAdapter(data);
                adapter = data;
                dataBinder.removeListener(this);
            }
        }

        @Override
        public final void saveInstanceState(@NonNull final Bundle outState) {
            if (adapter != null && !adapter.isEmpty()) {
                String[] array = new String[adapter.getCount()];

                for (int i = 0; i < array.length; i++) {
                    array[i] = adapter.getItem(i);
                }

                outState.putStringArray(String.format(ADAPTER_STATE_EXTRA, getTab().getTitle()),
                        array);
            }
        }

        @Override
        public void restoreInstanceState(@Nullable final Bundle savedInstanceState) {
            if (savedInstanceState != null) {
                String key = String.format(ADAPTER_STATE_EXTRA, getTab().getTitle());
                String[] items = savedInstanceState.getStringArray(key);

                if (items != null && items.length > 0) {
                    adapter = new ArrayAdapter<>(MainActivity.this,
                            android.R.layout.simple_list_item_1, items);
                }
            }
        }

        @Override
        public boolean onLoadTabPreview(@NonNull final TabSwitcher tabSwitcher,
                                        @NonNull final Tab tab) {
            return !getTab().equals(tab) || adapter != null;
        }

    }

    public void changeColorFloatBasic(Context context, String url) {
        DownloadBtn.setImageResource(R.drawable.app_download2);
        is_float_condition = false;
        mmContext = context;
        DownloadURL = url;
    }

    public void changeColorFloat(final Context context, String url, String type) {
        DownloadBtn.setImageResource(R.drawable.app_download1);
        android.view.animation.Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);
        DownloadBtn.startAnimation(myAnim);
        is_float_condition = true;
        mmContext = context;
        DownloadURL = url;
        TypeURL = type;
    }

    public void getVideo(String URL, final Context context) {
        String BaseURL = "https://alltubedownload.net/json?url=" + URL;
        StringRequest stringRequest = new StringRequest(BaseURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                modelBaseArrayList.clear();
                dialog.dismiss();
                String duration = "";
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String id = jsonObject.getString("id");
                    String title = jsonObject.getString("title");
                    String thumbnail = jsonObject.getString("thumbnail");
                    String videourl = jsonObject.getString("url");
                    try {
                        duration = jsonObject.getString("duration");
                    } catch (Exception e) {
                        duration = "";
                    }
                    JSONArray formatList = jsonObject.getJSONArray("formats");
                    if (formatList != null) {
                        for (int i = 0; i < formatList.length(); i++) {
                            JSONObject object = formatList.getJSONObject(i);
                            if (object.getString("url") != null && object.getString("format_id") != null
                                    && object.getString("ext") != null) {
                                ModelBase modelBase1 = new ModelBase();
                                modelBase1.setVideourl(videourl);
                                modelBase1.setId(id);
                                modelBase1.setTitle(title);
                                modelBase1.setDuration(duration);
                                modelBase1.setThumnail(thumbnail);
                                if (object.getString("url") == null || object.getString("url").isEmpty()) {
                                    modelBase1.setDownurl("Null");
                                } else {
                                    modelBase1.setDownurl(object.getString("url"));
                                }
                                modelBase1.setExt(object.getString("ext"));
                                modelBase1.setFormat(object.getString("format_id"));
                                modelBase1.setSize("");
                                if (modelBase1.getDownurl().contains("m3u8")) {
                                } else if (modelBase1.getDownurl().contains("mp4")) {
                                    modelBaseArrayList.add(modelBase1);
                                }
                            }
                        }
                    }
                    saveMyDialog(modelBaseArrayList, context);
                } catch (JSONException e) {
                    dialog.dismiss();
                    Toast.makeText(context, "Failed! Please try again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(context, "Failed! Please try again", Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue re = Volley.newRequestQueue(context);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        re.add(stringRequest);
    }

    public void showBottomSheetDialog(String id, final Context context, String url) {
        modelBaseArrayList.clear();
        String title = URLUtil.guessFileName(url, null, null);
        ModelBase modelBase = new ModelBase();
        modelBase.setExt("mp4");
        modelBase.setFormat("mp4");
        modelBase.setDuration("null");
        modelBase.setTitle(title);
        modelBase.setDownurl(id);
        modelBase.setThumnail("");
        modelBase.setVideourl(url);
        modelBase.setId(id);
        modelBase.setSize("null");
        modelBaseArrayList.add(modelBase);
        saveMyDialog(modelBaseArrayList, context);
    }

    public void saveMyDialog(final ArrayList<ModelBase> modelBaseArrayLista, final Context context) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE},
                    1213);
        }
        ContextThemeWrapper contextThemeWrapper;
        contextThemeWrapper = new ContextThemeWrapper(context, R.style.AppTheme);
        bottomsheetdialog = new BottomSheetDialog(contextThemeWrapper);
        bottomsheetdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        RelativeLayout rel_cro;
        RecyclerView rec_bottomsheet;
        PopUpBottomAdapter popUpBottomAdapter;
        GridLayoutManager gridLayoutManager;
        TextView txt_detail;
        try {
            bottomsheetdialog.setContentView(R.layout.topsheet_down_popup);
        } catch (Exception e) {
            e.printStackTrace();
        }
        bottomsheetdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        bottomsheetdialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        rec_bottomsheet = (RecyclerView) bottomsheetdialog.findViewById(R.id.rec_bottomsheet);
        ImageView showthumb = bottomsheetdialog.findViewById(R.id.showthumb);
        Glide.with(MainActivity.this).load(modelBaseArrayLista.get(0).getThumnail()).into(showthumb);
        popUpBottomAdapter = new PopUpBottomAdapter(contextThemeWrapper, modelBaseArrayLista);
        gridLayoutManager = new GridLayoutManager(contextThemeWrapper, 1);
        rec_bottomsheet.setLayoutManager(gridLayoutManager);
        Log.i("adap", " set ");
        rec_bottomsheet.setAdapter(popUpBottomAdapter);
        txt_pop_up_title = (TextView) bottomsheetdialog.findViewById(R.id.txt_pop_up_title);
        txt_pop_up_title.setText(modelBaseArrayList.get(0).getTitle());
        bottomsheetdialog.show();
    }

    public void LinkDialog(String type) {
        AlertDialog dialogLink = new AlertDialog.Builder(this).create();
        View view = LayoutInflater.from(this).inflate(R.layout.link_down_popup, null);
        dialogLink.setView(view);
        ImageView icon = view.findViewById(R.id.LinkImage);
        Button Browse = view.findViewById(R.id.Browse);
        TextView ORtext = view.findViewById(R.id.ORtext);
        if (TextUtils.equals(type, "twitter")) {
            Browse.setText("Browse Twitter");
            icon.setImageDrawable(getResources().getDrawable(R.drawable.twitter_icon));
        } else if (TextUtils.equals(type, "tiktok")) {
            Browse.setVisibility(View.GONE);
            ORtext.setVisibility(View.GONE);
            Browse.setText("Browse TikTok");
            icon.setImageDrawable(getResources().getDrawable(R.drawable.tiktok_icon));
        } else if (TextUtils.equals(type, "facebook")) {
            Browse.setText("Browse Facebook");
            icon.setImageDrawable(getResources().getDrawable(R.drawable.facebook_icon));
        } else if (TextUtils.equals(type, "instagram")) {
            Browse.setText("Browse Instagram");
            icon.setImageDrawable(getResources().getDrawable(R.drawable.instagram_icon));
        }
        EditText etLink = view.findViewById(R.id.LinkPaste);
        Browse.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlSearch = etLink.getText().toString().trim();
                if (!urlSearch.contains("youtu")) {
                    if (TextUtils.equals(type, "twitter")) {
                        webview.onResume();
                        webview.loadUrl("https://twitter.com/home");
                        BrowserLayout.setVisibility(View.VISIBLE);
                        HomeLayout.setVisibility(View.GONE);
                        etBrowserURL.setText("https://twitter.com/home");
                    } else if (TextUtils.equals(type, "facebook")) {
                        webview.onResume();
                        webview.loadUrl("https://m.facebook.com/watch/");
                        BrowserLayout.setVisibility(View.VISIBLE);
                        HomeLayout.setVisibility(View.GONE);
                        etBrowserURL.setText("https://m.facebook.com/watch/");
                    } else if (TextUtils.equals(type, "instagram")) {
                        webview.onResume();
                        webview.loadUrl("https://www.instagram.com/explore/tags/videos/");
                        BrowserLayout.setVisibility(View.VISIBLE);
                        HomeLayout.setVisibility(View.GONE);
                        etBrowserURL.setText("https://www.instagram.com/explore/tags/videos/");
                    }
                    dialogLink.dismiss();
                } else {
                    Toast.makeText(MainActivity.this, "Youtube videos are not supported", Toast.LENGTH_SHORT).show();
                }
            }
        });
        Button btDownload = view.findViewById(R.id.LinkDownload);
        btDownload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlSearch = etLink.getText().toString().trim();
                if (!urlSearch.contains("youtu")) {
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    try {
                        CharSequence textToPaste = clipboard.getPrimaryClip().getItemAt(0).getText();
                        if (textToPaste == null) {
                            Toast.makeText(MainActivity.this, "Cope Text First", Toast.LENGTH_SHORT).show();
                        } else {
                            etLink.setText(textToPaste.toString());
                            String URL = etLink.getText().toString();
                            if (TextUtils.equals(URL, "")) {
                                Toast.makeText(MainActivity.this, "Please Paste URL First", Toast.LENGTH_SHORT).show();
                            } else {
                                dialogLink.dismiss();
                                DownloadURL = URL;
                                if (TextUtils.equals(type, "instagram")) {
                                    InstagramVideoDownloader downloader = new InstagramVideoDownloader(MainActivity.this, URL);
                                    downloader.DownloadVideo();
                                } else if (TextUtils.equals(type, "tiktok")) {
                                    TikTokVideoDownloader downloader = new TikTokVideoDownloader(MainActivity.this, URL);
                                    downloader.DownloadVideo();
                                } else {
                                    dialog.show();
                                    getVideo(DownloadURL, MainActivity.this);

                                }
                            }
                        }

                    } catch (Exception e) {
                        return;
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Youtube videos are not supported", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogLink.show();
//        if (interstitialAd.isAdLoaded()){
//            interstitialAd.show();
//        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("pat.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private class Decorator extends StatefulTabSwitcherDecorator<State> {
        @Nullable
        @Override
        protected State onCreateState(@NonNull final Context context,
                                      @NonNull final TabSwitcher tabSwitcher,
                                      @NonNull final View view, @NonNull final Tab tab,
                                      final int index, final int viewType,
                                      @Nullable final Bundle savedInstanceState) {
            if (viewType == 5) {
                State state = new State(tab);
                tabSwitcher.addTabPreviewListener(state);

                if (savedInstanceState != null) {
                    state.restoreInstanceState(savedInstanceState);
                }

                return state;
            }

            return null;
        }

        @Override
        protected void onClearState(@NonNull final State state) {
            tabSwitcher.removeTabPreviewListener(state);
        }

        @Override
        protected void onSaveInstanceState(@NonNull final View view, @NonNull final Tab tab,
                                           final int index, final int viewType,
                                           @Nullable final State state,
                                           @NonNull final Bundle outState) {
            if (state != null) {
                state.saveInstanceState(outState);
            }
        }

        @NonNull
        @Override
        public View onInflateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup parent, final int viewType) {
            View view = inflater.inflate(R.layout.tab_main_screen, parent, false);
//            AudienceNetworkAds.initialize(MainActivity.this);
//            Toolbar toolbar = view.findViewById(R.id.toolbar);
//            toolbar.inflateMenu(R.menu.tab);
//            toolbar.setOnMenuItemClickListener(createToolbarMenuListener());
//            Menu menu = toolbar.getMenu();
            TabSwitcher.setupWithMenu(tabSwitcher, createTabSwitcherButtonListener());
            return view;
        }

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public void onShowTab(@NonNull final Context context,
                              @NonNull final TabSwitcher tabSwitcher, @NonNull final View view,
                              @NonNull final Tab tab, final int index, final int viewType,
                              @Nullable final State state,
                              @Nullable final Bundle savedInstanceState) {
//            Toolbar toolbar = findViewById(R.id.toolbar);
//            toolbar.setVisibility(tabSwitcher.isSwitcherShown() ? View.GONE : View.VISIBLE);
//
            AdPage = view.findViewById(R.id.AdPage);
            AdPage2 = view.findViewById(R.id.AdPage2);

            animRightToLeft = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.right_left);
            animLeftToTight = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.left_right);
            mainpattarray = new ArrayList<MainPattModel>();
            try {
                JSONArray obj = new JSONArray(loadJSONFromAsset());
                for (int i = 0; i < obj.length(); i++) {
                    MainPattModel mainPattModel = new MainPattModel();
                    JSONObject jb = (JSONObject) obj.get(i);
                    String name = jb.getString("name");
                    String pattern = jb.getString("pattern");
                    mainPattModel.setName(name);
                    mainPattModel.setPattern(pattern);
                    mainpattarray.add(mainPattModel);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            tinyDB = new TinyDB(MainActivity.this);
            HowTo = findViewById(R.id.HowTo);
            TutorialPage = findViewById(R.id.TutorialPage);
            TuImage = findViewById(R.id.TuImage);
            TuImage.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this) {
                public void onSwipeTop() {
                }

                public void onSwipeRight() {
                    if (TextUtils.equals(type, "twitter")) {
                       /* switch (tu){
                            case 1:
                                break;
                            case 2:
                                TuImage.setImageResource(R.drawable.twitter_image1);
                                TuImage.startAnimation(animLeftToTight);
                                tu--;
                                break;
                            case 3:
                                TuImage.setImageResource(R.drawable.twitter_image2);
                                TuImage.startAnimation(animLeftToTight);
                                tu--;
                                break;
                            case 4:
                                TuNext.setImageDrawable(getResources().getDrawable(R.drawable.next_btn));
                                TuImage.setImageResource(R.drawable.twitter_image3);
                                TuImage.startAnimation(animLeftToTight);
                                tu--;
                                break;
                        }*/
                    } else {
                        switch (tu) {
                            case 1:
                                break;
                            case 2:
                                TuImage.setImageResource(R.drawable.image1);
                                TuImage.startAnimation(animLeftToTight);
                                tu--;
                                break;
                            case 3:
                                TuImage.setImageResource(R.drawable.image2);
                                TuImage.startAnimation(animLeftToTight);
                                tu--;
                                break;
                            case 4:
                                TuImage.setImageResource(R.drawable.image3);
                                TuImage.startAnimation(animLeftToTight);
                                tu--;
                                break;
                            case 5:
                                TuImage.setImageResource(R.drawable.image4);
                                TuImage.startAnimation(animLeftToTight);
                                TuNext.setImageDrawable(getResources().getDrawable(R.drawable.next_btn));
                                tu--;
                                break;
                        }
                    }

                }

                public void onSwipeLeft() {
                    if (TextUtils.equals(type, "twitter")) {
                      /*  switch (tu){
                            case 1:
                                TuImage.setImageResource(R.drawable.twitter_image2);
                                TuImage.startAnimation(animRightToLeft);
                                tu++;
                                break;
                            case 2:
                                TuImage.setImageResource(R.drawable.twitter_image3);
                                TuImage.startAnimation(animRightToLeft);
                                tu++;
                                break;
                            case 3:
                                TuNext.setImageDrawable(getResources().getDrawable(R.drawable.finish));
                                TuImage.setImageResource(R.drawable.twitter_image4);
                                TuImage.startAnimation(animRightToLeft);
                                tu++;
                                break;
                            case 4:
                                break;
                        }*/
                    } else {
                        switch (tu) {
                            case 1:
                                TuImage.setImageResource(R.drawable.image2);
                                TuImage.startAnimation(animRightToLeft);
                                tu++;
                                break;
                            case 2:
                                TuImage.setImageResource(R.drawable.image3);
                                TuImage.startAnimation(animRightToLeft);
                                tu++;
                                break;
                            case 3:
                                TuImage.setImageResource(R.drawable.image4);
                                TuImage.startAnimation(animRightToLeft);
                                tu++;
                                break;
                            case 4:
                                TuImage.setImageResource(R.drawable.image5);
                                TuImage.startAnimation(animRightToLeft);
                                TuNext.setImageDrawable(getResources().getDrawable(R.drawable.finish));
                                tu++;
                                break;
                            case 5:
                                break;
                        }
                    }

                }

                public void onSwipeBottom() {
                }

            });
            TuNext = findViewById(R.id.TuNext);
            TuNext.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextUtils.equals(type, "twitter")) {
                        /*switch (tu){
                            case 1:
                                TuImage.setImageResource(R.drawable.twitter_image2);
                                TuImage.startAnimation(animRightToLeft);
                                tu++;
                                break;
                            case 2:
                                TuImage.setImageResource(R.drawable.twitter_image3);
                                TuImage.startAnimation(animRightToLeft);
                                tu++;
                                break;
                            case 3:
                                TuNext.setImageDrawable(getResources().getDrawable(R.drawable.finish));
                                TuImage.setImageResource(R.drawable.twitter_image4);
                                TuImage.startAnimation(animRightToLeft);
                                tu++;
                                break;
                            case 4:
                                TuNext.setImageDrawable(getResources().getDrawable(R.drawable.next_btn));
                                TutorialPage.setVisibility(View.GONE);
                                HowTo.setVisibility(View.GONE);
                                tinyDB.putString("howto","ok");
                                tu=1;
                                break;
                        }*/
                    } else {
                        switch (tu) {
                            case 1:
                                TuImage.setImageResource(R.drawable.image2);
                                TuImage.startAnimation(animRightToLeft);
                                tu++;
                                break;
                            case 2:
                                TuImage.setImageResource(R.drawable.image3);
                                TuImage.startAnimation(animRightToLeft);
                                tu++;
                                break;
                            case 3:
                                TuImage.setImageResource(R.drawable.image4);
                                TuImage.startAnimation(animRightToLeft);
                                tu++;
                                break;
                            case 4:
                                TuImage.setImageResource(R.drawable.image5);
                                TuImage.startAnimation(animRightToLeft);
                                TuNext.setImageDrawable(getResources().getDrawable(R.drawable.finish));
                                tu++;
                                break;
                            case 5:
                                TuNext.setImageDrawable(getResources().getDrawable(R.drawable.next_btn));
                                TutorialPage.setVisibility(View.GONE);
                                HowTo.setVisibility(View.GONE);
                                tinyDB.putString("howto", "ok");
                                tu = 1;
                                break;
                        }
                    }
                }
            });
            if (TextUtils.equals(tinyDB.getString("howto"), "ok")) {
                HowTo.setVisibility(View.GONE);
            }
            HowTo.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    tu = 1;
                    type = "main";
                    TuImage.setImageResource(R.drawable.image1);
                    TutorialPage.setVisibility(View.VISIBLE);
                }
            });

            getMain = MainActivity.this;
            HomeLayout = view.findViewById(R.id.HomeLayout);
            llContainer = view.findViewById(R.id.FragmentContainer);
            mHome = view.findViewById(R.id.mHome);
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            SideMenu = findViewById(R.id.SideMenu);
            SideMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.openDrawer(GravityCompat.START);
                    }
                }
            });
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            View header = navigationView.getHeaderView(0);
            header.findViewById(R.id.NewTab).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index = tabSwitcher.getCount();
                    Tab tab = createTab(index);
                    if (tabSwitcher.isSwitcherShown()) {

                        tabSwitcher.addTab(tab, 0, createRevealAnimation());
                    } else {
                        tabSwitcher.addTab(tab, 0, createPeekAnimation());
                    }
                }
            });
            header.findViewById(R.id.AllTabs).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    tabSwitcher.showSwitcher();
                }
            });
            header.findViewById(R.id.VideoVault).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivityForResult(new Intent(MainActivity.this, WalletActivity.class), 3);
                }
            });
            header.findViewById(R.id.RateApp).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri uri = Uri.parse("market://details?id=" + getPackageName());
                    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    try {
                        startActivity(myAppLinkToMarket);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(MainActivity.this, " unable to find market app", Toast.LENGTH_LONG).show();
                    }
                }
            });
            header.findViewById(R.id.ShareApp).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                        String shareMessage = "\nLet me recommend you this aweson Video Downloader\n\n";
                        shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + getPackageName() + "\n\n";
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        startActivity(Intent.createChooser(shareIntent, "choose one"));
                    } catch (Exception e) {
                        //e.toString();
                    }
                }
            });
            header.findViewById(R.id.PrivacyPolicy).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    webview.loadUrl("https://freedownloaderallvideodownloader2020.blogspot.com/2020/10/privacy-policy.html");
                    BrowserLayout.setVisibility(View.VISIBLE);
                    HomeLayout.setVisibility(View.GONE);
                    etBrowserURL.setText("https://freedownloaderallvideodownloader2020.blogspot.com/2020/10/privacy-policy.html");
                }
            });
            moreTab = view.findViewById(R.id.moreTab);
            moreTab.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    tabSwitcher.showSwitcher();
                }
            });
            moreTab2 = view.findViewById(R.id.moreTab2);
            moreTab2.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    tabSwitcher.showSwitcher();
                }
            });
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setMessage("Preparing Downloading Link...");
            DownloadBtn = view.findViewById(R.id.DownloadBtn);
            DownloadBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextUtils.equals(DownloadURL, "")) {
                        Toast.makeText(MainActivity.this, "Invalid URL", Toast.LENGTH_SHORT).show();
                    } else {
                        if (TextUtils.equals(TypeURL, "volley")) {
                            dialog.show();
                            getVideo(DownloadURL, MainActivity.this);
                        } else {
                            showBottomSheetDialog(DownloadURL, MainActivity.this, DownloadURL);
                        }
                    }
                }
            });
            tabCount = view.findViewById(R.id.tabCount);
            tabCount.setText(String.valueOf(tabSwitcher.getCount()));
            tabCount2 = view.findViewById(R.id.tabCount2);
            tabCount2.setText(String.valueOf(tabSwitcher.getCount()));
            mDownloading = view.findViewById(R.id.mDownloading);
            mFiles = view.findViewById(R.id.mFiles);
            mmHome = view.findViewById(R.id.mmHome);
            mmDownloading = view.findViewById(R.id.mmDownloading);
            mmFiles = view.findViewById(R.id.mmFiles);
            etBrowserURL = view.findViewById(R.id.etBrowserURL);
            view.findViewById(R.id.Vimeo).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    webview.onResume();
                    webview.loadUrl("https://vimeo.com/watch");
                    BrowserLayout.setVisibility(View.VISIBLE);
                    HomeLayout.setVisibility(View.GONE);
                    etBrowserURL.setText("https://vimeo.com/watch");
                }
            });
            view.findViewById(R.id.Facebook).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                        webview.onResume();
                        webview.loadUrl("https://m.facebook.com/watch/");
                        BrowserLayout.setVisibility(View.VISIBLE);
                        HomeLayout.setVisibility(View.GONE);
                        etBrowserURL.setText("https://m.facebook.com/watch/");

                }
            });
            view.findViewById(R.id.operVault).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivityForResult(new Intent(MainActivity.this, WalletActivity.class), 3);
                }
            });
            view.findViewById(R.id.Twitter).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                        webview.onResume();
                        webview.loadUrl("https://twitter.com/home");
                        BrowserLayout.setVisibility(View.VISIBLE);
                        HomeLayout.setVisibility(View.GONE);
                        etBrowserURL.setText("https://twitter.com/home");

//                    LinkDialog("twitter");
                }
            });
            view.findViewById(R.id.StatusSaver).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, MainActivityWA.class));
                }
            });
            view.findViewById(R.id.TikTok).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    LinkDialog("tiktok");
                }
            });
            view.findViewById(R.id.Likee).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    webview.onResume();
                    webview.loadUrl("https://likee.com/trending");
                    BrowserLayout.setVisibility(View.VISIBLE);
                    HomeLayout.setVisibility(View.GONE);
                    etBrowserURL.setText("https://likee.com/trending");
                }
            });
            view.findViewById(R.id.IMDB).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    webview.onResume();
                    webview.loadUrl("https://www.imdb.com/");
                    BrowserLayout.setVisibility(View.VISIBLE);
                    HomeLayout.setVisibility(View.GONE);
                    etBrowserURL.setText("https://www.imdb.com/");

                }
            });
            view.findViewById(R.id.TED).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    webview.onResume();
                    webview.loadUrl("https://www.ted.com/");
                    BrowserLayout.setVisibility(View.VISIBLE);
                    HomeLayout.setVisibility(View.GONE);
                    etBrowserURL.setText("https://www.ted.com/");

                }
            });
            view.findViewById(R.id.LiveLeak).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    webview.onResume();
                    webview.loadUrl("https://www.ytpak.com/");
                    BrowserLayout.setVisibility(View.VISIBLE);
                    HomeLayout.setVisibility(View.GONE);
                    etBrowserURL.setText("https://www.ytpak.com/");

                }
            });
            view.findViewById(R.id.Instagram).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                        webview.onResume();
                        webview.loadUrl("https://www.instagram.com/explore/tags/videos/");
                        BrowserLayout.setVisibility(View.VISIBLE);
                        HomeLayout.setVisibility(View.GONE);
                        etBrowserURL.setText("https://www.instagram.com/explore/tags/videos/");

                }
            });

            etBrowserURL.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        String urlSearch = etBrowserURL.getText().toString().trim();

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(etBrowserURL.getWindowToken(),
                                InputMethodManager.RESULT_UNCHANGED_SHOWN);
                        return true;
                    }
                    return false;
                }
            });
            final ProgressBar progressBar = view.findViewById(R.id.progress);
            BrowserLayout = view.findViewById(R.id.BrowserLayout);
            webview = view.findViewById(R.id.webview);
            webview.getSettings().setJavaScriptEnabled(true);
            webview.getSettings().setLoadWithOverviewMode(true);
            webview.getSettings().setPluginState(WebSettings.PluginState.ON);
            webview.getSettings().setDisplayZoomControls(false);
            webview.getSettings().setBuiltInZoomControls(true);
            webview.getSettings().setSupportMultipleWindows(false);
            webview.getSettings().setEnableSmoothTransition(true);
            ViewGroup videoLayout = (ViewGroup) view.findViewById(R.id.videoLayout);
            View loadingView = view.findViewById(R.id.videoLoading);
            webChromeClient = new VideoEnabledWebChromeClient(BrowserLayout, videoLayout, loadingView, webview, progressBar);
            webChromeClient.setOnToggledFullscreen(new VideoEnabledWebChromeClient.ToggledFullscreenCallback() {
                @Override
                public void toggledFullscreen(boolean fullscreen) {
                    if (fullscreen) {
                        WindowManager.LayoutParams attrs = getWindow().getAttributes();
                        attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
                        attrs.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                        getWindow().setAttributes(attrs);
                        if (android.os.Build.VERSION.SDK_INT >= 14) {
                            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
                        }
                    } else {
                        WindowManager.LayoutParams attrs = getWindow().getAttributes();
                        attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
                        attrs.flags &= ~WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                        getWindow().setAttributes(attrs);
                        if (android.os.Build.VERSION.SDK_INT >= 14) {
                            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                        }
                    }
                }
            });
            webview.setWebChromeClient(webChromeClient);
            webview.setWebViewClient(new WebViewClient(etBrowserURL, MainActivity.this, webview));
            ImageView reFresh = findViewById(R.id.reFresh);
            ImageView menuMore = findViewById(R.id.menuMore);
            menuMore.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(MainActivity.this, menuMore);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater()
                            .inflate(R.menu.tab, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.remove_tab_menu_item:
                                    Tab selectedTab = tabSwitcher.getSelectedTab();
                                    if (selectedTab != null) {
                                        tabSwitcher.removeTab(selectedTab);
                                    }
                                    return true;
                                case R.id.add_tab_menu_item:
                                    int index = tabSwitcher.getCount();
                                    Tab tab = createTab(index);
                                    if (tabSwitcher.isSwitcherShown()) {

                                        tabSwitcher.addTab(tab, 0, createRevealAnimation());
                                    } else {
                                        tabSwitcher.addTab(tab, 0, createPeekAnimation());
                                    }
                                    return true;
                                case R.id.clear_tabs_menu_item:
                                    tabSwitcher.clear();
                                    return true;
                                case R.id.history_menu_item:
                                    try {
                                        customDialog = new DialogHistory(MainActivity.this);
                                        customDialog.show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    return true;
                                case R.id.clear_menu_item:
                                    MainActivity.webview.clearCache(true);
                                    Toast.makeText(MainActivity.this, "cleared", Toast.LENGTH_SHORT).show();
                                    return true;
                                case R.id.privacy_menu_item:
                                    webview.onResume();
                                    webview.loadUrl("https://freedownloaderallvideodownloader2020.blogspot.com/2020/10/privacy-policy.html");
                                    BrowserLayout.setVisibility(View.VISIBLE);
                                    HomeLayout.setVisibility(View.GONE);
                                    etBrowserURL.setText("https://freedownloaderallvideodownloader2020.blogspot.com/2020/10/privacy-policy.html");
                                    return true;
                                case R.id.share_menu_item:
                                    try {
                                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                        shareIntent.setType("text/plain");
                                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                                        String shareMessage = "\nLet me recommend you this aweson Video Downloader\n\n";
                                        shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + getPackageName() + "\n\n";
                                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                                        startActivity(Intent.createChooser(shareIntent, "choose one"));
                                    } catch (Exception e) {
                                        //e.toString();
                                    }
                                    return true;
                                case R.id.rate_menu_item:
                                    Uri uri = Uri.parse("market://details?id=" + getPackageName());
                                    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                    try {
                                        startActivity(myAppLinkToMarket);
                                    } catch (ActivityNotFoundException e) {
                                        Toast.makeText(MainActivity.this, " unable to find market app", Toast.LENGTH_LONG).show();
                                    }
                                    return true;

                                default:
                                    return false;
                            }
                        }
                    });

                    popup.show(); //showing popup menu
                }
            });
            ImageView imageView = findViewById(R.id.FrGif);
            imageView.setVisibility(View.GONE);
            Glide.with(MainActivity.this).asGif().load(R.drawable.giftheart).into(imageView);
            imageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isadOne) {
                        isadOne = false;
                        AdPage.setVisibility(View.VISIBLE);
                        AdPage2.setVisibility(View.GONE);
                    } else {
                        isadOne = true;
                        AdPage2.setVisibility(View.VISIBLE);
                        AdPage.setVisibility(View.GONE);
                    }

                }
            });
            AdPage2.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri uri = Uri.parse("market://details?id=best.video.downloader.bestvideodownloader");
                    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    try {
                        startActivity(myAppLinkToMarket);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(MainActivity.this, "unable to find market app", Toast.LENGTH_LONG).show();
                    }
                }
            });
            view.findViewById(R.id.AdCross).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    AdPage.setVisibility(View.GONE);
                }
            });
            view.findViewById(R.id.AdCross2).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    AdPage2.setVisibility(View.GONE);
                }
            });

            AdPage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri uri = Uri.parse("market://details?id=vidme.browser.videodownloader.adblocker.vpn.downloader.app");
                    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    try {
                        startActivity(myAppLinkToMarket);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(MainActivity.this, "unable to find market app", Toast.LENGTH_LONG).show();
                    }
                }
            });
            view.findViewById(R.id.adInstall).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri uri = Uri.parse("market://details?id=vidme.browser.videodownloader.adblocker.vpn.downloader.app");
                    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    try {
                        startActivity(myAppLinkToMarket);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(MainActivity.this, " unable to find market app", Toast.LENGTH_LONG).show();
                    }
                }
            });
            reFresh.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ObjectAnimator rotate = ObjectAnimator.ofFloat(reFresh, "rotation", 0f, 360f);
                    rotate.setDuration(1000);
                    rotate.start();
                    webview.loadUrl(webview.getUrl());
                }
            });
            final EditText etSearch = view.findViewById(R.id.etSearch);
            etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        webview.onResume();
                        String urlSearch = etSearch.getText().toString().trim();
                        if (!urlSearch.contains("youtu")) {
                            if (urlSearch.contains("http") || urlSearch.contains("https") || urlSearch.contains("www.")) {
                                webview.loadUrl(urlSearch);
                                etBrowserURL.setText(urlSearch);
                            } else {
                                webview.loadUrl("https://www.google.com/search?q=" + urlSearch);
                                etBrowserURL.setText("https://www.google.com/search?q=" + urlSearch);
                            }
                            BrowserLayout.setVisibility(View.VISIBLE);
                            HomeLayout.setVisibility(View.GONE);
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(etSearch.getWindowToken(),
                                    InputMethodManager.RESULT_UNCHANGED_SHOWN);
                            return true;
                        } else {
                            Toast.makeText(MainActivity.this, "Youtube videos are not supported", Toast.LENGTH_SHORT).show();
                        }
                        return false;
                    }
                    return false;
                }
            });
            mmHome.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (BrowserLayout.getVisibility() == View.VISIBLE) {
                        webview.onPause();

                    }
                    mHome.setImageResource(R.drawable.home_selector);
                    mDownloading.setImageResource(R.drawable.inprogress_normal);
                    mFiles.setImageResource( R.drawable.downloaded_normal);
                    HomeLayout.setVisibility(View.VISIBLE);
                    BrowserLayout.setVisibility(View.GONE);
                    llContainer.setVisibility(View.GONE);
                    Constants.isURL = 0;
                }
            });
            mmDownloading.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (BrowserLayout.getVisibility() == View.VISIBLE) {
                        webview.onPause();
                        }

                    mHome.setImageResource(R.drawable.home_normal);
                    mDownloading.setImageResource(R.drawable.inprogress_selector);
                    mFiles.setImageResource(R.drawable.downloaded_normal);
                    HomeLayout.setVisibility(View.GONE);
                    BrowserLayout.setVisibility(View.GONE);
                    llContainer.setVisibility(View.VISIBLE);
                    FragmentManager fm = getSupportFragmentManager();
                    DownloadingFragment fragment = new DownloadingFragment();
                    fm.beginTransaction().replace(R.id.FragmentContainer, fragment).commit();
                    }
            });
            mmFiles.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (BrowserLayout.getVisibility() == View.VISIBLE) {
                        webview.onPause();


                    }
                            mHome.setImageResource(R.drawable.home_normal);
                            mDownloading.setImageResource(R.drawable.inprogress_normal);
                            mFiles.setImageResource(R.drawable.downloded_selector);
                            HomeLayout.setVisibility(View.GONE);
                            BrowserLayout.setVisibility(View.GONE);
                            llContainer.setVisibility(View.VISIBLE);
                            FragmentManager fm = getSupportFragmentManager();
                            CompletedFragment fragment = new CompletedFragment();
                            fm.beginTransaction().replace(R.id.FragmentContainer, fragment).commit();
                        }

            });
            if (Constants.isURL == 1) {
                webview.onResume();
                String urlSearch = Constants.url;
                if (urlSearch.contains("http") || urlSearch.contains("https") || urlSearch.contains("www.")) {
                    webview.loadUrl(urlSearch);
                    etBrowserURL.setText(urlSearch);
                } else {
                    webview.loadUrl("https://www.google.com/search?q=" + urlSearch);
                    etBrowserURL.setText("https://www.google.com/search?q=" + urlSearch);
                }
                BrowserLayout.setVisibility(View.VISIBLE);
                HomeLayout.setVisibility(View.GONE);
            } else if (Constants.isURL == 2) {
                if (BrowserLayout.getVisibility() == View.VISIBLE) {
                    webview.onPause();
                }
                mHome.setImageResource(R.drawable.home_normal);
                mDownloading.setImageResource(R.drawable.inprogress_selector);
                mFiles.setImageResource(R.drawable.downloaded_normal);
                HomeLayout.setVisibility(View.GONE);
                llContainer.setVisibility(View.VISIBLE);
                BrowserLayout.setVisibility(View.GONE);
                FragmentManager fm = getSupportFragmentManager();
                DownloadingFragment fragment = new DownloadingFragment();
                fm.beginTransaction().replace(R.id.FragmentContainer, fragment).commit();
            } else if (Constants.isURL == 3) {
                if (BrowserLayout.getVisibility() == View.VISIBLE) {
                    webview.onPause();
                }
                mHome.setImageResource(R.drawable.home_normal);
                mDownloading.setImageResource(R.drawable.inprogress_normal);
                mFiles.setImageResource(R.drawable.downloaded_selector);
                HomeLayout.setVisibility(View.GONE);
                BrowserLayout.setVisibility(View.GONE);
                llContainer.setVisibility(View.VISIBLE);
                FragmentManager fm = getSupportFragmentManager();
                CompletedFragment fragment = new CompletedFragment();
                fm.beginTransaction().replace(R.id.FragmentContainer, fragment).commit();
            }
        }

        @Override
        public int getViewTypeCount() {
            return 5;
        }

        @Override
        public int getViewType(@NonNull final Tab tab, final int index) {
            Bundle parameters = tab.getParameters();
            return parameters != null ? parameters.getInt(VIEW_TYPE_EXTRA) : 0;
        }
    }


    private void loadInterstitialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(MainActivity.this, MainActivity.this.getString(R.string.interstial_id_admob), adRequest,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        mInterstitialMain = interstitialAd;
//                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
//                        Log.i(TAG, "onAdLoaded");


                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error
//                        Log.i(TAG, loadAdError.getMessage());
                        mInterstitialMain = null;

                    }
                });
    }

    private void showIntersitial() {
        try {

            ProgressDialog progressDialog = new ProgressDialog(MainActivity.this, R.style.AppCompatAlertDialogStyle);
            progressDialog.setMessage("Ad Is Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                    mInterstitialMain.show(MainActivity.this);

                }
            }, 1000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (AdPage.getVisibility() == View.VISIBLE) {
            AdPage.setVisibility(View.GONE);
        } else if (AdPage2.getVisibility() == View.VISIBLE) {
            AdPage2.setVisibility(View.GONE);
        } else if (TutorialPage.getVisibility() == View.VISIBLE) {
            TutorialPage.setVisibility(View.GONE);
        } else {
            if (webview.canGoBack()) {
                if (HomeLayout.getVisibility() == View.VISIBLE) {
                    if (isClose) {
                        super.onBackPressed();
                    } else {
                        Toast.makeText(this, "Press Again To Close", Toast.LENGTH_SHORT).show();
                        isClose = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isClose = false;
                            }
                        }, 3000);
                    }
                } else if (llContainer.getVisibility() == View.VISIBLE) {
                    if (BrowserLayout.getVisibility() == View.VISIBLE) {
                        llContainer.setVisibility(View.GONE);
                        webview.onResume();
                    } else {
                        HomeLayout.setVisibility(View.VISIBLE);
                        BrowserLayout.setVisibility(View.GONE);
                        llContainer.setVisibility(View.GONE);
                        Constants.isURL = 0;
                    }
                } else {
                    webview.goBack();
                }
            } else if (HomeLayout.getVisibility() == View.VISIBLE) {
                if (isClose) {
                    super.onBackPressed();
                } else {
                    Toast.makeText(this, "Press Again To Close", Toast.LENGTH_SHORT).show();
                    isClose = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isClose = false;
                        }
                    }, 3000);
                }
            } else if (llContainer.getVisibility() == View.VISIBLE) {
                if (BrowserLayout.getVisibility() == View.VISIBLE) {
                    mHome.setImageResource(R.drawable.home_selector);
                    mDownloading.setImageResource(R.drawable.inprogress_normal);
                    mFiles.setImageResource(R.drawable.downloaded_normal);
                    llContainer.setVisibility(View.GONE);
                    webview.onResume();
                } else {
                    mHome.setImageResource(R.drawable.home_selector);
                    mDownloading.setImageResource(R.drawable.inprogress_normal);
                    mFiles.setImageResource(R.drawable.downloaded_normal);
                    HomeLayout.setVisibility(View.VISIBLE);
                    BrowserLayout.setVisibility(View.GONE);
                    llContainer.setVisibility(View.GONE);
                    Constants.isURL = 0;
                }
            } else if (BrowserLayout.getVisibility() == View.VISIBLE) {
                mHome.setImageResource(R.drawable.home_selector);
                mDownloading.setImageResource(R.drawable.inprogress_normal);
                mFiles.setImageResource(R.drawable.downloaded_normal);
                HomeLayout.setVisibility(View.VISIBLE);
                BrowserLayout.setVisibility(View.GONE);
                Constants.isURL = 0;
                webview.goBack();
                webview.onPause();
            }

        }
    }

    private static class DataBinder extends AbstractDataBinder<ArrayAdapter<String>, Tab, ListView, Void> {

        public DataBinder(@NonNull final Context context) {
            super(context.getApplicationContext());
        }

        @Nullable
        @Override
        protected ArrayAdapter<String> doInBackground(@NonNull final Tab key,
                                                      @NonNull final Void... params) {
            String[] array = new String[10];

            for (int i = 0; i < array.length; i++) {
                array[i] = String.format(Locale.getDefault(), "%s, item %d", key.getTitle(), i + 1);
            }

            try {
                // Simulate a long loading time...
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // There's nothing we can do...
            }

            return new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, array);
        }

        @Override
        protected void onPostExecute(@NonNull final ListView view,
                                     @Nullable final ArrayAdapter<String> data, final long duration,
                                     @NonNull final Void... params) {
            if (data != null) {
                view.setAdapter(data);
            }
        }

    }

    private static final String VIEW_TYPE_EXTRA = MainActivity.class.getName() + "::ViewType";
    private static final String ADAPTER_STATE_EXTRA = State.class.getName() + "::%s::AdapterState";
    private static final int TAB_COUNT = 12;
    private TabSwitcher tabSwitcher;
    private Decorator decorator;
    private Snackbar snackbar;
    private DataBinder dataBinder;

    @NonNull
    private OnApplyWindowInsetsListener createWindowInsetsListener() {
        return new OnApplyWindowInsetsListener() {

            @Override
            public WindowInsetsCompat onApplyWindowInsets(final View v,
                                                          final WindowInsetsCompat insets) {
                int left = insets.getSystemWindowInsetLeft();
                int top = insets.getSystemWindowInsetTop();
                int right = insets.getSystemWindowInsetRight();
                int bottom = insets.getSystemWindowInsetBottom();
                tabSwitcher.setPadding(left, top, right, bottom);
                float touchableAreaTop = top;

                if (tabSwitcher.getLayout() == Layout.TABLET) {
                    touchableAreaTop += getResources()
                            .getDimensionPixelSize(R.dimen.tablet_tab_container_height);
                }

                RectF touchableArea = new RectF(left, touchableAreaTop,
                        getDisplayWidth(MainActivity.this) - right, touchableAreaTop +
                        ThemeUtil.getDimensionPixelSize(MainActivity.this, R.attr.actionBarSize));
                tabSwitcher.addDragGesture(
                        new SwipeGesture.Builder().setTouchableArea(touchableArea).create());
                tabSwitcher.addDragGesture(
                        new PullDownGesture.Builder().setTouchableArea(touchableArea).create());
                return insets;
            }

        };
    }

    @NonNull
    private OnClickListener createAddTabListener() {
        return new OnClickListener() {

            @Override
            public void onClick(final View view) {
                int index = tabSwitcher.getCount();
                Animation animation = createRevealAnimation();
                tabSwitcher.addTab(createTab(index), 0, animation);
            }

        };
    }

    @NonNull
    private OnMenuItemClickListener createToolbarMenuListener() {
        return new OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(final MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.remove_tab_menu_item:
                        Tab selectedTab = tabSwitcher.getSelectedTab();
                        if (selectedTab != null) {
                            tabSwitcher.removeTab(selectedTab);
                        }
                        return true;
                    case R.id.add_tab_menu_item:
                        int index = tabSwitcher.getCount();
                        Tab tab = createTab(index);
                        if (tabSwitcher.isSwitcherShown()) {

                            tabSwitcher.addTab(tab, 0, createRevealAnimation());
                        } else {
                            tabSwitcher.addTab(tab, 0, createPeekAnimation());
                        }
                        return true;
                    case R.id.clear_tabs_menu_item:
                        tabSwitcher.clear();
                        return true;
                    case R.id.history_menu_item:
                        try {
                            customDialog = new DialogHistory(MainActivity.this);
                            customDialog.show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return true;
                    case R.id.clear_menu_item:
                        MainActivity.webview.clearCache(true);
                        Toast.makeText(MainActivity.this, "cleared", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.privacy_menu_item:
                        webview.onResume();
                        webview.loadUrl("https://freedownloaderallvideodownloader2020.blogspot.com/2020/10/privacy-policy.html");
                        BrowserLayout.setVisibility(View.VISIBLE);
                        HomeLayout.setVisibility(View.GONE);
                        etBrowserURL.setText("https://freedownloaderallvideodownloader2020.blogspot.com/2020/10/privacy-policy.html");
                        return true;
                    case R.id.share_menu_item:
                        try {
                            Intent shareIntent = new Intent(Intent.ACTION_SEND);
                            shareIntent.setType("text/plain");
                            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                            String shareMessage = "\nLet me recommend you this aweson Video Downloader\n\n";
                            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + getPackageName() + "\n\n";
                            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                            startActivity(Intent.createChooser(shareIntent, "choose one"));
                        } catch (Exception e) {
                            //e.toString();
                        }
                        return true;
                    case R.id.rate_menu_item:
                        Uri uri = Uri.parse("market://details?id=" + getPackageName());
                        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        try {
                            startActivity(myAppLinkToMarket);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(MainActivity.this, " unable to find market app", Toast.LENGTH_LONG).show();
                        }
                        return true;

                    default:
                        return false;
                }
            }

        };
    }

    @NonNull
    private OnClickListener createTabSwitcherButtonListener() {
        return new OnClickListener() {

            @Override
            public void onClick(final View view) {
                tabSwitcher.toggleSwitcherVisibility();
            }

        };
    }

    @NonNull
    private AddTabButtonListener createAddTabButtonListener() {
        return new AddTabButtonListener() {

            @Override
            public void onAddTab(@NonNull final TabSwitcher tabSwitcher) {
                int index = tabSwitcher.getCount();
                Tab tab = createTab(index);
                tabSwitcher.addTab(tab, 0);
            }

        };
    }

    @NonNull
    private OnClickListener createUndoSnackbarListener(@NonNull final Snackbar snackbar,
                                                       final int index,
                                                       @NonNull final Tab... tabs) {
        return new OnClickListener() {

            @Override
            public void onClick(final View view) {
                snackbar.setAction(null, null);

                if (tabSwitcher.isSwitcherShown()) {
                    tabSwitcher.addAllTabs(tabs, index);
                } else if (tabs.length == 1) {
                    tabSwitcher.addTab(tabs[0], 0, createPeekAnimation());
                }

            }

        };
    }

    @NonNull
    private BaseTransientBottomBar.BaseCallback<Snackbar> createUndoSnackbarCallback(
            final Tab... tabs) {
        return new BaseTransientBottomBar.BaseCallback<Snackbar>() {

            @Override
            public void onDismissed(final Snackbar snackbar, final int event) {
                if (event != DISMISS_EVENT_ACTION) {
                    for (Tab tab : tabs) {
                        tabSwitcher.clearSavedState(tab);
                        decorator.clearState(tab);
                    }
                }
            }
        };
    }

    private void showUndoSnackbar(@NonNull final CharSequence text, final int index,
                                  @NonNull final Tab... tabs) {
        snackbar = Snackbar.make(tabSwitcher, text, Snackbar.LENGTH_LONG).setActionTextColor(
                ContextCompat.getColor(this, R.color.snackbar_action_text_color));
        snackbar.setAction(R.string.undo, createUndoSnackbarListener(snackbar, index, tabs));
        snackbar.addCallback(createUndoSnackbarCallback(tabs));
        snackbar.show();
    }

    @NonNull
    private Animation createRevealAnimation() {
        float x = 0;
        float y = 0;
        View view = getNavigationMenuItem();

        if (view != null) {
            int[] location = new int[2];
            view.getLocationInWindow(location);
            x = location[0] + (view.getWidth() / 2f);
            y = location[1] + (view.getHeight() / 2f);
        }

        return new RevealAnimation.Builder().setX(x).setY(y).create();
    }

    @NonNull
    private Animation createPeekAnimation() {
        return new PeekAnimation.Builder().setX(tabSwitcher.getWidth() / 2f).create();
    }

    @Nullable
    private View getNavigationMenuItem() {
        Toolbar[] toolbars = tabSwitcher.getToolbars();

        if (toolbars != null) {
            Toolbar toolbar = toolbars.length > 1 ? toolbars[1] : toolbars[0];
            int size = toolbar.getChildCount();

            for (int i = 0; i < size; i++) {
                View child = toolbar.getChildAt(i);

                if (child instanceof ImageButton) {
                    return child;
                }
            }
        }

        return null;
    }

    @NonNull
    private Tab createTab(final int index) {
        CharSequence title = getString(R.string.tab_title, index + 1);
        Tab tab = new Tab(title);
        Bundle parameters = new Bundle();
        parameters.putInt(VIEW_TYPE_EXTRA, index % 3);
        tab.setParameters(parameters);
        return tab;
    }

    @Override
    public final void onSwitcherShown(@NonNull final TabSwitcher tabSwitcher) {

    }

    @Override
    public final void onSwitcherHidden(@NonNull final TabSwitcher tabSwitcher) {
        if (snackbar != null) {
            snackbar.dismiss();
        }
    }

    @Override
    public final void onSelectionChanged(@NonNull final TabSwitcher tabSwitcher,
                                         final int selectedTabIndex,
                                         @Nullable final Tab selectedTab) {

    }

    @Override
    public final void onTabAdded(@NonNull final TabSwitcher tabSwitcher, final int index,
                                 @NonNull final Tab tab, @NonNull final Animation animation) {
//        inflateMenu();
        TabSwitcher.setupWithMenu(tabSwitcher, createTabSwitcherButtonListener());
    }

    @Override
    public final void onTabRemoved(@NonNull final TabSwitcher tabSwitcher, final int index,
                                   @NonNull final Tab tab, @NonNull final Animation animation) {
        if (tabSwitcher.getCount() == 0) {
            finish();
        } else {
            CharSequence text = getString(R.string.removed_tab_snackbar, tab.getTitle());
            showUndoSnackbar(text, index, tab);
//            inflateMenu();
            TabSwitcher.setupWithMenu(tabSwitcher, createTabSwitcherButtonListener());
        }
    }

    @Override
    public final void onAllTabsRemoved(@NonNull final TabSwitcher tabSwitcher,
                                       @NonNull final Tab[] tabs,
                                       @NonNull final Animation animation) {
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1213: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE},
                            1213);
                }
                return;
            }
        }
    }

    @Override
    protected final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dataBinder = new DataBinder(this);
        decorator = new Decorator();
        tabSwitcher = findViewById(R.id.tab_switcher);
        tabSwitcher.clearSavedStatesWhenRemovingTabs(false);
        ViewCompat.setOnApplyWindowInsetsListener(tabSwitcher, createWindowInsetsListener());
        tabSwitcher.setDecorator(decorator);
        tabSwitcher.addListener(this);
        tabSwitcher.showToolbars(true);
        tabSwitcher.addTab(createTab(0));
        tabSwitcher.showAddTabButton(createAddTabButtonListener());
        tabSwitcher.setToolbarNavigationIcon(R.drawable.ic_plus_24dp, createAddTabListener());
        TabSwitcher.setupWithMenu(tabSwitcher, createTabSwitcherButtonListener());
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        AdmobHelper.getInstance().loadInterstitial(MainActivity.this, "");
        refreshAd();
    }


    private void refreshAd() {
        AdLoader.Builder builder = new AdLoader.Builder(this, MainActivity.this.getString(R.string.native_id_admob));

        builder.forNativeAd(
                new NativeAd.OnNativeAdLoadedListener() {
                    // OnLoadedListener implementation.
                    @Override
                    public void onNativeAdLoaded(NativeAd nativeAd) {
                        // If this callback occurs after the activity is destroyed, you must call
                        // destroy and return or you may get a memory leak.
                        boolean isDestroyed = false;
//                        refresh.setEnabled(true);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            isDestroyed = isDestroyed();
                        }
                        if (isDestroyed || isFinishing() || isChangingConfigurations()) {
                            nativeAd.destroy();
                            return;
                        }
                        // You must call destroy on old ads when you are done with them,
                        // otherwise you will have a memory leak.
                        if (MainActivity.this.nativeAd != null) {
                            MainActivity.this.nativeAd.destroy();
                        }
                        MainActivity.this.nativeAd = nativeAd;
                        FrameLayout frameLayout = findViewById(R.id.native_ad_container);
                        NativeAdView adView =
                                (NativeAdView) getLayoutInflater().inflate(R.layout.native_ad_layout, null);
                        populateNativeAdView(nativeAd, adView);
                        frameLayout.removeAllViews();
                        frameLayout.addView(adView);
                    }
                });

//        VideoOptions videoOptions =
//                new VideoOptions.Builder().setStartMuted(startVideoAdsMuted.isChecked()).build();
//
//        NativeAdOptions adOptions =
//                new NativeAdOptions.Builder().setVideoOptions(videoOptions).build();

//        builder.withNativeAdOptions(adOptions);
        AdLoader adLoader =
                builder
                        .withAdListener(
                                new AdListener() {
                                    @Override
                                    public void onAdFailedToLoad(LoadAdError loadAdError) {
//                                        refresh.setEnabled(true);
                                        String error =
                                                String.format(
                                                        "domain: %s, code: %d, message: %s",
                                                        loadAdError.getDomain(),
                                                        loadAdError.getCode(),
                                                        loadAdError.getMessage());
                                        Toast.makeText(
                                                MainActivity.this,
                                                "Failed to load native ad with error " + error,
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                })
                        .build();

        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void populateNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        // Set the media view.
        adView.setMediaView((MediaView) adView.findViewById(R.id.ad_media));

        // Set other ad assets.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

        // The headline and mediaContent are guaranteed to be in every NativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        adView.getMediaView().setMediaContent(nativeAd.getMediaContent());

        // These assets aren't guaranteed to be in every NativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad.
        adView.setNativeAd(nativeAd);

        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = nativeAd.getMediaContent().getVideoController();

        // Updates the UI to say whether or not this ad has a video asset.
        if (vc.hasVideoContent()) {


            // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
            // VideoController will call methods on this object when events occur in the video
            // lifecycle.
            vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                @Override
                public void onVideoEnd() {
                    // Publishers should allow native ads to complete video playback before
                    // refreshing or replacing them with another ad in the same UI location.
//                    refresh.setEnabled(true);
//                    videoStatus.setText("Video status: Video playback has ended.");
                    super.onVideoEnd();
                }
            });
        }
//        else {
//            videoStatus.setText("Video status: Ad does not contain a video asset.");
//            refresh.setEnabled(true);
//        }
    }

}
