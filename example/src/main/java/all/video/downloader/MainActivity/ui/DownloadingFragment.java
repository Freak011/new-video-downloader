package all.video.downloader.MainActivity.ui;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.facebook.ads.AdView;
import com.facebook.ads.AudienceNetworkAds;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.tonyodev.fetch2core.DownloadBlock;
import com.tonyodev.fetch2core.Downloader;
import com.tonyodev.fetch2core.Func;
import com.tonyodev.fetch2okhttp.OkHttpDownloader;

import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;

import all.video.downloader.Helping.Constants;
import all.video.downloader.Helping.TinyDB;
import all.video.downloader.MainActivity.Adapter.ActionListener;
import all.video.downloader.MainActivity.Adapter.FileAdapter;
import all.video.downloader.MainActivity.MainActivity;
import all.video.downloader.R;
import all.video.downloader.fetchmainpac.AbstractFetchListener;
import all.video.downloader.fetchmainpac.Download;
import all.video.downloader.fetchmainpac.Error;
import all.video.downloader.fetchmainpac.Fetch;
import all.video.downloader.fetchmainpac.FetchConfiguration;
import all.video.downloader.fetchmainpac.FetchListener;
import all.video.downloader.fetchmainpac.NetworkType;
import all.video.downloader.fetchmainpac.Status;

public class DownloadingFragment extends Fragment implements ActionListener {
    boolean is_first_check=false;
    private static final int STORAGE_PERMISSION_CODE = 200;
    private static final long UNKNOWN_REMAINING_TIME = -1;
    private static final long UNKNOWN_DOWNLOADED_BYTES_PER_SECOND = 0;
    private static final int GROUP_ID = "listGroup".hashCode();
    static final String FETCH_NAMESPACE = "DownloadListActivity";
    private AdView adView;
    private View mainView;
    private FileAdapter fileAdapter;
    private all.video.downloader.fetchmainpac.Fetch fetch;
    private com.google.android.gms.ads.AdView mAdView;

    private LinearLayout adLayout;
    TinyDB tinyDB;
    public DownloadingFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_downloading, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View rootView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        tinyDB=new TinyDB(getActivity());
//        AudienceNetworkAds.initialize(getActivity());
//
//        // Find the Ad Container
//        LinearLayout adContainer = (LinearLayout) rootView.findViewById(R.id.banner_container);
//        if(((MainActivity) getActivity()).adViewBanner.getParent() != null) {
//            ((ViewGroup)((MainActivity) getActivity()).adViewBanner.getParent()).removeView(((MainActivity) getActivity()).adViewBanner); // <- fix
//        }
        // Add the ad view to your activity layout
//        adContainer.addView(((MainActivity) getActivity()).adViewBanner);
//        MobileAds.initialize(getActivity(), new OnInitializationCompleteListener() {
//            @Override
//            public void onInitializationComplete(InitializationStatus initializationStatus) {
//            }
//        });
//        mAdView = rootView.findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);
//        mAdView.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                mAdView.setVisibility(View.VISIBLE);
//                // Code to be executed when an ad finishes loading.
//            }
//
//            @Override
//            public void onAdFailedToLoad(int errorCode) {
////                adContainer.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onAdOpened() {
//                // Code to be executed when an ad opens an overlay that
//                // covers the screen.
//            }
//
//            @Override
//            public void onAdClicked() {
//                // Code to be executed when the user clicks on an ad.
//            }
//
//            @Override
//            public void onAdLeftApplication() {
//                // Code to be executed when the user has left the app.
//            }
//
//            @Override
//            public void onAdClosed() {
//                // Code to be executed when the user is about to return
//                // to the app after tapping on an ad.
//            }
//        });
        Constants.isURL=2;
        all.video.downloader.fetchmainpac.FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(getActivity())
                .setDownloadConcurrentLimit(10)
                .enableRetryOnNetworkGain(true)
                .setHttpDownloader(new OkHttpDownloader(Downloader.FileDownloaderType.SEQUENTIAL))
                .build();
        fetch = Fetch.Impl.getInstance(fetchConfiguration);
        setUpViews(rootView);
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser) {

            onResume();
        } else {

        }
    }
    private void setUpViews(View view) {
        final SwitchCompat networkSwitch = view.findViewById(R.id.networkSwitch);
        final RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        mainView = view.findViewById(R.id.activity_main);

        LinearLayoutManager manager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        networkSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                fetch.setGlobalNetworkType(all.video.downloader.fetchmainpac.NetworkType.WIFI_ONLY);
            } else {
                fetch.setGlobalNetworkType(NetworkType.ALL);
            }
        });
        fileAdapter = new FileAdapter(this);
        recyclerView.setAdapter(fileAdapter);

    }
    @Override
    public void onResume() {
        super.onResume();
        fetch.getDownloadsWithStatus(all.video.downloader.fetchmainpac.Status.CANCELLED, new Func<List<all.video.downloader.fetchmainpac.Download>>() {
            @Override
            public void call(@NotNull List<all.video.downloader.fetchmainpac.Download> result) {

                Collections.sort(result, (first, second) -> Long.compare(first.getCreated(), second.getCreated()));
                for (all.video.downloader.fetchmainpac.Download download : result) {
                    fileAdapter.addDownload(download);
                }

            }
        });

        fetch.getDownloadsWithStatus(all.video.downloader.fetchmainpac.Status.PAUSED, new Func<List<all.video.downloader.fetchmainpac.Download>>() {
            @Override
            public void call(@NotNull List<all.video.downloader.fetchmainpac.Download> result) {

                Collections.sort(result, (first, second) -> Long.compare(first.getCreated(), second.getCreated()));
                for (all.video.downloader.fetchmainpac.Download download : result) {
                    fileAdapter.addDownload(download);
                }

            }
        });

        fetch.getDownloadsWithStatus(all.video.downloader.fetchmainpac.Status.FAILED, new Func<List<all.video.downloader.fetchmainpac.Download>>() {
            @Override
            public void call(@NotNull List<all.video.downloader.fetchmainpac.Download> result) {

                Collections.sort(result, (first, second) -> Long.compare(first.getCreated(), second.getCreated()));
                for (all.video.downloader.fetchmainpac.Download download : result) {
                    fileAdapter.addDownload(download);
                }

            }
        });

        fetch.getDownloadsWithStatus(all.video.downloader.fetchmainpac.Status.DOWNLOADING, new Func<List<all.video.downloader.fetchmainpac.Download>>() {
            @Override
            public void call(@NotNull List<all.video.downloader.fetchmainpac.Download> result) {


                Collections.sort(result, (first, second) -> Long.compare(first.getCreated(), second.getCreated()));
                for (all.video.downloader.fetchmainpac.Download download : result) {
                    fileAdapter.addDownload(download);
                }
                try {
                    if (result.size() < 1) {
//                        ((MainActivity) getActivity()).putnullbadge1();
                    } else {
//                        ((MainActivity) getActivity()).putnewbadge1("" + result.size());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).addListener(fetchListener);
    }
    private final FetchListener fetchListener = new AbstractFetchListener() {
        @Override
        public void onError(@NotNull all.video.downloader.fetchmainpac.Download download, @NotNull Error error, @org.jetbrains.annotations.Nullable Throwable throwable) {
            super.onError(download, error, throwable);
            fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);

        }

        @Override
        public void onStarted(@NotNull all.video.downloader.fetchmainpac.Download download, @NotNull List<? extends DownloadBlock> downloadBlocks, int totalBlocks) {

        }

        @Override
        public void onDownloadBlockUpdated(@NotNull all.video.downloader.fetchmainpac.Download download, @NotNull DownloadBlock downloadBlock, int totalBlocks) {

        }

        @Override
        public void onAdded(@NotNull all.video.downloader.fetchmainpac.Download download) {
            fileAdapter.addDownload(download);
        }

        @Override
        public void onQueued(@NotNull all.video.downloader.fetchmainpac.Download download, boolean waitingOnNetwork) {
            fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
        }

        @Override
        public void onCompleted(@NotNull all.video.downloader.fetchmainpac.Download download) {
            fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fetch.getDownloadsWithStatus(all.video.downloader.fetchmainpac.Status.DOWNLOADING, new Func<List<all.video.downloader.fetchmainpac.Download>>() {
                @Override
                public void call(@NotNull List<all.video.downloader.fetchmainpac.Download> result) {
                    try{
                        if (result.size()<1){
//                            ((MainActivity)getActivity()).putnullbadge1();
                        }else {
//                            ((MainActivity)getActivity()).putnewbadge1("" + result.size());
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void onProgress(@NotNull all.video.downloader.fetchmainpac.Download download, long etaInMilliseconds, long downloadedBytesPerSecond) {
            if (is_first_check){

            }else{
                //onResume();
            }
            fileAdapter.update(download, etaInMilliseconds, downloadedBytesPerSecond);
        }

        @Override
        public void onPaused(@NotNull all.video.downloader.fetchmainpac.Download download) {
            fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fetch.getDownloadsWithStatus(all.video.downloader.fetchmainpac.Status.DOWNLOADING, new Func<List<all.video.downloader.fetchmainpac.Download>>() {
                @Override
                public void call(@NotNull List<all.video.downloader.fetchmainpac.Download> result) {


                    try {
                        if (result.size() < 1) {
//                            ((MainActivity) getActivity()).putnullbadge1();
                        } else {
//                            ((MainActivity) getActivity()).putnewbadge1("" + result.size());
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

//}
                }
            });
            //    fetch.pause(download.getId());
        }

        @Override
        public void onResumed(@NotNull all.video.downloader.fetchmainpac.Download download) {
            fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fetch.getDownloadsWithStatus(all.video.downloader.fetchmainpac.Status.DOWNLOADING, new Func<List<all.video.downloader.fetchmainpac.Download>>() {
                @Override
                public void call(@NotNull List<all.video.downloader.fetchmainpac.Download> result) {
                    try{
                        if (result.size()<1){
//                            ((MainActivity)getActivity()).putnullbadge1();
                        }else {
//                            ((MainActivity)getActivity()).putnewbadge1("" + result.size());
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void onCancelled(@NotNull all.video.downloader.fetchmainpac.Download download) {
            fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fetch.getDownloadsWithStatus(all.video.downloader.fetchmainpac.Status.DOWNLOADING, new Func<List<all.video.downloader.fetchmainpac.Download>>() {
                @Override
                public void call(@NotNull List<all.video.downloader.fetchmainpac.Download> result) {
                    try{
                        if (result.size()<1){
//                            ((MainActivity)getActivity()).putnullbadge1();
                        }else {
//                            ((MainActivity)getActivity()).putnewbadge1("" + result.size());
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void onRemoved(@NotNull all.video.downloader.fetchmainpac.Download download) {
            fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fetch.getDownloadsWithStatus(all.video.downloader.fetchmainpac.Status.DOWNLOADING, new Func<List<all.video.downloader.fetchmainpac.Download>>() {
                @Override
                public void call(@NotNull List<all.video.downloader.fetchmainpac.Download> result) {

                    try{

                        if (result.size()<1){
//                            ((MainActivity)getActivity()).putnullbadge1();
                        }else {
//                            ((MainActivity)getActivity()).putnewbadge1("" + result.size());
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void onDeleted(@NotNull all.video.downloader.fetchmainpac.Download download) {
            fileAdapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
            fetch.getDownloadsWithStatus(Status.DOWNLOADING, new Func<List<all.video.downloader.fetchmainpac.Download>>() {
                @Override
                public void call(@NotNull List<Download> result) {


                    try{

                        if (result.size()<1){
//                            ((MainActivity)getActivity()).putnullbadge1();
                        }else {
//                            ((MainActivity)getActivity()).putnewbadge1("" + result.size());
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }




                }
            });
        }
    };
    @Override
    public void onPauseDownload(int id) {
        fetch.pause(id);
    }

    @Override
    public void onResumeDownload(int id) {
        fetch.resume(id);
    }

    @Override
    public void onRemoveDownload(int id) {
        fetch.remove(id);
    }

    @Override
    public void onRetryDownload(int id) {
        fetch.retry(id);
    }
}
