package all.video.downloader.MainActivity.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.Collections;
import java.util.List;

import all.video.downloader.Helping.MimeTypes;
import all.video.downloader.MainActivity.WalletActivity;
import all.video.downloader.R;
import tcking.github.com.giraffeplayer2.GiraffePlayer;
import tcking.github.com.giraffeplayer2.VideoInfo;

public class AdapterWallet extends RecyclerView.Adapter<AdapterWallet.RecyclerViewHolder> {
    private static int viewtypeinit = 0;
    List<File> list;
    Context context;
    WalletActivity completedFragment;
    public AdapterWallet(Context con, List<File> list1, WalletActivity completedFragment){
        this.context = con;
        this.list = list1;
        Collections.reverse(this.list);
        this.completedFragment=completedFragment;
    }
    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_activity_saved_files, null, false);
        return new RecyclerViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.bindView(position);
        Glide
                .with(context)
                .load(list.get(position).getAbsolutePath())
                .thumbnail(0.1f)
                .into(holder.img_thumb);
        if (list.size()>0){
            ((WalletActivity)completedFragment).emptyView.setVisibility(View.GONE);
        }else{
            ((WalletActivity)completedFragment).emptyView.setVisibility(View.VISIBLE);
        }
        holder.MenuBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, holder.MenuBTN);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.videow, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.rename:
                                ((WalletActivity)completedFragment).renamedialog(list.get(position),list.get(position).getAbsolutePath());
                                return true;
                            case R.id.delete:
                                ((WalletActivity)completedFragment).deletemydialog(list.get(position),list.get(position).getAbsolutePath());
                                return true;
                            case R.id.share:
                                ((WalletActivity)completedFragment).sharedialog(list.get(position));
                                return true;
                            case R.id.hide:
                                ((WalletActivity)completedFragment).hidedialog(list.get(position));
                                return true;
                            default:
                                return false;
                        }
                    }
                });

                popup.show(); //showing popup menu
            }
        });
        holder.tvFileName.setText(list.get(position).getName());
        String value = null;
        long Filesize = getFolderSize(list.get(position)) / 1024;//call function and convert bytes into Kb
        if (Filesize >= 1024)
            value = Filesize / 1024 + " Mb";
        else
            value = Filesize + " Kb";
        holder.tvFileSize.setText(value);
        holder.playbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFile(new File(list.get(position).getAbsolutePath()));
            }
        });
    }
    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
    @Override
    public int getItemCount() {
        return list.size();
    }
    public class RecyclerViewHolder extends RecyclerView.ViewHolder  {
        ImageView img_thumb,MenuBTN;
        TextView tvFileName,tvFileSize;
        RelativeLayout playbtn;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            try {
                init(itemView);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        private void init(View itemView) {
            playbtn=itemView.findViewById(R.id.playbtn);
            img_thumb = (ImageView) itemView.findViewById(R.id.img_thumb);
            MenuBTN=itemView.findViewById(R.id.MenuBTN);
            tvFileName=(TextView) itemView.findViewById(R.id.tvFileName);
            tvFileSize=(TextView) itemView.findViewById(R.id.tvFileSize);
        }
        public void bindView(int position) {
        }
    }
    public static long getFolderSize(File f) {
        long size = 0;
        if (f.isDirectory()) {
            for (File file : f.listFiles()) {
                size += getFolderSize(file);
            }
        } else {
            size = f.length();
        }
        return size;
    }
    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);
        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable)drawable).getBitmap() != null;
        }
        return hasImage;
    }

    public void openFile(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        final String ext = getExtension(file.getAbsoluteFile().toString());


        String str1=file.getAbsolutePath();

        Uri uri=  Uri.parse(str1);
        String mimeType = MimeTypes.getMimeType(file.getName());
        intent.setDataAndType(uri, mimeType);

        if (ext.contains("mp3")||ext.contains("wav")){
            try {
                Intent intent11 = new Intent();
                intent11.setAction(Intent.ACTION_VIEW);
                intent11.setDataAndType(uri, "audio/*");
                context.startActivity(intent11);
            }catch (Exception e){
                try{
                    Uri uri1=  Uri.parse(str1);
                    Intent intent11 = new Intent();
                    intent11.setAction(Intent.ACTION_VIEW);
                    intent11.setDataAndType(uri1, "audio/*");
                    context.startActivity(intent11);
                }catch (Exception ea){
                    ea.printStackTrace();
                    Toast.makeText(context, "No application to open file",
                            Toast.LENGTH_SHORT).show();

                }
            }

        }else if(ext.contains("png")||ext.contains("jpg")||ext.contains("jpeg")){

            Intent intent1 = new Intent();
            intent1.setAction(Intent.ACTION_VIEW);
            intent1.setDataAndType(uri, "image/*");
            context.startActivity(Intent.createChooser(intent1,"Open file"));


        }else {


            try{
                GiraffePlayer.play(context, new VideoInfo(uri));
            }catch (Exception ea){
                ea.printStackTrace();
                Toast.makeText(context, "No application to open file",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
    public static String getExtension(String uri) {
        if (uri == null) {
            return null;
        }

        int dot = uri.lastIndexOf(".");
        if (dot >= 0) {
            return uri.substring(dot);
        } else {
            // No extension.
            return "";
        }
    }
}