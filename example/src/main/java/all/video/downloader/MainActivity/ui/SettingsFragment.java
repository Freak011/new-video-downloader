package all.video.downloader.MainActivity.ui;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;


import all.video.downloader.BuildConfig;
import all.video.downloader.MainActivity.MainActivity;

import all.video.downloader.R;
import all.video.downloader.View.VideoEnabledWebChromeClient;
import all.video.downloader.View.VideoEnabledWebView;

public class SettingsFragment extends Fragment {
    RelativeLayout ClearCache,Rate,Share,UserAgreement,Privacy,Update;
    private VideoEnabledWebChromeClient webChromeClient;

    public SettingsFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ClearCache=view.findViewById(R.id.ClearCache);
        ClearCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.webview.clearCache(true);
                Toast.makeText(getActivity(), "cleared", Toast.LENGTH_SHORT).show();
            }
        });
        Rate=view.findViewById(R.id.Rate);
        Rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(myAppLinkToMarket);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), " unable to find market app", Toast.LENGTH_LONG).show();
                }
            }
        });
        Share=view.findViewById(R.id.Share);
        Share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                    String shareMessage= "\nLet me recommend you this aweson Video Downloader\n\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch(Exception e) {
                    //e.toString();
                }
            }
        });
        UserAgreement=view.findViewById(R.id.UserAgreement);
        UserAgreement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialog=new AlertDialog.Builder(getActivity()).create();
                View view1=LayoutInflater.from(getActivity()).inflate(R.layout.link_pop_up_lay,null);
                dialog.setView(view1);
                VideoEnabledWebView webview=view1.findViewById(R.id.WebView);
                RelativeLayout BrowserLayout=view1.findViewById(R.id.BrowserLayout);
                ProgressBar progressBar=view1.findViewById(R.id.progress);

                ViewGroup videoLayout = (ViewGroup) view1.findViewById(R.id.videoLayout);
                View loadingView =  view1.findViewById(R.id.videoLoading);
                webChromeClient= new VideoEnabledWebChromeClient(BrowserLayout, videoLayout,
                        loadingView, webview, progressBar);
                webview.setWebChromeClient(webChromeClient);
                webview.getSettings().setJavaScriptEnabled(true);
                webview.getSettings().setLoadWithOverviewMode(true);
                webview.getSettings().setPluginState(WebSettings.PluginState.ON);
                webview.getSettings().setDisplayZoomControls(false);
                webview.getSettings().setBuiltInZoomControls(true);
                webview.getSettings().setSupportMultipleWindows(false);
                webview.getSettings().setEnableSmoothTransition(true);
                webview.loadUrl("https://freedownloaderallvideodownloader2020.blogspot.com/2020/10/privacy-policy.html");
                dialog.show();
            }
        });
        Privacy=view.findViewById(R.id.Privacy);
        Privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialog=new AlertDialog.Builder(getActivity()).create();
                View view1=LayoutInflater.from(getActivity()).inflate(R.layout.link_pop_up_lay,null);
                dialog.setView(view1);
                VideoEnabledWebView webview=view1.findViewById(R.id.WebView);
                RelativeLayout BrowserLayout=view1.findViewById(R.id.BrowserLayout);
                ProgressBar progressBar=view1.findViewById(R.id.progress);

                ViewGroup videoLayout = (ViewGroup) view1.findViewById(R.id.videoLayout);
                View loadingView =  view1.findViewById(R.id.videoLoading);
                webChromeClient= new VideoEnabledWebChromeClient(BrowserLayout, videoLayout,
                        loadingView, webview, progressBar);
                webview.setWebChromeClient(webChromeClient);
                webview.getSettings().setJavaScriptEnabled(true);
                webview.getSettings().setLoadWithOverviewMode(true);
                webview.getSettings().setPluginState(WebSettings.PluginState.ON);
                webview.getSettings().setDisplayZoomControls(false);
                webview.getSettings().setBuiltInZoomControls(true);
                webview.getSettings().setSupportMultipleWindows(false);
                webview.getSettings().setEnableSmoothTransition(true);
                webview.loadUrl("https://freedownloaderallvideodownloader2020.blogspot.com/2020/10/privacy-policy.html");
                dialog.show();
            }
        });
        Update=view.findViewById(R.id.Update);
        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(myAppLinkToMarket);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), "unable to find market app", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}