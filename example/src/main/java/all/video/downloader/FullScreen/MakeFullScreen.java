package all.video.downloader.FullScreen;

import android.app.Activity;
import android.os.Build;
import android.view.WindowManager;

public class MakeFullScreen {
    Activity context;
    public MakeFullScreen(Activity context){
        this.context=context;
    }

    public void makeFull(){
        context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        WindowManager.LayoutParams attributes=context.getWindow().getAttributes();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            attributes.layoutInDisplayCutoutMode= WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;

        }
    }
}
