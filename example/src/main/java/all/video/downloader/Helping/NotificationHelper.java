package all.video.downloader.Helping;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import androidx.core.app.NotificationCompat;

import all.video.downloader.MainActivity.MainActivity;
import all.video.downloader.R;

public class NotificationHelper {
    private Context mContext;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    public NotificationHelper(Context context) {
        mContext = context;
    }
    public void createNotification(String title, String message,int notify)
    {
        Intent resultIntent = new Intent(mContext , MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Intent intentAction = new Intent(mContext, ActionReceiver.class);
        intentAction.putExtra("action","action1");
        PendingIntent  pIntentlogin = PendingIntent.getBroadcast(mContext,1,intentAction,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder = new NotificationCompat.Builder(mContext);
        mBuilder.setOnlyAlertOnce(true);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(title)
                .setContentText(message)
                .setSubText("Getting Info")
                .setAutoCancel(false)
                .setContentIntent(pIntentlogin)
                .addAction(R.drawable.ic_cancel_black_24dp, "Cancel", pIntentlogin);
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(false);
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mBuilder.setProgress(100, 0, false);
        mNotificationManager.notify(notify /* Request Code */, mBuilder.build());
    }
    public void createNotificationNew(String title, String message,int notify)
    {
        try{
            /**Creates an explicit intent for an Activity in your app**/
            Intent resultIntent = new Intent(mContext , MainActivity.class);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext,
                    0 /* Request code */, resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            Intent intentAction = new Intent(mContext, ActionReceiver.class);
            intentAction.putExtra("action","action1");
            intentAction.putExtra("requestid",notify);
            PendingIntent  pIntentlogin = PendingIntent.getBroadcast(mContext,1,intentAction,PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder=new NotificationCompat.Builder(mContext);
            mBuilder.setOnlyAlertOnce(true);
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
            mBuilder.setContentTitle(title)
                    .setContentText(message)
                    .setSubText("Getting Info")
                    .setAutoCancel(false)
                    .setContentIntent(resultPendingIntent);
            mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
            {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.enableVibration(false);
                assert mNotificationManager != null;
                mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }
            assert mNotificationManager != null;
            mBuilder.setProgress(100, 0, false);
            mNotificationManager.notify(notify /* Request Code */, mBuilder.build());
        }catch (Exception e){
            e.printStackTrace();
            Log.i("exception notigy ",""+e);
        }
    }
    public void setprogress(int progress,int notify){
        try {
            mBuilder.setProgress(100, progress, false);
            mNotificationManager.notify(notify,mBuilder.build());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void setprogressNew(int progress,int notify,String info,String contenttext){
        mBuilder.setProgress(100, progress, false);
        mBuilder.setContentText(contenttext);
        mBuilder.setSubText(info);
        mNotificationManager.notify(notify, mBuilder.build());
    }
    public void createNotification_end(String title, String message,int notify,Uri uri)
    {
        try {
            mBuilder.setContentTitle(title);
            mBuilder.setContentText(message);
            mNotificationManager.notify(notify /* Request Code */, mBuilder.build());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void createNotification_endNew(String title, String message, int notify, String subtext, Uri uri)
    {
        try {
            Intent intent  = new Intent(Intent.ACTION_VIEW);
            try {
                intent.setDataAndType(uri,"video/*");
            } catch (ActivityNotFoundException e) {
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Intent.createChooser(intent, "Open file");
            PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext,
                    0 /* Request code */, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentTitle(title);
            mBuilder.setContentText(message);
            mBuilder.setSubText(subtext);
            mBuilder.setContentIntent(resultPendingIntent);
            mNotificationManager.notify(notify /* Request Code */, mBuilder.build());
        }catch (Exception e){
        }
    }
}