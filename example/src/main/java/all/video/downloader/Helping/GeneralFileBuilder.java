package all.video.downloader.Helping;

import java.util.ArrayList;
import java.util.List;


public class GeneralFileBuilder extends FileBuilder {

    String url;
    private MyFile f;

    public GeneralFileBuilder(String url) {
        this.url = url;
        f = new MyFile();
    }

    @Override
    public MyFile getFile() {
        return f;
    }

    @Override
    public void process() {
        setFileName();
        setFileUrl();

    }


    public void setFileName() {
        String name = null;
        List mm = new ArrayList();
        for (String n : url.split("/")) {
            mm.add(n);

        }

        name = (String) mm.get(mm.size() - 1);
        if (
                name.endsWith(".jpg") || name.endsWith(".jpeg") || name.endsWith(".png")
                        || name.endsWith(".gif") || name.endsWith(".ai") || name.endsWith(".eps")
                        || name.endsWith(".pdf") || name.endsWith(".xps") || name.endsWith(".doc")
                        || name.endsWith(".docx") || name.endsWith(".xls") || name.endsWith(".txt")
                        || name.endsWith(".cdr") || name.endsWith(".xml") || name.endsWith(".htm")
                        || name.endsWith(".html") || name.endsWith(".rtf") || name.endsWith(".ppt")
                        || name.endsWith(".pptx") || name.endsWith(".tif") || name.endsWith(".bmp")
                        || name.endsWith(".xls") || name.endsWith(".xlsx") || name.endsWith(".xlsm")
                        || name.endsWith(".xlsb") || name.endsWith(".csv") || name.endsWith(".java") ||
                        name.endsWith(".3gp") ||
                        name.endsWith(".aac") || name.endsWith(".amr")
                        || name.endsWith(".mp3") ||
                        name.endsWith(".wav") || name.endsWith(".wma")
                        || name.endsWith(".webm") || name.endsWith(".m4p")
                        || name.endsWith(".aa")
                        || name.endsWith(".mkv") || name.endsWith(".mp4") || name.endsWith(".flv")
                        || name.endsWith(".3g2") || name.endsWith(".3gpp") || name.endsWith(".asf")
                        || name.endsWith(".dat") || name.endsWith(".divx") || name.endsWith(".dv")
                        || name.endsWith(".f4v") || name.endsWith(".m4v") || name.endsWith(".mod")
                        || name.endsWith(".mov") || name.endsWith(".mpe") || name.endsWith(".mpeg")
                        || name.endsWith(".mpeg4") || name.endsWith(".vob")) {
            f.setName(name);
        } else {
            f.setName(name + ".mp4");
        }
    }


    public void setFileUrl() {

        f.setUrl(url);


    }
}
