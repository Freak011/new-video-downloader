package all.video.downloader.Helping;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;

import all.video.downloader.R;

public class PathAddress {

    public static File pathofdownload(Context context){
        File yourAppDir = new File(Environment.getExternalStorageDirectory()
                + "/FreeVideoDownloader");
        if (!yourAppDir.exists()) {
            yourAppDir.mkdirs();
        }
        if(!yourAppDir.exists() && !yourAppDir.isDirectory())
        {
            if (yourAppDir.mkdirs())
            {
                Log.i("CreateDir","App dir created");
            }
            else
            {
                Log.w("CreateDir","Unable to create app dir!");
            }
        }
        else
        {
            Log.i("CreateDir","App dir already exists");
        }
        return yourAppDir;
    }
    public static File pathofwapp(Context context){
        File yourAppDir = new File(Environment.getExternalStorageDirectory()
                + "/WhatsApp/Media/.Statuses");
        if (!yourAppDir.exists()) {
            yourAppDir.mkdirs();
        }
        if(!yourAppDir.exists() && !yourAppDir.isDirectory())
        {
            if (yourAppDir.mkdirs())
            {
                Log.i("CreateDir","App dir created");
            }
            else
            {
                Log.w("CreateDir","Unable to create app dir!");
            }
        }
        else
        {
            Log.i("CreateDir","App dir already exists");
        }
        return yourAppDir;
    }
    public void cretedir(Context context){
        File yourAppDir = new File(String.valueOf(Environment.getExternalStoragePublicDirectory(context.getResources().getString(R.string.app_name))));
        if(!yourAppDir.exists() && !yourAppDir.isDirectory())
        {
            if (yourAppDir.mkdirs())
            {
                Log.i("CreateDir","App dir created");
            }
            else
            {
                Log.w("CreateDir","Unable to create app dir!");
            }
        }
        else
        {
            Log.i("CreateDir","App dir already exists");
        }
    }
}
