package all.video.downloader.Helping;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.Request;

public class ActionReceiver extends BroadcastReceiver {
    private Fetch fetch;
    Request request;

    @Override
    public void onReceive(Context context, Intent intent) {
        FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(context)
                .setDownloadConcurrentLimit(5)
                .build();
        fetch = Fetch.Impl.getInstance(fetchConfiguration);
        //Toast.makeText(context,"recieved",Toast.LENGTH_SHORT).show();

        String action=intent.getStringExtra("action");
       // String request_id="";
        int request_id= intent.getIntExtra("requestid",0);
        if(action.equals("action1")){

            performAction1(context, request_id);
        }
        else if(action.equals("action2")){
            performAction2();

        }
        //This is used to close the notification tray
        Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        context.sendBroadcast(it);
    }

    public void performAction1(Context context,int request_id){

        Toast.makeText(context, "Cancel is pressed", Toast.LENGTH_SHORT).show();
            fetch.cancel(request_id);



        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(request_id);
//        ((MainActivity)context).cancelMyfetch(request_id);

    }

    public void performAction2(){

    }

}