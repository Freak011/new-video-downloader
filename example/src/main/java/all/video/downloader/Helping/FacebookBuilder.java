package all.video.downloader.Helping;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FacebookBuilder extends FileBuilder {

    private String url;
    private MyFile f;

    public FacebookBuilder(String url) {
        this.url = url;
        f = new MyFile();
    }

    @Override
    public MyFile getFile() {
        return f;
    }

    public String cleanUrl() {
        String u = null;
        if (url.contains("www")) {
            u = url.replace("www", "m");
        } else
            u = url;

        return u;
    }

    public String getFileName() {
        String name = null;
        List mm = new ArrayList();
        for (String n : url.split("/")) {
            mm.add(n);

        }

        name = (String) mm.get(mm.size() - 1);
        return name;
    }

    @Override
    public void process() {
        try {
            String viURl = null;
            String viUR2 = null;
            String pageContent = Downloader.download(cleanUrl());
//        Document doc= Jsoup.parse(pageContent);
            Document doc = null;
            try {
                doc = Jsoup.connect(cleanUrl()).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Elements ele = doc.select("a");

            String filename = getFileName();
//        if(filename.length()>20){
//            char newname[] = new char[10];
//            filename.getChars(0,9,newname,0);
//            filename = newname.toString();
//        }
//        filename = filename.replace("-","_");
//        filename = filename.replace("&","_");
//        filename = filename.replace("*","_");
//        filename = filename.replace("%","_");
//        filename = filename.replace("(","_");
//        filename = filename.replace(")","_");
//        filename = filename.replace("[","_");
//        filename = filename.replace("]","_");
//        filename = filename.replace("@","_");
//        filename = filename.replace("_","");
            filename = filename + ".mp4";
            f.setName(filename);

//        f.setName("Fb.mp4");
            String pattern = "https%(.*)&s";
            String elementData;

            for (Element e : ele) {
                elementData = e.attr("href");

                Log.d("ELEMENT", elementData);
                if (elementData.startsWith("/video_redirect")) {
                    Pattern p = Pattern.compile(pattern);
                    Matcher m = p.matcher(elementData);
                    if (m.find()) {

                        try {
                            viURl = URLDecoder.decode(m.group().replace("&s", ""), "UTF-8");
                        } catch (UnsupportedEncodingException e1) {
                            e1.printStackTrace();
                        }
                        System.out.println();
                    }

                }
                if (viUR2==null) {
                    viUR2=viURl;
                    f.setUrl(viURl);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
