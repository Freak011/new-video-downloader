package all.video.downloader.Helping;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import all.video.downloader.R;


public class FileUtils {
    public static final boolean isDebug = true;
    private static final int BUFFER = 2048;
    private static FileComparator comparator = new FileComparator();
    private static int fileCount = 0;
    static final String TAG = "FileUtils";
    private static Activity activity;

    public FileUtils(Activity activity) {
        this.activity = activity;
    }

    /**
     * @param old    the file to be copied
     * @param newDir the directory to move the file to
     * @return
     */


    public Drawable getapkicon(String url) {

        Drawable icon;
        String filePath = url;
        try {
            PackageInfo packageInfo = activity.getPackageManager()
                    .getPackageArchiveInfo(filePath,
                            PackageManager.GET_ACTIVITIES);
            ApplicationInfo appInfo = packageInfo.applicationInfo;

            if (Build.VERSION.SDK_INT >= 5) {
                appInfo.sourceDir = filePath;
                appInfo.publicSourceDir = filePath;

                icon = appInfo.loadIcon(activity.getPackageManager());
                if (icon.getIntrinsicHeight() > 50
                        && icon.getIntrinsicWidth() > 50) {
                }

            } else {
                icon = activity.getResources().getDrawable(R.drawable.icon_video_play_big_p);

            }

            return icon;
        } catch (Exception e) {
            // TODO: handle exception
            return activity.getResources().getDrawable(R.drawable.icon_video_play_big_p);
        }
    }

    public static int copyToDirectory(String old, String newDir) {
        File old_file = new File(old);
        File temp_dir = new File(newDir);
        byte[] data = new byte[BUFFER];
        int read = 0;

        if (old_file.isFile() && temp_dir.isDirectory() && temp_dir.canWrite()) {
            String file_name = old
                    .substring(old.lastIndexOf("/"), old.length());
            File cp_file = new File(newDir + file_name);

            try {

                BufferedOutputStream o_stream = new BufferedOutputStream(
                        new FileOutputStream(cp_file));
                BufferedInputStream i_stream = new BufferedInputStream(
                        new FileInputStream(old_file));

                while ((read = i_stream.read(data, 0, BUFFER)) != -1)
                    o_stream.write(data, 0, read);

                o_stream.flush();
                i_stream.close();
                o_stream.close();

            } catch (FileNotFoundException e) {
                Log.e("FileNotFoundException", e.getMessage());
                return -1;

            } catch (IOException e) {
                Log.e("IOException", e.getMessage());
                return -1;

            }

        } else if (old_file.isDirectory() && temp_dir.isDirectory()
                && temp_dir.canWrite()) {
            String files[] = old_file.list();
            String dir = newDir
                    + old.substring(old.lastIndexOf("/"), old.length());
            int len = files.length;

            if (!new File(dir).mkdir())
                return -1;

            for (int i = 0; i < len; i++)
                copyToDirectory(old + "/" + files[i], dir);

        } else if (!temp_dir.canWrite())
            return -1;

        return 0;
    }

    /**
     * @param zipName
     * @param toDir
     * @param fromDir
     */
    public void extractZipFilesFromDir(String zipName, String toDir,
                                       String fromDir) {
        if (!(toDir.charAt(toDir.length() - 1) == '/'))
            toDir += "/";
        if (!(fromDir.charAt(fromDir.length() - 1) == '/'))
            fromDir += "/";

        String org_path = fromDir + zipName;

        extractZipFiles(org_path, toDir);
    }

    /**
     * @param zip_file
     * @param directory
     */

    public static void extractZipFiles(String zip_file, String directory) {
        byte[] data = new byte[BUFFER];
        String name, path, zipDir;
        ZipEntry entry;
        ZipInputStream zipstream;

        if (!(directory.charAt(directory.length() - 1) == '/'))
            directory += "/";

        if (zip_file.contains("/")) {
            path = zip_file;
            name = path.substring(path.lastIndexOf("/") + 1, path.length() - 4);
            zipDir = directory + name + "/";

        } else {
            path = directory + zip_file;
            name = path.substring(path.lastIndexOf("/") + 1, path.length() - 4);
            zipDir = directory + name + "/";
        }

        new File(zipDir).mkdir();

        try {
            zipstream = new ZipInputStream(new FileInputStream(path));

            while ((entry = zipstream.getNextEntry()) != null) {
                String buildDir = zipDir;
                String[] dirs = entry.getName().split("/");

                if (dirs != null && dirs.length > 0) {
                    for (int i = 0; i < dirs.length - 1; i++) {
                        buildDir += dirs[i] + "/";
                        new File(buildDir).mkdir();
                    }
                }

                int read = 0;
                FileOutputStream out = new FileOutputStream(zipDir
                        + entry.getName());
                while ((read = zipstream.read(data, 0, BUFFER)) != -1)
                    out.write(data, 0, read);

                zipstream.closeEntry();
                out.close();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param path
     */

    public static void createZipFile(String path) {

        try {

            File dir = new File(path);

            File parent = dir.getParentFile();
            String filepath = parent.getAbsolutePath();
            String[] list = dir.list();
            String name = path.substring(path.lastIndexOf("/"), path.length());
            String _path;

            if (!dir.canRead() || !dir.canWrite())
                return;

            int len = list.length;

            if (path.charAt(path.length() - 1) != '/')
                _path = path + "/";
            else
                _path = path;

            try {
                ZipOutputStream zip_out = new ZipOutputStream(
                        new BufferedOutputStream(new FileOutputStream(filepath
                                + name + ".zip"), BUFFER));

                for (int i = 0; i < len; i++)
                    zip_folder(new File(_path + list[i]), zip_out);

                zip_out.close();

            } catch (FileNotFoundException e) {
                Log.e("File not found", e.getMessage());

            } catch (IOException e) {
                Log.e("IOException", e.getMessage());
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * @param filePath
     * @param newName
     * @return
     */
    public static int renameTarget(String filePath, String newName) {
        File src = new File(filePath);
        String ext = "";
        File dest;

        if (src.isFile())
            /* get file extension */
            ext = filePath.substring(filePath.lastIndexOf("."),
                    filePath.length());

        if (newName.length() < 1)
            return -1;

        String temp = filePath.substring(0, filePath.lastIndexOf("/"));

        dest = new File(temp + "/" + newName);
        if (src.renameTo(dest))
            return 0;
        else
            return -1;
    }

    /**
     * @param path
     * @param name
     * @return
     */
    public static int createDir(String path, String name) {
        int len = path.length();

        if (len < 1 || len < 1)
            return -1;

        if (path.charAt(len - 1) != '/')
            path += "/";

        if (new File(path + name).mkdir())
            return 0;

        return -1;
    }

    public static int createFile(String path, String name) {

        int len = path.length();

        if (len < 1 || len < 1)
            return -1;

        if (path.charAt(len - 1) != '/')
            path += "/";

        try {
            if (new File(path + name).createNewFile())
                return 0;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }

        return -1;

    }

    /**
     * The full path name of the file to delete.
     *
     * @param path name
     * @return
     */
    public static int deleteTarget(String path) {
        File target = new File(path);

        if (target.exists() && target.isFile() && target.canWrite()) {
            target.delete();
            return 0;
        } else if (target.exists() && target.isDirectory() && target.canRead()) {
            String[] file_list = target.list();

            if (file_list != null && file_list.length == 0) {
                target.delete();
                return 0;

            } else if (file_list != null && file_list.length > 0) {

                for (int i = 0; i < file_list.length; i++) {
                    File temp_f = new File(target.getAbsolutePath() + "/"
                            + file_list[i]);

                    if (temp_f.isDirectory())
                        deleteTarget(temp_f.getAbsolutePath());
                    else if (temp_f.isFile())
                        temp_f.delete();
                }
            }
            if (target.exists())
                if (target.delete())
                    return 0;
        }
        return -1;
    }

    /**
     * converts integer from wifi manager to an IP address.
     *
     * @param des
     * @return
     */
    public static String integerToIPAddress(int ip) {
        String ascii_address = "";
        int[] num = new int[4];

        num[0] = (ip & 0xff000000) >> 24;
        num[1] = (ip & 0x00ff0000) >> 16;
        num[2] = (ip & 0x0000ff00) >> 8;
        num[3] = ip & 0x000000ff;

        ascii_address = num[0] + "." + num[1] + "." + num[2] + "." + num[3];

        return ascii_address;
    }

    /**
     * @param dir
     * @param pathName
     * @return
     */
    public static ArrayList<String> searchInDirectory(String dir,
                                                      String fileName) {
        ArrayList<String> names = new ArrayList<String>();
        search_file(dir, fileName, names);

        return names;
    }

    public static ArrayList<String> Ultrasearch(String dir, String fileName) {
        ArrayList<String> names = new ArrayList<String>();
        Ultrasearch_file(dir, fileName, names);

        return names;
    }

    /**
     * @param path
     * @return
     */

	/*
	 * 
	 * @param file
	 * 
	 * @param zout
	 * 
	 * @throws IOException
	 */
    private static void zip_folder(File file, ZipOutputStream zout)
            throws ZipException, IOException {
        byte[] data = new byte[BUFFER];
        int read;

        if (file.isFile()) {
            ZipEntry entry = new ZipEntry(file.getName());
            zout.putNextEntry(entry);
            BufferedInputStream instream = new BufferedInputStream(
                    new FileInputStream(file));

            while ((read = instream.read(data, 0, BUFFER)) != -1)
                zout.write(data, 0, read);

            zout.closeEntry();
            instream.close();

        } else if (file.isDirectory()) {
            String[] list = file.list();
            int len = list.length;

            for (int i = 0; i < len; i++)
                zip_folder(new File(file.getPath() + "/" + list[i]), zout);
        }
    }

	/*
	 * 
	 * @param path
	 */

    // Inspired by org.apache.commons.io.FileUtils.isSymlink()
    private static boolean isSymlink(File file) throws IOException {
        File fileInCanonicalDir = null;
        if (file.getParent() == null) {
            fileInCanonicalDir = file;
        } else {
            File canonicalDir = file.getParentFile().getCanonicalFile();
            fileInCanonicalDir = new File(canonicalDir, file.getName());
        }
        return !fileInCanonicalDir.getCanonicalFile().equals(
                fileInCanonicalDir.getAbsoluteFile());
    }

    /*
     * (non-JavaDoc) I dont like this method, it needs to be rewritten. Its
     * hacky in that if you are searching in the root dir (/) then it is not
     * going to be treated as a recursive method so the user dosen't have to sit
     * forever and wait.
     *
     * I will rewrite this ugly method.
     *
     * @param dir directory to search in
     *
     * @param fileName filename that is being searched for
     *
     * @param n ArrayList to populate results
     */
    private static void search_file(String dir, String fileName,
                                    ArrayList<String> n) {
        File root_dir = new File(dir);
        String[] list = root_dir.list();

        if (list != null && root_dir.canRead()) {
            int len = list.length;

            for (int i = 0; i < len; i++) {
                File check = new File(dir + "/" + list[i]);
                String name = check.getName();

                if (check.isFile()
                        && name.toLowerCase().contains(fileName.toLowerCase())) {
                    n.add(check.getPath());
                } else if (check.isDirectory()) {
                    if (name.toLowerCase().contains(fileName.toLowerCase())) {
                        n.add(check.getPath());

                    } else if (check.canRead() && !dir.equals("/"))
                        search_file(check.getAbsolutePath(), fileName, n);
                }
            }
        }
    }

    public static int getFileCount(File file) {
        fileCount = 0;
        calculateFileCount(file);
        return fileCount;
    }

    private static void calculateFileCount(File file) {
        if (!file.isDirectory()) {
            fileCount++;
            return;
        }
        if (file.list() == null) {
            return;
        }
        for (String fileName : file.list()) {
            File f = new File(file.getAbsolutePath() + File.separator
                    + fileName);
            calculateFileCount(f);
        }
    }

    public static ArrayList<File> getDuplicates(File file) {
        ArrayList<File> dupfiles = new ArrayList<File>();
        HashMap<Long, ArrayList<String>> lists = new HashMap<Long, ArrayList<String>>();
        Find(file.getAbsolutePath(), lists);
        for (ArrayList<String> list : lists.values()) {

            if (list.size() > 1) {

                for (String filepath : list) {

                    dupfiles.add(new File(filepath));

                }

            }
        }
        return dupfiles;

    }

    public static void Find(String dir, HashMap<Long, ArrayList<String>> lists) {
        File root_dir = new File(dir);
        String[] filelist = root_dir.list();

        if (filelist != null && root_dir.canRead()) {
            int len = filelist.length;

            for (int i = 0; i < len; i++) {
                File check = new File(dir + "/" + filelist[i]);

                if (check.isFile()) {
                    long length = check.length();
                    ArrayList<String> list = lists.get(length);
                    if (list == null) {
                        list = new ArrayList<String>();
                        lists.put(length, list);
                    }
                    list.add(check.getAbsolutePath());
                } else if (check.isDirectory()) {
                    Find(check.getAbsolutePath(), lists);
                }
            }
        }
    }

    public boolean CompareSize(File file1, File file2) {
        if (file1.length() == file2.length())
            return true;
        else
            return false;

    }

    private static void Ultrasearch_file(String dir, String fileName,
                                         ArrayList<String> n) {
        File root_dir = new File(dir);
        String[] list = root_dir.list();

        if (list != null && root_dir.canRead()) {
            int len = list.length;

            for (int i = 0; i < len; i++) {
                File check = new File(dir + "/" + list[i]);
                String name = check.getName();

                if (check.isFile()
                        && name.toLowerCase().contains(fileName.toLowerCase())) {
                    n.add(check.getPath());
                } else if (check.isDirectory()
                        && !check.getName().startsWith(".")) {
                    if (name.toLowerCase().contains(fileName.toLowerCase()))
                        n.add(check.getPath());

                }
            }
        }
    }

    /**
     * Constructs a file from a path and file name.
     *
     * @param curdir
     * @param file
     * @return
     */

    public static File getFile(String curdir, String file) {
        String separator = "/";
        if (curdir.endsWith("/")) {
            separator = "";
        }
        File clickedFile = new File(curdir + separator + file);
        return clickedFile;
    }

    public static String getFastHash(String filepath) {
        MessageDigest md;
        String hash;
        File file = new File(filepath);
        try {
            try {
                md = MessageDigest.getInstance("MD5");

            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(
                        "cannot initialize MD5 hash function", e);
            }
            FileInputStream fin = new FileInputStream(file);
            if (file.length() > 1048576L) {
                byte data[] = new byte[(int) file.length() / 100];
                fin.read(data);
                fin.close();
                hash = new BigInteger(1, md.digest(data)).toString(16);
            } else if (file.length() > 1024L) {
                byte data[] = new byte[(int) file.length() / 10];
                fin.read(data);
                fin.close();
                hash = new BigInteger(1, md.digest(data)).toString(16);
            } else {
                byte data[] = new byte[(int) file.length()];
                fin.read(data);
                fin.close();
                hash = new BigInteger(1, md.digest(data)).toString(16);
            }
        } catch (IOException e) {
            // TODO: handle exception
            throw new RuntimeException("cannot read file "
                    + file.getAbsolutePath(), e);

        }

        return hash;
    }

    private static class FileComparator implements Comparator<File> {
        private Collator c = Collator.getInstance();

        public int compare(File f1, File f2) {
            if (f1 == f2)
                return 0;

            if (f1.isDirectory() && f2.isFile())
                return -1;
            if (f1.isFile() && f2.isDirectory())
                return 1;

            return c.compare(f1.getName(), f2.getName());
        }
    }
}
