package all.video.downloader.Helping;

import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class FileBuilder {

    public abstract MyFile getFile();
    public abstract void process();

    
    protected String matchGroup1(String pattern, String input, int grp_id) {
        Pattern pat = Pattern.compile(pattern);
        Matcher mat = pat.matcher(input);
        boolean foundMatch = mat.find();
        if (foundMatch) {
            return mat.group(grp_id);
        }
        else {
            Log.e("MatchGroup", "failed to find pattern \"" + pattern + "\" inside of \"" + input + "\"");
                new Exception("failed to find pattern \""+pattern+"\"").printStackTrace();
            return "";
        }

    }







}
