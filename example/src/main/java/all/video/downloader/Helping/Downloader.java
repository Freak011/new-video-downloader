package all.video.downloader.Helping;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;


public class Downloader {

    private static final String USER_AGENT = "Mozilla/5.0";


    public static String download(String siteUrl, String language) {
        String ret = "";
        try {
            URL url = new URL(siteUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestProperty("Accept-Language", language);
            ret = dl(con);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    private static String dl(HttpURLConnection con) throws IOException {
        StringBuilder response = new StringBuilder();

        try {

            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;

            while((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

        }
        catch(UnknownHostException uhe) {//thrown when there's no internet connection
            uhe.printStackTrace();
            //Toast.makeText(getActivity(), uhe.getMessage(), Toast.LENGTH_LONG).show();
        }

        return response.toString();
    }


    public static String download(String siteUrl) {
        String ret = "";

        try {
            URL url = new URL(siteUrl);

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            ret = dl(con);
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return ret;
    }
}
