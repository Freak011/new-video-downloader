package all.video.downloader.WAStatus.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import all.video.downloader.WAStatus.Fragments.ImageFragment;
import all.video.downloader.WAStatus.Fragments.VideoFragment;


public class PageAdapter extends FragmentPagerAdapter {

    private int totalTabs;

    public PageAdapter(@NonNull FragmentManager fm, int totalTabs) {
        super(fm);
        this.totalTabs = totalTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        if (position == 1) {
            return new VideoFragment();
        }
        return new ImageFragment();

    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
