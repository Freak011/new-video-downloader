package all.video.downloader.WAStatus;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.tabs.TabLayout;

import java.io.File;
import java.util.Objects;

import all.video.downloader.AdmobHelper.AdmobHelper;
import all.video.downloader.Helping.TinyDB;
import all.video.downloader.R;
import all.video.downloader.WAStatus.Adapter.PageAdapter;
import all.video.downloader.WAStatus.Utils.Common;

public class MainActivityWA extends AppCompatActivity {

    private ViewPager viewPager;
    TinyDB tinyDB;
    //    public InterstitialAd mInterstitialAd;
    RelativeLayout TuImage;
    ImageView Back, TuWhatsApp;
    private static final int REQUEST_PERMISSIONS = 1234;
    private static final String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_wa);
//        loadGoogleAds();
        MaterialToolbar toolbar = findViewById(R.id.toolbarMainActivity);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        setSupportActionBar(toolbar);
        tinyDB = new TinyDB(MainActivityWA.this);
        TuImage = findViewById(R.id.TuImage);
        Back = findViewById(R.id.TuBack);
        TuWhatsApp = findViewById(R.id.TuWhatsApp);
        AdmobHelper.getInstance().loadInterstitial(MainActivityWA.this,"");
        TuWhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    tinyDB.putString("ok", "ok");
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.whatsapp");
                    startActivity(launchIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (TextUtils.equals(tinyDB.getString("ok"), "")) {
            TuImage.setVisibility(View.VISIBLE);
        }
        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TuImage.setVisibility(View.GONE);
                tinyDB.putString("ok", "ok");
            }
        });
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.images)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.videos)));
        PagerAdapter adapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSIONS && grantResults.length > 0) {
            if (arePermissionDenied()) {
                ((ActivityManager) Objects.requireNonNull(this.getSystemService(ACTIVITY_SERVICE))).clearApplicationUserData();
                recreate();
            } else {
                finish();
                startActivity(new Intent(MainActivityWA.this, MainActivityWA.class));
            }
        }
    }

    private boolean arePermissionDenied() {

        for (String permissions : PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), permissions) != PackageManager.PERMISSION_GRANTED) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && arePermissionDenied()) {
            requestPermissions(PERMISSIONS, REQUEST_PERMISSIONS);
            return;
        }
        File newf = new File(Environment.getExternalStorageDirectory()
                + "/FreeVideoDownloader");
        Common.APP_DIR = newf.getAbsolutePath();

    }

    @Override
    public void onBackPressed() {
        if (TuImage.getVisibility() == View.VISIBLE) {
            TuImage.setVisibility(View.GONE);
            tinyDB.putString("ok", "ok");
        } else {

            if (AdmobHelper.getInstance().mInterstitial != null){
                AdmobHelper.getInstance().showAdAfterDialogFor2Seconds(MainActivityWA.this);
                AdmobHelper.getInstance().mInterstitial.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        finish();
                    }
                });
            }else {
                finish();
            }
//                if (interstitialAd != null)
//                if (interstitialAd.isAdLoaded())
//                    interstitialAd.show();
//                else
//                    finish();
        }
    }

}
