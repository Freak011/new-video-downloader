package all.video.downloader.View;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.MimeTypeMap;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import all.video.downloader.Helping.Constants;
import all.video.downloader.MainActivity.MainActivity;
import all.video.downloader.R;
import all.video.downloader.database.DbItem;
import all.video.downloader.database.HistoryDatabase;

public class WebViewClient extends android.webkit.WebViewClient {
    private EditText TEXT;
    private Map<String, Boolean> loadedUrls = new HashMap<>();
    Activity act;
    VideoEnabledWebView mainView;
    boolean priv=false;
    String old_down_url = "";
    public String prev_url_fb="";
    public String prev_url_fb_n="";

    public String peertube="";
    WebView view;
    @SuppressLint("JavascriptInterface")
    public WebViewClient(EditText textView, Activity ac, VideoEnabledWebView view){
        super();
        TEXT = textView;
        act = ac;
        mainView = view;
//        mainView.addJavascriptInterface(this, "FBDownloader");
        mainView.addJavascriptInterface(this, "browser");
        // mainView.addJavascriptInterface(this, "FBDownloader22");


    }

    public void getpat(String toSearch){

        for (int i = 0; i< MainActivity.mainpattarray.size(); i++){
            if (toSearch.contains(MainActivity.mainpattarray.get(i).getName()) || MainActivity.mainpattarray.get(i).getName().contains(toSearch)){
                MainActivity.pattern_mat_string=MainActivity.mainpattarray.get(i).getPattern();
                MainActivity.site_mat_string=MainActivity.mainpattarray.get(i).getName();
                i=MainActivity.mainpattarray.size()+1;
            }
        }
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        view.loadUrl("javascript:(function prepareVideo() { "
                + "var el = document.querySelectorAll('div[data-sigil]');"
                + "for(var i=0;i<el.length; i++)"
                + "{"
                + "var sigil = el[i].dataset.sigil;"
                + "if(sigil.indexOf('inlineVideo') > -1){"
                + "delete el[i].dataset.sigil;"
                + "console.log(i);"
                + "var jsonData = JSON.parse(el[i].dataset.store);"
                + "el[i].setAttribute('onClick', 'FBDownloader.processVideo(\"'+jsonData['src']+'\",\"'+jsonData['videoID']+'\");');"
                + "}" + "}" + "})()");
        view.loadUrl("javascript:( window.onload=prepareVideo;"
                + ")()");
        Constants.isURL=1;
        Constants.url=url;
        if (url.contains("ted")||url.contains("https://www.liveleak.com/view")){

        }else{
            ((MainActivity) act).changeColorFloatBasic(act,"");
        }
        Log.i("url main pattern",MainActivity.pattern_mat_string);
        String str;
        if (url.contains("facebook")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {

                view.getSettings().setMediaPlaybackRequiresUserGesture(false);
            }

        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                view.getSettings().setMediaPlaybackRequiresUserGesture(false);
            }
        }


        if (url.contains("instagram")){
//                ((MainActivity) act).changeColorFloat(act,url,"normal");
            MainActivity.is_insta_on=false;
        }else{
            MainActivity.is_insta_on=false;
        }


        try{
            if (view.getUrl().contains("https://")) {
                getpat(view.getUrl());
                str = view.getUrl().toString().replace("https://", "<font color='#228B22'>https://</font>");
                TEXT.setText(Html.fromHtml(str), TextView.BufferType.SPANNABLE);
                TEXT.clearFocus();

            } else {
                getpat(view.getUrl());
                if (!view.getUrl().contains("file")) {
                    TEXT.setText(view.getUrl());
                }
            }
        }
        catch(Exception e){}
    }

    @Override
    public void onPageFinished(WebView view,String url) {


        //old
        view.loadUrl("javascript:(function prepareVideo() { "
                + "var el = document.querySelectorAll('div[data-sigil]');"
                + "for(var i=0;i<el.length; i++)"
                + "{"
                + "var sigil = el[i].dataset.sigil;"
                + "if(sigil.indexOf('inlineVideo') > -1){"
                + "delete el[i].dataset.sigil;"
                + "console.log(i);"
                + "var jsonData = JSON.parse(el[i].dataset.store);"
                + "el[i].setAttribute('onClick', 'FBDownloader.processVideo(\"'+jsonData['src']+'\",\"'+jsonData['videoID']+'\");');"
                + "}" + "}" + "})()");
        view.loadUrl("javascript:( window.onload=prepareVideo;"
                + ")()");


        //////
        String str;
//            if (mainView.isPrivate()) {
//                mainView.clearCache(true);
//                WebStorage storage = WebStorage.getInstance();
//                storage.deleteAllData();
//            } else {
        DbItem dbItem = new DbItem(url, view.getTitle());
        HistoryDatabase db = new HistoryDatabase(act);
        db.addItem(dbItem);
        mainView.clearCache(true);
//            }

        try {
            if(!TEXT.isFocused()) {
                if (view.getUrl().contains("https://")) {
                    str = view.getUrl().toString().replace("https://", "<font color='#228B22'>https://</font>");
                    TEXT.setText(Html.fromHtml(str), TextView.BufferType.SPANNABLE);
                    TEXT.clearFocus();
                } else {
                    if (!view.getUrl().contains("file")) {
                        if (view.getUrl().equalsIgnoreCase("about:blank")) {
                            TEXT.setText("");
                            TEXT.setHint("Search Here");
                        }
                    }else {
                        if (view.getUrl().equalsIgnoreCase("about:blank")) {
                            TEXT.setHint("Search Here");
                        }else {
                            //  TEXT.setText(view.getUrl());
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        view.clearCache(true);

    }
    @JavascriptInterface
    public void processVideo(final String vidData, final String vidID) {
        Log.i("url", "processVideo " + vidData);
        try {

            if (TextUtils.isEmpty(vidData)) {
                Toast.makeText(act, "Invalid/Empty URL", Toast.LENGTH_SHORT).show();
            } else {

                if (!vidData.isEmpty()) {
                    Log.i("urlfb", "processVideo " + vidData);


                    ((MainActivity) act).changeColorFloat(act,vidData,"normal");
                    //   view.loadUrl("https://www.facebook.com/video/embed?video_id="+vidID);
                    //Toast.makeText(act, ""+vidData, Toast.LENGTH_SHORT).show();

                }
            }
        }catch (Exception e){
            e.printStackTrace();}

    }




    ////new
//    @JavascriptInterface
//    public void processVideo(final String vidData, final String vidID)
//    {
//        try
//        {
//            String mBaseFolderPath = android.os.Environment
//                    .getExternalStorageDirectory()
//                    + File.separator
//                    + "FacebookVideos" + File.separator;
//            if (!new File(mBaseFolderPath).exists())
//            {
//                new File(mBaseFolderPath).mkdir();
//            }
//            String mFilePath = "file://" + mBaseFolderPath + "/" + vidID + ".mp4";
//
//            ((VpnMainActivity) act).showBottomSheetDialog(mFilePath,act,mFilePath);
////            Uri downloadUri = Uri.parse(vidData);
////            DownloadManager.Request req = new DownloadManager.Request(downloadUri);
////            req.setDestinationUri(Uri.parse(mFilePath));
////            req.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
////            DownloadManager dm = (DownloadManager) getSystemService(getApplicationContext().DOWNLOAD_SERVICE);
////            dm.enqueue(req);
//            Toast.makeText(act, "Download Started" +mFilePath, Toast.LENGTH_LONG).show();
//        }
//        catch (Exception e)
//        {
//            Toast.makeText(act, "Download Failed: " + e.toString(), Toast.LENGTH_LONG).show();
//        }
//    }

    @Override
    public void onLoadResource(WebView view, String url) {
        super.onLoadResource(view, url);

        Log.i("allurl",url);

      /*  view.loadUrl("javascript:function clickOnVideo(link, classValueName){" +
                "browser.getVideoData(link);" +
                "var element = document.getElementById(\"mInlineVideoPlayer\");" +
                "element.muted = true;" +
                "var parent = element.parentNode; " +
                "parent.removeChild(element);" +
                "parent.setAttribute('class', classValueName);}" +
                "function getVideoLink(){" +
                "try{var items = document.getElementsByTagName(\"div\");" +
                "for(i = 0; i < items.length; i++){" +
                "if(items[i].getAttribute(\"data-sigil\") == \"inlineVideo\"){" +
                "var classValueName = items[i].getAttribute(\"class\");" +
                "var jsonString = items[i].getAttribute(\"data-store\");" +
                "var obj = JSON && JSON.parse(jsonString) || $.parseJSON(jsonString);" +
                "var videoLink = obj.src;" +
                "var videoName = obj.videoID;" +
                "items[i].setAttribute('onclick', \"clickOnVideo('\"+videoLink+\"')\");}}" +
                "var links = document.getElementsByTagName(\"a\");" +
                "for(i = 0; i < links.length; i++){" +
                "if(links[ i ].hasAttribute(\"data-store\")){" +
                "var jsonString = links[i].getAttribute(\"data-store\");" +
                "var obj = JSON && JSON.parse(jsonString) || $.parseJSON(jsonString);" +
                "var videoName = obj.videoID;" +
                "var videoLink = links[i].getAttribute(\"href\");" +
                "var res = videoLink.split(\"src=\");" +
                "var myLink = res[1];" +
                "links[i].setAttribute('onclick', \"browser.processVideo('\"+myLink+\"')\");" +
                "while (links[i].firstChild){" +
                "links[i].parentNode.insertBefore(links[i].firstChild," +
                "links[i]);}" +
                "links[i].parentNode.removeChild(links[i]);}}}catch(e){}}" +
                "getVideoLink();");*/
        view.loadUrl("javascript:(function prepareVideo() { "
                + "var el = document.querySelectorAll('div[data-sigil]');"
                + "for(var i=0;i<el.length; i++)"
                + "{"
                + "var sigil = el[i].dataset.sigil;"
                + "if(sigil.indexOf('inlineVideo') > -1){"
                + "delete el[i].dataset.sigil;"
                + "console.log(i);"
                + "var jsonData = JSON.parse(el[i].dataset.store);"
                + "el[i].setAttribute('onClick', 'browser.processVideo(\"'+jsonData['src']+'\",\"'+jsonData['videoID']+'\");');"
                + "}" + "}" + "})()");
        view.loadUrl("javascript:( window.onload=prepareVideo;"
                + ")()");


        String str=MainActivity.pattern_mat_string;


        Pattern pattern = Pattern.compile(str);
        Matcher matcher = pattern.matcher(url);

        if ((matcher.find() || url.contains(pattern.toString())) && !str.isEmpty() ) {

            Log.i("url load match pattern ", url);



//                if (url.contains("56.com") && url.contains("shtml")){
//
//
//                }else{
//                    return;
//                }


            if (url.contains("beeg")) {
                if (url.contains("beeg.com/api/v6/")) {
                    String p = url.substring(url.lastIndexOf("/") + 1);
                    url = "https://beeg.com/" + p;

                }else{
                    return;
                }
            }


            if (url.contains("imdb")) {
                if (url.contains("imdb.com/vi")) {

                    String[] parts = url.split("[/?]");

                    for (int i = 0; i < parts.length; i++) {

                        if (parts[i].startsWith("vi")) {
                            Log.i("url load match pattern ", url);

                            String video_url = "http://www.imdb.com/video/imdb/" + parts[i];

                            i = parts.length;
                            url = video_url;

                        }
                    }
                }else{
                    return;
                }
            }

            if (url.contains("ted")){
                if(url.contains("json")){
                    Log.i("url match pattern ted", url);
                    url=url.substring(0,url.lastIndexOf("/"));
                    Log.i("url match pattern ted", url);
                }
            }

            if (url.contains("syndication") || url.contains("videa.hu/videaplayer") || url.contains("?rhttp")|| url.contains("?http") ||
                    url.contains("ref=http") ||url.contains("referer=http")) {

            }else {

                //  url=url.substring(url.lastIndexOf("/",0));
                if (url.contains("assets") ||url.contains("internal") || url.contains("language")) {}else{
                    //getdownloadlinkextractor
                    if (url.contains("vidivodo") ||url.contains("liveleak") || url.contains("56.com") || url.contains("anyporn.com") || url.contains("onlyindianporn") || url.contains("anysex.com") ||
                            url.contains("hlebo.mobi/mov")) {
                        Log.i("url match pattern ent ", url);
                        ((MainActivity) act).changeColorFloat(act,url,"volley");
                        //  ((MainActivity) act).getdownloadlinkextractor(url, act);
                        //  ((MainActivity) act).getdownloadlink_dl(url, act);
                    } else {
                        Log.i("url match pattern for ", url);
                        ((MainActivity) act).changeColorFloat(act,url,"volley");
                        //                     ((MainActivity) act).getdownloadlink_dl(url, act);
                    }
                }
            }


        }


        if (url.contains("www.dailymotion.com/player/metadata/video/") || url.contains("www.dailymotion.com/embed/video")) {

            String[] parts = url.split("[/?]");

            for (int i = 0; i < parts.length; i++) {

                if (parts[i].equals("video")) {
                    Log.i("url load match pattern ", url);
                    //Toast.makeText(getApplicationContext(),""+parts[i+1],Toast.LENGTH_SHORT).show();

                    String float_str = parts[i + 1];

                    String video_url = "http://www.dailymotion.com/video/" + parts[i + 1];
                    ((MainActivity) act).changeColorFloat(act,video_url,"volley");
                    i = parts.length;

                }
            }

        }

        ///////////liveleak
        if (url.contains("https://instagram.fisb9-1.fna.fbcdn.net")||url.contains("https://instagram.fisb9-2.fna.fbcdn.net")
                ||url.contains("https://instagram.fisb8-1.fna.fbcdn.net")||url.contains("https://instagram.fisb8-2.fna.fbcdn.net")
                ||url.contains("https://instagram.fisb7-1.fna.fbcdn.net")||url.contains("https://instagram.fisb7-2.fna.fbcdn.net")
                ||url.contains("https://instagram.fisb6-1.fna.fbcdn.net")||url.contains("https://instagram.fisb6-2.fna.fbcdn.net")
                ||url.contains("https://instagram.fisb5-1.fna.fbcdn.net")||url.contains("https://instagram.fisb5-2.fna.fbcdn.net")
                ||url.contains("https://instagram.fisb4-1.fna.fbcdn.net")||url.contains("https://instagram.fisb4-2.fna.fbcdn.net")
                ||url.contains("https://instagram.fisb3-1.fna.fbcdn.net")||url.contains("https://instagram.fisb3-2.fna.fbcdn.net")
                ||url.contains("https://instagram.fisb2-1.fna.fbcdn.net")||url.contains("https://instagram.fisb2-2.fna.fbcdn.net")
                ||url.contains("https://instagram.fisb1-1.fna.fbcdn.net")||url.contains("https://instagram.fisb1-2.fna.fbcdn.net")
                ||url.contains("https://instagram.fisb0-1.fna.fbcdn.net")||url.contains("https://instagram.fisb0-2.fna.fbcdn.net")) {
            if (url.contains(".mp4")) {

                ((MainActivity) act).changeColorFloat(act,url,"normal");
            }
        }
        if (url.contains("https://instagram.flhe9-1.fna.fbcdn.net")||url.contains("https://instagram.flhe9-2.fna.fbcdn.net")
                ||url.contains("https://instagram.flhe8-1.fna.fbcdn.net")||url.contains("https://instagram.flhe8-2.fna.fbcdn.net")
                ||url.contains("https://instagram.flhe7-1.fna.fbcdn.net")||url.contains("https://instagram.flhe7-2.fna.fbcdn.net")
                ||url.contains("https://instagram.flhe6-1.fna.fbcdn.net")||url.contains("https://instagram.flhe6-2.fna.fbcdn.net")
                ||url.contains("https://instagram.flhe5-1.fna.fbcdn.net")||url.contains("https://instagram.flhe5-2.fna.fbcdn.net")
                ||url.contains("https://instagram.flhe4-1.fna.fbcdn.net")||url.contains("https://instagram.flhe4-2.fna.fbcdn.net")
                ||url.contains("https://instagram.flhe3-1.fna.fbcdn.net")||url.contains("https://instagram.flhe3-2.fna.fbcdn.net")
                ||url.contains("https://instagram.flhe2-1.fna.fbcdn.net")||url.contains("https://instagram.flhe2-2.fna.fbcdn.net")
                ||url.contains("https://instagram.flhe1-1.fna.fbcdn.net")||url.contains("https://instagram.flhe1-2.fna.fbcdn.net")
                ||url.contains("https://instagram.flhe0-1.fna.fbcdn.net")||url.contains("https://instagram.flhe0-2.fna.fbcdn.net")) {
            if (url.contains(".mp4")) {

                ((MainActivity) act).changeColorFloat(act,url,"normal");
            }
        }


        if (url.contains("https://video.dtube.top/ipfs")) {

            ((MainActivity) act).changeColorFloat(act,url,"normal");
        }
        if (url.contains("https://video.like.video/eu_live")){
            if (url.contains(".mp4")) {
                ((MainActivity) act).changeColorFloat(act,url,"normal");
            }
        }

        if (url.contains("https://peertube.cpy.re")) {
            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {
                Log.i("url load match pattern ", url);
                //   Log.i("url load match pattern ", url);
                if (peertube.isEmpty()){
                    ((MainActivity) act).changeColorFloat(act,url,"normal");
                    peertube=url;
                }else{
                    if (peertube.equalsIgnoreCase(url)){

                    }else{
                        ((MainActivity) act).changeColorFloat(act,url,"normal");
                        peertube=url;
                    }

                }

            }

        }

        if (url.contains("cdn.liveleak.com")) {
            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {
                Log.i("url load match pattern ", url);
                //   Log.i("url load match pattern ", url);
                //  ((VpnMainActivity) act).showBottomSheetDialog(url,act,url);
            }

        }



//////////////////Ted.com
        if (url.contains("ted")) {
            if ((url.contains("www.ted.com/talks") && url.contains("metadata.json"))) {


                String[] parts = url.split("[/?]");

                int i = 0;
                while (i < parts.length) {
                    if (parts[i].contains("talks")) {
                        String float_str = parts[i + 1];
                        Log.i("url load match pattern ", url);
                        //    ((VpnMainActivity) act).getmyTedModel(float_str,act,url);

                        i = parts.length;
                        // Toast.makeText(act, "ted", Toast.LENGTH_SHORT).show();
                    }
                    i++;
                }
            }

        }




        /////////Scloud

        if (url.contains("media/soundcloud:tracks")  && url.contains("progressive")) {

            //  mainbrowseractivity.showBottomSheetDialog(url,activity,url)

            String[] parts = url.split("[/?]");

            int i = 0;
            while (i < parts.length) {

                if (parts[i].contains("media")) {

                    //Toast.makeText(getApplicationContext(),""+parts[i+1],Toast.LENGTH_SHORT).show();

                    String float_str = parts[i + 1];
                    // below is the method of abstract class... method kidr bna hoa // hm activity ko context ma change kar lata ha,



                    String[] partas = float_str.split(":");

                    String ind1=partas[0];
                    String ind2=partas[1];
                    String ind3=partas[2];
                    // val ind4=parts.get(3)


//                    ((MainActivity) act).getmySCloudModel(ind3,act,url);
                    ((MainActivity) act).changeColorFloat(act,url,"volley");
                    //   Toast.makeText(activity,""+parts[i+1],Toast.LENGTH_SHORT).show();


                    i = partas.length;


                }
                i++;

            }


        }

        //cdn.anysex.com/videos


        if (url.contains(MainActivity.site_mat_string)){
            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {

                ((MainActivity) act).changeColorFloat(act,url,"normal");

            }
        }


        if (url.contains("cdn.anysex.com/videos") || url.contains("cdn13.com/videos") ||url.contains("cdn-ht.pornhd.com/video")) {
            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {

                ((MainActivity) act).changeColorFloat(act,url,"normal");

            }

        }

        ///////////vidio

        if (url.contains("cdn0-a.production.vidio.static6.com")) {
            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {

                ((MainActivity) act).changeColorFloat(act,url,"normal");

            }

        }

///////////wwe

        if (url.contains("cdn2hls-vh.akamaihd.net")) {
            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {
                ((MainActivity) act).changeColorFloat(act,url,"normal");

            }

        }


        ///////////kannadacine

        if (url.contains("s14.votrefiles.com")) {
            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {

                ((MainActivity) act).changeColorFloat(act,url,"normal");
            }

        }



        ///////////indoxx1.com

        if (url.contains("googleusercontent.com")) {
            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {

                ((MainActivity) act).changeColorFloat(act,url,"normal");

            }
        }



        ///////////kurdcinama

        if (url.contains("vd53.mycdn.me")) {
            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {

                ((MainActivity) act).changeColorFloat(act,url,"normal");

            }
        }


        ///////////animeleggendari

        if (url.contains("blacraft.com")||url.contains("sorb.fruithosted.net")) {
            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {

                ((MainActivity) act).changeColorFloat(act,url,"normal");

            }
        }


        ///////////tamilgun

        if (url.contains("embed2.tamildbox.us")) {
            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {

                ((MainActivity) act).changeColorFloat(act,url,"normal");

            }
        }


        //////////1230moview

        if (url.contains("blacraft.com")) {
            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {

                ((MainActivity) act).changeColorFloat(act,url,"normal");

            }

        }

        ///////////////////okjutt

        if(url.contains("/video.php?href=https%3A%2F%2Fwww.facebook.com%2F")){

            String[] parts = url.split("[/?]");
            //  String parts = url.split("%");

            String ind1=parts[3];
            String ind2=parts[4];
            String ind3=parts[5];
            String ind4=parts[6];


            StringBuilder sb =new StringBuilder(ind1);
            sb.deleteCharAt(0);
            sb.deleteCharAt(0);
            sb.deleteCharAt(0);
            sb.deleteCharAt(0);
            sb.deleteCharAt(0);
            String link1=sb.toString();

            StringBuilder sb2 = new StringBuilder(ind2);
            sb2.deleteCharAt(0);
            sb2.deleteCharAt(0);
            String link2=sb2.toString();

            StringBuilder sb3 = new StringBuilder(ind3);
            sb3.deleteCharAt(0);
            sb3.deleteCharAt(0);
            String link3=sb3.toString();

            StringBuilder sb4 =new StringBuilder(ind4);
            sb4.deleteCharAt(0);
            sb4.deleteCharAt(0);
            String link4=sb4.toString();

//                val finalLink="https://mbasic"+link1+"/"+link2+"/"+link3+"/"+link4
            String finalLink="https://m"+link1+"/"+link2+"/"+link3+"/"+link4;

            ((MainActivity) act).changeColorFloat(act,finalLink,"normal");
        }
        //             }




        if(url.contains("video.flhe5-1.fna.fbcdn.net")) {


            if (prev_url_fb.equals(url)){

            }else{
                prev_url_fb=url;
                if (url.contains("bytestart") || url.contains("byteend") ){

                }else {

                    //            ((MainActivity) act).showBottomSheetDialog(url, act, url);
                }
            }

        }





        if(url.contains("video.flhe5-1.fna.fbcdn.net") && url.contains(".mp4") ) {


            if (prev_url_fb_n.equals(url)){

            }else{
                prev_url_fb_n=url;
                if (url.contains("bytestart") || url.contains("byteend") ){
                    int indexOfLast = url.lastIndexOf("bytestart");
                    url=url.substring(0,indexOfLast);
                    url.replace("video","scontent");
                    Log.i("url newfb if"," "+url);
                    ((MainActivity) act).changeColorFloat(act,url,"normal");
                }else {

                    Log.i("url newfb else"," "+url);
                    ((MainActivity) act).changeColorFloat(act,url,"normal");
                }
            }

        }

/////////////////////////
        if (url.contains("1fiagef.oloadcdn.net/dl")) {
            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {

                ((MainActivity) act).changeColorFloat(act,url,"normal");

            }

        }
        ////////////
        try {
            if (view.getUrl().contains("watch-free")) {
                if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                        url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                        url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                        url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                        url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                        url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {

                    if (!url.contains("jwpltx")) {


                        ((MainActivity) act).changeColorFloat(act,url,"normal");
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        if (url.contains("megavideo.xyz/storage") ||url.contains("str14.vidoza")) {

            if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
                    url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
                    url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
                    url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
                    url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
                    url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {

                ((MainActivity) act).changeColorFloat(act,url,"normal");
            }
        }
        ////////
        if (url.contains("openload.co/stream")) {

//                if (url.contains(".3g2") || url.contains(".3gp") || url.contains(".3gpp") ||
//                        url.contains(".asf") || url.contains(".dat") || url.contains(".divx") || url.contains(".dv") ||
//                        url.contains(".f4v") || url.contains(".m4v") || url.contains(".mod") || url.contains(".mov") ||
//                        url.contains(".m4p") || url.contains(".mkv") || url.contains(".mpe") || url.contains(".mpeg") ||
//                        url.contains(".mp4") || url.contains(".flv") || url.contains(".avi") || url.contains(".mpeg4") ||
//                        url.contains(".wmv") || url.contains(".mpg") || url.contains(".webm") || url.contains(".vob")) {

            ((MainActivity) act).changeColorFloat(act,url,"normal");

            // }
        }
////

        if (url.contains("player.vimeo.com/video/") && url.contains("config")) {
            String[] parts = url.split("/");
            for (int i = 0; i < parts.length; i++) {
                if (parts[i].equals("video")) {
//                    ((MainActivity) act).getmyvimeimodel(parts[i + 1],act,url);
                    ((MainActivity) act).changeColorFloat(act,"https://vimeo.com/"+parts[i + 1],"volley");
                    i = parts.length;


                }

            }

        }
////vimeoend

        // if (VpnMainActivity.mainpattarray.contains(url))


    }

    @Override
    public void onFormResubmission(WebView view, @NonNull final Message dontResend, @NonNull final Message resend) {
        Activity mActivity = ((MainActivity) act);
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Form resubmission?");
        builder.setMessage("Do you want to resubmit?")
                .setCancelable(true)
                .setPositiveButton(mActivity.getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                resend.sendToTarget();
                            }
                        })
                .setNegativeButton(mActivity.getString(android.R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dontResend.sendToTarget();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

//    @Override
//    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
////        super.onReceivedSslError(view, handler, error);
//
//try {
//    String errora = "";
//    switch (error.getPrimaryError()) {
//        case SslError.SSL_DATE_INVALID:
//            errora = String.valueOf(R.string.notification_error_ssl_date_invalid);
//            break;
//        case SslError.SSL_EXPIRED:
//            errora = String.valueOf(R.string.notification_error_ssl_expired);
//            break;
//        case SslError.SSL_IDMISMATCH:
//            errora = String.valueOf(R.string.notification_error_ssl_idmismatch);
//            break;
//        case SslError.SSL_INVALID:
//            errora = String.valueOf(R.string.notification_error_ssl_invalid);
//            break;
//        case SslError.SSL_NOTYETVALID:
//            errora = String.valueOf(R.string.notification_error_ssl_not_yet_valid);
//            break;
//        case SslError.SSL_UNTRUSTED:
//            errora = String.valueOf(R.string.notification_error_ssl_untrusted);
//            break;
//        default:
//            errora = String.valueOf(R.string.notification_error_ssl_cert_invalid);
//    }
//
//
//    final AlertDialog.Builder builder = new AlertDialog.Builder(act);
//    builder.setMessage(errora);
//    builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
//        @Override
//        public void onClick(DialogInterface dialog, int which) {
//            handler.proceed();
//        }
//    });
//    builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
//        @Override
//        public void onClick(DialogInterface dialog, int which) {
//            handler.cancel();
//        }
//    });
//    final AlertDialog dialog = builder.create();
//    dialog.show();
//}catch (Exception e){
//    e.printStackTrace();
//}
//    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        Log.i("urlover",url);
//        view.loadUrl("javascript:(function() { "
//                + "var el = document.querySelectorAll('div[data-sigil]');"
//                + "for(var i=0;i<el.length; i++)"
//                + "{"
//                + "var sigil = el[i].dataset.sigil;"
//                + "if(sigil.indexOf('inlineVideo') > -1){"
//                + "delete el[i].dataset.sigil;"
//                + "var jsonData = JSON.parse(el[i].dataset.store);"
//                + "el[i].setAttribute('onClick', 'FBDownloader.processVideo(\"'+jsonData['src']+'\");');"
//                + "}" + "}" + "})()");

        if(url.startsWith("http") || url.startsWith("file")|| url.startsWith("https")){
            Log.i("urloverride", "" + url);
            String str=MainActivity.pattern_mat_string;
            Pattern pattern = Pattern.compile(str);
            Matcher matcher = pattern.matcher(url);

            if (matcher.find() || url.contains(pattern.toString()) ) {

                Log.i("url load match pattern ", url);



//                if (url.contains("56.com") && url.contains("shtml")){
//
//
//                }else{
//                    return;
//                }


                if (url.contains("beeg.com/api/v6/")){
                    String p= url.substring(url.lastIndexOf("/") + 1);
                    url="https://beeg.com/"+p;
                }


                if (url.contains("ted")){
                    if(url.contains("json")){
                        Log.i("url match pattern ted", url);
                        url=url.substring(0,url.lastIndexOf("/"));
                        Log.i("url match pattern ted", url);
                    }else{

                    }
                }

                if (url.contains("syndication")) {

                }else {

                    //  url=url.substring(url.lastIndexOf("/",0));

                    //getdownloadlinkextractor
                    if (url.contains("liveleak") ||url.contains("56.com") ||url.contains("anyporn.com")) {
                        //((MainActivity) act).getdownloadlinkextractor(url, act);
                        ((MainActivity) act).changeColorFloat(act,url,"volley");
                        // ((MainActivity) act).getdownloadlink_dl(url, act);
                    }else {
                        if (url.contains("redtube")||url.contains("videa.hu/videok") )
                            ((MainActivity) act).changeColorFloat(act,url,"volley");
                        // ((MainActivity) act).getdownloadlink_dl(url, act);
                    }
                }

            } else {
                // TODO handle condition when input doesn't have an email address

            }

            if (url.contains("twitter")){
                view.loadUrl(url);
                return true;
            }



            return false;
        }
        else{
            Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
            ((MainActivity) act).startActivity(Intent.createChooser(intent,((MainActivity) act).getResources().getString(R.string.share)));
            return true;
        }
    }
    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        // Log.i("urlinter",url);
//        AdBlocker.init(act);
//        PreferenceUtils utils = new PreferenceUtils(act);
//        // if(utils.getAdBlock()) {
//        boolean ad;
//        if (!loadedUrls.containsKey(url)) {
//            ad = AdBlocker.isAd(url);
//            loadedUrls.put(url, ad);
//        } else {
//            ad = loadedUrls.get(url);
//        }
        return super.shouldInterceptRequest(view, url);
        //  }
        //  else {
        //      return super.shouldInterceptRequest(view,url);
        // }
    }

    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        super.onReceivedError(view, request, error);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (error.getErrorCode()==-6){
                opencheckdialog("Error",error.getDescription().toString(),view);
            }else{

            }
        }


    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public void opencheckdialog(String error,String description,WebView view){
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(act).create();
        alertDialog.setTitle(""+error +" ");
        alertDialog.setMessage(""+description+"\n"+"This site may be blocked in your country."+"\n"+"If you want to unlock this site click on Get VPN");
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Try Again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                view.reload();
                dialog.dismiss();

            }
        });
//        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Get Vpn", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                try {
//                    Intent irate = new Intent(Intent.ACTION_VIEW);
//                    irate.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + "com.jrzheng.supervpnfree"));
//                    act.startActivity(irate);
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//                dialog.dismiss();
//                //   startActivity(getIntent());
//            }
//        });

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Go back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //   startActivity(getIntent());
            }
        });

        alertDialog.show();
    }


/*    @JavascriptInterface
    public void getVideoData(String link) {
        act.runOnUiThread(() -> {

            Log.i("url", "processVideo " + link);
            try {

                if (TextUtils.isEmpty(link)) {
                    Toast.makeText(act, "Invalid/Empty URL", Toast.LENGTH_SHORT).show();
                } else {

                    if (!link.isEmpty()) {
                        Log.i("urlfb", "processVideo " + link);

                        ((MainActivity) act).changeColorFloat(act,link,"normal");
                        //   view.loadUrl("https://www.facebook.com/video/embed?video_id="+vidID);
                        //Toast.makeText(act, ""+vidData, Toast.LENGTH_SHORT).show();

                    }
                }
            }catch (Exception e){
                e.printStackTrace();}

        });
    }*/
}



