package all.video.downloader.AdmobHelper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;

import androidx.annotation.NonNull;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import all.video.downloader.R;

public class AdmobHelper {



    public static AdmobHelper instance;
    public InterstitialAd mInterstitial ;

    public static AdmobHelper getInstance() {
        if (instance == null) {
            instance = new AdmobHelper();
        }
        return instance;
    }
    private AdmobHelper() {

    }


    public void loadInterstitial(Context context, String msg){

        AdRequest adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(context,context.getString(R.string.interstial_id_admob), adRequest,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        mInterstitial = interstitialAd;
//                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
//                        Log.i(TAG, "onAdLoaded");


                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error
//                        Log.i(TAG, loadAdError.getMessage());
                        mInterstitial = null;

                    }
                });

    }

    public void showAdAfterDialogFor2Seconds(Activity context){
        try{

            ProgressDialog progressDialog = new ProgressDialog(context,R.style.AppCompatAlertDialogStyle);
            progressDialog.setMessage("Ad Is Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                    mInterstitial.show(context);
                    mInterstitial = null;

                }
            },1000);

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
